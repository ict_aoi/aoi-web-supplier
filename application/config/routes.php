<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$today = date('d');
// annual maintenance 28 - 30 december 2019
// if($today == 28 || $today == 29 || $today == 30){
//     $route['default_controller'] = "maintenance";
// }else{
//     $route['default_controller'] = "data/home";
// }

$route['default_controller'] = "data/home";
$route['404_override'] = 'home/error';
