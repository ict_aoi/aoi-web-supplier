<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
    /**
    *
    */
    class m_new_pl extends CI_model
    {
        //
        var $table = 'adt_activepo';
        var $select = array('c_order_id', 'documentno', 'm_warehouse_id');
        var $column_order = array('c_order_id', 'documentno');
        var $column_search = 'documentno';

        //
        private function _get_datatables_query($user, $m_warehouse_id)
        {
            $this->db
                 ->select($this->select)
                 ->from($this->table)
                 ->where('c_bpartner_id',$user)
                 ->where('m_warehouse_id', $m_warehouse_id)
                 ->group_by($this->select);

            //searching
            $columnSearch = $this->column_search;
            if(isset($_POST['search']['value']))
            {
                $this->db->like($columnSearch, $_POST['search']['value']);
            }

            //ordering
            if(isset($_POST['order']))
            {
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
            // elseif (isset($this->order))
            // {
            //     $order = $this->order;
            //
            //     $this->db2->order_by(key($order), $order[key($order)]);
            //
            // }
        }

        //
        public function get_datatables($user, $m_warehouse_id)
        {
            $this->_get_datatables_query($user, $m_warehouse_id);

            if($_POST['length'] != -1){
                $this->db->limit($_POST['length'], $_POST['start']);
            }
            $query = $this->db->get();
            return $query->result();
        }

        public function count_filtered($user, $m_warehouse_id)
        {
            $this->_get_datatables_query($user, $m_warehouse_id);
            $query = $this->db->get();
            return $query->num_rows();
        }

        public function count_all($user, $m_warehouse_id)
        {
            $this->db->from($this->table);
            $this->db->where('c_bpartner_id', $user);
            $this->db->where('m_warehouse_id', $m_warehouse_id);
            return $this->db->count_all_results();
        }

        function get_warna($c_order_id) {
            $where = array(
                'c_order_id' => $c_order_id
            );
            $query = $this->db->get_where('po_header', $where);
            return $query->num_rows();
        }

        function get_detail($m_warehouse_id=NULL, $limit=NULL) {
            if(!empty($m_warehouse_id)) {
                $this->db->where('m_warehouse_id',$m_warehouse_id);
            }
            if(!empty($limit)) {
                $this->db->limit(1);
            }

            $this->db->where('c_bpartner_id',$this->session->userdata('user_id'));
            $query = $this->db->get('adt_activepo');
            return $query->result();
        }

        function get_warehouse($type_po) {
            $this->db->order_by('name','asc');
            $this->db->where('description',$type_po);
            $query = $this->db->get('m_warehouse');
            return $query->result();
        }

        //
        function get_adt_mapping_bp() {
            $this->db2->where('c_bpartner_id',$this->session->userdata('user_id'));
            $query = $this->db2->get('adt_mapping_bp');
            return $query->result();
        }
    }

?>