<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	/**
	*
	*/
	class m_activepo extends CI_model
	{
		//
		var $table = 'adt_activepo';
        var $select = array('c_order_id', 'documentno', 'release_date', 'category','type_po');
        var $column_order = array('c_order_id', 'documentno', 'release_date', 'category');
        var $column_search = 'documentno';
        var $order = array('release_date' => 'asc');

		//
		private function _get_datatables_query($userid, $dari, $sampai)
        {
            $this->db
                 ->select($this->select)
                 ->from($this->table)
                 ->where('c_bpartner_id',$userid)
                 ->group_by($this->select);

			//
            if($dari != '' && $sampai != '' || $dari != NULL)
            {
                $this->db->where('to_char(release_date,\'YYYY-mm-dd\') BETWEEN \''. date('Y-m-d', strtotime($dari)). '\' and \''. date('Y-m-d', strtotime($sampai)).'\'');
            }

			//searching
            $columnSearch = $this->column_search;
            if(isset($_POST['search']['value']))
            {
				$this->db->like($columnSearch, $_POST['search']['value']);
            }

			//ordering
            if(isset($_POST['order']))
            {
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
            elseif (isset($this->order))
            {
                $order = $this->order;

                $this->db->order_by(key($order), $order[key($order)]);

            }
        }

		//
		public function get_datatables($userid, $dari, $sampai)
        {
            $this->_get_datatables_query($userid, $dari, $sampai);

            if($_POST['length'] != -1){
                $this->db->limit($_POST['length'], $_POST['start']);
            }
            $query = $this->db->get();
            return $query->result();
        }

		public function count_filtered($userid, $dari, $sampai)
        {
            $this->_get_datatables_query($userid, $dari, $sampai);
            $query = $this->db->get();
            return $query->num_rows();
        }

        public function count_all($userid)
        {
            $this->db->from($this->table);
            $this->db->where('c_bpartner_id', $userid);
            return $this->db->count_all_results();
        }

		//
		function get_po_status($c_order_id, $c_bpartner_id)
		{
			$where = array(
				'c_order_id' => $c_order_id,
				'status' => 't',
				'c_bpartner_id' => $c_bpartner_id
			);
			$query = $this->db->get_where('show_po_status', $where);
			return $query->num_rows();
			// $query = $this->db->query("SELECT COUNT('c_order_id') FROM show_po_status
			// 						   WHERE c_order_id = '".$c_order_id."' AND status = 't' AND c_bpartner_id = '".$c_bpartner_id."'");
			// return $query;
		}

		//-----------------------------------------------------------
		function get_po_file_additional($c_order_id)
		{
			$this->db->where('c_order_id',$c_order_id);
			$this->db->limit(1);
			$query = $this->db->get('m_po_file_additional');
			return $query->num_rows();
			// $query = $this->db->query("SELECT COUNT('c_order_id') FROM m_po_file_additional
			// 						   WHERE c_order_id = '".$c_order_id."' LIMIT 1");
			// return $query;
		}

		//count1
		function get_po_file_additional2($userid, $c_order_id)
		{
			$this->db->from('m_po_file_additional');
			$this->db->where('c_bpartner_id',$userid);
			$this->db->where('read','f');
			$this->db->where('c_order_id',$c_order_id);
            return $this->db->count_all_results();
			// $query = $this->db->query("SELECT COUNT('c_order_id') FROM m_po_file_additional
			// 						   WHERE c_bpartner_id = '".$userid."' AND c_order_id = '".$c_order_id."' AND read = 'f'");
			// return $query;

		}

		//count2
		function get_f_web_po_detail($c_order_id)
		{
			$this->db2->where('c_order_id',$c_order_id);
			$query = $this->db2->get('f_web_po_detail');
			return $query->num_rows();
			// $query = $this->db2->query("SELECT COUNT('c_order_id') FROM f_web_po_detail
			// 						   WHERE c_order_id = '".$c_order_id."'");
			// return $query;
		}

		function get_m_date_promised($c_order_id)
		{
			$this->db->distinct();
			$this->db->select('c_orderline_id');
			$this->db->from('m_date_promised');
			$this->db->where('c_order_id',$c_order_id);
			$this->db->where('c_orderline_id >',0);
            return $this->db->count_all_results();

		}
	}

?>
