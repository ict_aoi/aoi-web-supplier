<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	/**
	*
	*/
	class upload_add_model extends CI_model
	{
		//
        // var $table = 'f_web_po_header';
        var $table = 'adt_web_po_header_status';
        var $select = array('c_order_id','documentno','c_bpartner_id','create_po');
        var $column_order = array('c_order_id','documentno','c_bpartner_id','create_po');
        var $column_search = 'documentno';
        // var $order = array('create_po' => 'asc');
        var $order = array('status' => 'desc');

		//
		private function _get_datatables_query($c_bpartner_id)
        {
            $this->db2
                 ->select($this->select)
                 ->from($this->table)
                 ->where('c_bpartner_id',$c_bpartner_id);
			//
            

			//searching
            $columnSearch = $this->column_search;
            if(isset($_POST['search']['value']))
            {
				$this->db2->like($columnSearch, $_POST['search']['value']);
            }

			//ordering
            if(isset($_POST['order']))
            {
                $this->db2->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
            elseif (isset($this->order))
            {
                $order = $this->order;

                $this->db2->order_by(key($order), $order[key($order)]);

            }
        }

		//
		public function get_datatables($c_bpartner_id)
        {
            $this->_get_datatables_query($c_bpartner_id);

            if($_POST['length'] != -1){
                $this->db2->limit($_POST['length'], $_POST['start']);
            }
            $query = $this->db2->get();
            return $query->result();
        }

		public function count_filtered($c_bpartner_id)
        {
            $this->_get_datatables_query($c_bpartner_id);
            $query = $this->db2->get();
            return $query->num_rows();
        }

        public function count_all($c_bpartner_id)
        {
            $this->db2->from($this->table);
            $this->db->where('c_bpartner_id', $c_bpartner_id);
            return $this->db2->count_all_results();
        }

	}

?>
