<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	/**
	*
	*/
	class listpo_model extends CI_model
	{
		//
		var $table = 'm_user';
        var $select = array('user_id','nama');
        var $column_order = array('user_id','nama');
        var $column_search = 'nama';
        var $order = array('nama' => 'asc');

		//
		private function _get_datatables_query()
        {
			$this->db
				 ->where('isactive','t')
                 ->select($this->select)
                 ->from($this->table);
			//


			//searching
            $columnSearch = $this->column_search;
            if(isset($_POST['search']['value']))
            {
				// $this->db->like($columnSearch, $_POST['search']['value']);
				$this->db->like('LOWER(' .$columnSearch. ')', strtolower($_POST['search']['value']));
            }

			//ordering
            if(isset($_POST['order']))
            {
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
            elseif (isset($this->order))
            {
                $order = $this->order;

                $this->db->order_by(key($order), $order[key($order)]);

            }
        }

		//
		public function get_datatables()
        {
            $this->_get_datatables_query();

            if($_POST['length'] != -1){
                $this->db->limit($_POST['length'], $_POST['start']);
            }
            $query = $this->db->get();
            return $query->result();
        }

		public function count_filtered()
        {
            $this->_get_datatables_query();
            $query = $this->db->get();
            return $query->num_rows();
        }

        public function count_all()
        {
            $this->db->from($this->table);
            return $this->db->count_all_results();
        }


		//-----------------------------------------------------------
		function get_unconfirmed($user_id)
		{
			$i          = 0;
			$a = $this->db->where('status','t')->where('c_bpartner_id',$user_id)->get('show_po_status');
			if ($a->num_rows()>0) {
				foreach($a->result() as $b){
					$c_order[$i] = $b->c_order_id;
				}

				if(empty($c_order[$i])){
					$this->db2->where_not_in('c_order_id',$c_order[$i]);
				}
				$this->db2->where_not_in('c_order_id', $c_order[$i]);
				$this->db2->where('c_bpartner_id', $user_id);
				return $this->db2->get('f_web_po_header')->num_rows();
				// //$this->db2->where_not_in('c_order_id',$c_order[$i]);
				// return $this->db2->where('c_bpartner_id',$user_id)->get('f_web_po_header')->num_rows();
			}
		}

		//count1
		function get_unlock($user_id)
		{
			$i          = 0;
			$a = $this->db->where('lock','t')->where('c_bpartner_id',$user_id)->get('m_date_promised');
			if($a->num_rows()>0){
				foreach($a->result() as $b){
					$c_orderline_id[$i] = $b->c_orderline_id;
				}
				if(empty($c_orderline_id[$i])){
					$this->db2->where_not_in('c_orderline_ida',$c_orderline_id[$i]);
				}
				return $this->db2->where('c_bpartner_id',$user_id)->get('f_web_po_detail')->num_rows();
			}
		}

	}

?>
