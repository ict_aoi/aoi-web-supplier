<?php if(!defined('BASEPATH')) exit('No direct script access allowed');
	/**
	* 
	*/
	class data_model extends CI_model
	{	
        var $table = 'edm_packinglist'; // define table
        var $select = array('no_packinglist', 'kst_suratjalanvendor', 'kst_invoicevendor', 'kst_resi', 'type_po', 'packing_list_total', 'is_locked', 'c_bpartner_id', 'tanggal'); //specify the columns you want to fetch from table
        var $column_order = array('no_packinglist', 'kst_invoicevendor','kst_suratjalanvendor','packing_list_total','c_bpartner_id','tanggal','is_locked'); //set column field database for datatable orderable
        // var $column_search = array('no_packinglist','kst_invoicevendor'); //set column field database for datatable searchable
        var $column_search = 'no_packinglist';
        var $order = array('tanggal' => 'desc'); // default order


        public function get_datatables($from,$to,$user)
        {
            $this->_get_datatables_query($from, $to,$user);
           
            if($_POST['length'] != -1){
                $this->db->limit($_POST['length'], $_POST['start']);
            }
            $query = $this->db->get();
            
            return $query->result();
        }

        public function count_filtered($from,$to,$user)
        {
            $this->_get_datatables_query($from, $to,$user);
            $query = $this->db->get();
           
            return $query->num_rows();
        }

        public function count_all($user)
        {
            
            $this->db->from($this->table);
            $this->db->where('c_bpartner_id',$user);
            
            return $this->db->count_all_results();
        }

        private function _get_datatables_query($from,$to,$user)
        {

            $this->db
                 ->select($this->select)
                 ->from($this->table)
                 ->where('c_bpartner_id',$user);

            if($from!='' && $to!='' || $from!= NULL) // To process our custom input parameter
            {

                $this->db->where('tanggal BETWEEN \''. date('Y-m-d', strtotime($from)). '\' and \''. date('Y-m-d', strtotime($to)).'\'');
            }

            $i = 0;
            $test = $_POST['search']['value'];
            $item=$this->column_search;
            if($_POST['search']['value']){
                $this->db->where('('.$item.' LIKE \'%'.$test.'%\' OR '.$item.' LIKE \'%'.$test.'%\')');
            }
            // foreach ($this->column_search as $item) // loop column
            // {
            //     if($_POST['search']['value']) // if datatable send POST for search
            //     {

            //         if($i===0) // first loop
            //         {
            //            // $this->db->or_group_start(); // open bracket. query Where with OR clause better with bracket. because maybe can combine with other WHERE with AND.
            //             $this->db->like($item, $_POST['search']['value']);
            //         }
            //         else
            //         {
            //             $this->db->or_like($item, $_POST['search']['value']);
            //         }

            //         //if(count($this->column_search) - 1 == $i) //last loop
            //             //$this->db->group_end(); //close bracket
            //     }
            //     $i++;
            // }

            if(isset($_POST['order'])) // here order processing
            {
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);

            }
            elseif (isset($this->order)) // default order processing
            {
                $order = $this->order;

                $this->db->order_by(key($order), $order[key($order)]);

            }
        }  
	}

?>