<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Unlock_Model extends CI_Model {

	var $table = 'edm_packinglist'; // define table
    var $select = array('no_packinglist','kst_suratjalanvendor','kst_invoicevendor','kst_resi','is_locked'); //specify the columns you want to fetch from table
    var $column_order = array('no_packinglist','kst_suratjalanvendor','kst_invoicevendor','kst_resi','is_locked'); //set column field database for datatable orderable
    var $column_search = 'no_packinglist'; //set column field database for datatable searchable
    var $order = array('no_packinglist' => 'asc'); // default order
    //var $use  = $this->session->userdata('user_id');


    private function _get_datatables_query($user)
        {
            $this->db
                 ->select($this->select)
                 ->from($this->table)
                 ->where('c_bpartner_id',$user)
                 ->where('is_locked','t')
                 ->group_by($this->select);

            //searching
            $columnSearch = $this->column_search;
            if(isset($_POST['search']['value']))
            {
                $this->db->like($columnSearch, $_POST['search']['value']);
            }

            //ordering
            if(isset($_POST['order']))
            {
                $this->db->order_by($this->column_order[$_POST['order']['0']['column']], $_POST['order']['0']['dir']);
            }
        }

        //
        public function get_datatables($user)
        {
            $this->_get_datatables_query($user);

            if($_POST['length'] != -1){
                $this->db->limit($_POST['length'], $_POST['start']);
            }
            $query = $this->db->get();
            return $query->result();
        }

        public function count_filtered($user)
        {
            $this->_get_datatables_query($user);
            $query = $this->db->get();
            return $query->num_rows();
        }

        public function count_all($user)
        {
            $this->db->from($this->table);
            $this->db->where('c_bpartner_id', $user);
            $this->db->where('is_locked', 't');
            return $this->db->count_all_results();
        }

}

/* End of file modelName.php */
/* Location: ./application/models/modelName.php */