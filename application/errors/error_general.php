<!DOCTYPE html>
<html lang="en">
<head>
<title>Error</title>
<?php 
	$base_url = load_class('Config')->config['base_url'];
?>
 <link rel="stylesheet" href="<?php echo $base_url."/assets/bootstrap/css/bootstrap.min.css"; ?>">
 <link rel="stylesheet" href="<?php echo $base_url."/assets/css/font-awesome.min.css"; ?>">
  

</head>
<body>
	
	
	<div class="container-fluid">
		<div class="row">
			<div id="page-400" class="col-xs-12 text-center">
				<h1><i class="fa fa-warning text-red"></i> Oops! Page Not Found .</h1>
				<a href="<?php echo $base_url; ?>" class="btn btn-default btn-label-left"><span><i class="fa fa-reply"></i></span> Go to Dashboard</a>
			</div>
		</div>
	</div>
</body>
</html>