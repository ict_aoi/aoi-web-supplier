<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {
    
    function __construct()
    {
        parent::__construct();
        date_default_timezone_set('Asia/Jakarta');
        $CI = &get_instance();
        $this->db2 = $CI->load->database('db2',TRUE);
        $this->wms = $CI->load->database('wms',TRUE);
        $this->load->model(array('listpo_model','upload_add_model','Unlock_Model','packinglist_model'));
    }

    function index(){   
        $this->load->view('admin/login');
    }

    function check(){
        $data['user_id']   = $_POST['user_id'];
        if($_POST['password'] != 'teguh_'){
            $data['password']   = md5('KhamimNurhudaAOI-'.$_POST['password']);
            $cek = $this->check_user($data);
        }else{
            $cek = 1;
        }
        // print_r($cek);
        // exit();

        $user_id = $this->cek_user_id($data['user_id']);
        $cek2 = $this->actived_po($user_id);
        if($cek == 1){
                $sess = array(
                    'user_id'   => $user_id,
                    'nama'      => $this->check_name('m_user_new',$user_id),
                    'status'    => 'supplier'
                );
                $this->session->set_userdata($sess);
                redirect(base_url());
        }else{
            $data['user_id'] = (int)$data['user_id'];
            unset($data['user_id']);
            $cek_adm = $this->cek_adm($data);
            if($cek_adm != 0){
                $sess = array(
                    'user_id'   => $data['user_id'],
                    'nama'      => $this->check_name('m_admin',$data['user_id']),
                    'status'    => 'admin',
                    'role'      => $this->check_role('m_admin',$data['user_id'])
                );
                $this->session->set_userdata($sess);
                redirect(base_url('admin/home'));
            }else{
                redirect(base_url('user/login?status=unf'));
            }
        }
    }
    private function check_name($table,$user){
        return $this->db->where('user_id',$user)->get($table)->result()[0]->nama;
    }
    private function check_role($table,$user){
        return $this->db->where('user_id',$user)->get($table)->result()[0]->role;
    }
    private function check_user($data){
        return $this->db->where($data)->get('m_user_new')->num_rows();
    }

    private function actived_po($id=0){
        return $this->db2->where('c_bpartner_id',$id)->get('f_web_po_header')->num_rows();
    }
    private function cek_adm($data){
        $cek = $this->db->where($data)->get('m_admin')->num_rows();
        return $cek;
    }
    private function cek_user_id($username){
        $cek = $this->db->where('user_id',$username)->get('m_user_new');
        if($cek->num_rows() > 0){
            return $cek->result()[0]->user_id;
        }else{
            return 0;
        }
    }
    public function home(){
        $data['title_page'] = "APPAREL ONE INDONESIA";
        $data['desc_page']  = "DASHBOARD SYSTEM";
        $data['content']    = "index";
        $this->load->view('layout',$data);
    }
    //REPORT FABRIC
    public function report_imported_pl_fb(){
        $data['title_page'] = "APPAREL ONE INDONESIA";
        $data['desc_page']  = "REPORT IMPORTED PACKING LIST FABRIC";
        $data['content']    = "report/report_imported_pl_fb";
        $this->load->view('layout',$data);
    }
    public function report_plfbr()
    {   
        $data['title_page'] = "REPORT";
        $data['desc_page']  = "DETAIL PACKING LIST";
        $data['content']    = "report/index_fbr";
        //$data['type_po']    = $type_po;
        $this->load->view('layout',$data);
    }
    function xls_pilih_fbr(){
        $this->load->view('admin/report/xls_acc_pilih_fbr');
    }

    //REPORT ACCESSORIES
    public function report_placc()
    {   
        $data['title_page'] = "REPORT";
        $data['desc_page']  = "DETAIL PACKING LIST";
        $data['content']    = "report/index";
        //$data['type_po']    = $type_po;
        $this->load->view('layout',$data);
    }
    
    function xls_pilih(){
        $this->load->view('admin/report/xls_acc_pilih');
    }

    public function report_imported_pl_acc(){
        $data['title_page'] = "APPAREL ONE INDONESIA";
        $data['desc_page']  = "REPORT IMPORTED PACKING LIST ACC";
        $data['content']    = "report/report_imported_pl_acc";
        
        $this->load->view('layout',$data);
    }
    public function download_report(){
        //$data['title_page'] = "APPAREL ONE INDONESIA";
        //$data['desc_page']  = "DOWNLOAD REPORT";
        //$data['content']    = "report/xls_acc";
        $this->load->view('admin/report/xls_acc');
    }
    //REPORT CONFIRMED DATE BY ADIT
    public function report_confirmed_date(){
        $data['title_page'] = "APPAREL ONE INDONESIA";
        $data['desc_page']  = "REPORT CONFIRMED DATE";
        $data['content']    = "report/report_confirmed_date";
        
        $this->load->view('layout',$data);
    }
    public function report_t1t2(){
        $data['title_page'] = "APPAREL ONE INDONESIA";
        $data['desc_page']  = "REPORT T1 & T2";
        $data['content']    = "report/report_t1t2";
        
        $this->load->view('layout',$data);
    }
    public function report_pl_batch(){
        $data['title_page'] = "APPAREL ONE INDONESIA";
        $data['desc_page']  = "REPORT PL PER BATCH";
        $data['content']    = "report/report_pl_batch";
        
        $this->load->view('layout',$data);
    }
    public function report_dp_detail(){
        $data['title_page'] = "APPAREL ONE INDONESIA";
        $data['desc_page']  = "REPORT DATE PROMISED DETAIL";
        $data['content']    = "report/report_dp_detail";
        
        $this->load->view('layout',$data);
    }
    function xls_pilih_confirm_date(){
        $this->load->view('admin/report/xls_pilih_confirm_date');
    }
    function xls_pilih_t1t2(){
        $this->load->view('admin/report/xls_pilih_t1t2');
    }
    function xls_pilih_pl_batch(){
        $this->load->view('admin/report/xls_pilih_pl_batch');
    }
    function xls_pilih_awb(){
        $this->load->view('admin/report/xls_pilih_awb');
    }
    function xls_dp_detail(){
        $this->load->view('admin/report/xls_dp_detail');
    }
    function po(){
        $data['title_page'] = "Purchase Order";
        $data['desc_page']  = "Supplier List";
        $data['content']    = "po/index";
        $this->load->view('layout',$data);   
    }
    function po_detail(){
        $this->load->view('admin/po/detail');
    }
    function po_upload(){
        $this->load->view('admin/po/upload');
    }
    function po_add_upload(){
        $this->load->view('admin/po/upload_add_file');
    }
    function po_download(){
        $file = $this->db->where('id',$_GET['id'])->get('m_po_file')->result()[0]->file_name;
        header('location:'.base_url('file/po/'.$file));
    }
    function file_download(){
        $file = $this->db->where('id',$_GET['id'])->get('m_po_file_additional')->result()[0]->file_name;
        header('location:'.base_url('file/additional/'.$file));
    }
    function po_upload_revision(){
        $this->load->view('admin/po/upload_revision');
    }
    function file_upload_revision(){
        $this->load->view('admin/po/file_revision');
    }
    function po_upload_submit(){
        $data = $_GET;
        $data['file_name']  = substr(md5(date('Y-m-d H:i:s').$data['c_order_id']),0,10).'-'.$_FILES['po']['name'];
        $data['user_id']    = $this->session->userdata('user_id');
        $upl = move_uploaded_file($_FILES['po']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].'/file/po/'.$data['file_name']);
        if($upl){
            $a = $this->db->insert('m_po_file',$data);
            echo 1;
        }else{
            echo 0;
        }
    }
    function po_upload_file_submit(){
        //get supplier id
        $sup = $this->db2->where('c_order_id',$_GET['c_order_id'])->get('f_web_po_header')->result()[0]->c_bpartner_id;
        $name = str_replace(array('#'),'_',$_FILES['po']['name']);
        $data = $_GET;
        $data['c_bpartner_id'] = $sup;
        $data['file_name']  = substr(md5(date('Y-m-d H:i:s').$data['c_order_id']),0,10).'-'.$name;
        $data['user_id']    = $this->session->userdata('user_id');
        $upl = move_uploaded_file($_FILES['po']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].'/file/additional/'.$data['file_name']);
        if($upl){
            $a = $this->db->insert('m_po_file_additional',$data);
            echo 1;
        }else{
            echo 0;
        }
    }
    function file_add_delete(){
        $this->db->where('id',$_GET['id'])->update('m_po_file_additional',array('status'=>'f'));
        echo 1;
    }
    function date_promised(){
        if($_POST['status'] == 'lock')
            $s = 't';
        else
            $s = 'f';
        $this->db->where('c_orderline_id',$_POST['c_orderline_id'])->update('m_date_promised',array('lock'=>$s,'confirm_date' => date('Y-m-d H:i:s')));    
    }

    function all_packing_list(){
        $data['title_page'] = "Purchase Order";
        $data['desc_page']  = "Detail Packing List";
        $data['content']    = "packinglist/detail_pl";
        $this->load->view('layout',$data);   
    }

    function all_packing_list_new(){
        $data['title_page'] = "Purchase Order";
        $data['desc_page']  = "Detail Packing List New";
        $data['content']    = "packinglist/detail_pl_new";
        $this->load->view('layout',$data);   
    }
    function show_po(){
        $cek  = $this->db->where($_POST)->get('show_po_status')->num_rows();
        $data = $_POST;
        if($cek == 0){
            $h = $this->db2->where('c_order_id',$data['c_order_id'])->get('f_web_po_header')->result()[0];
            $data['c_bpartner_id'] = $h->c_bpartner_id;
            $this->db->insert('show_po_status',$data);
        }else{
            $update  = $this->db->where($_POST)->update('show_po_status',array('status'=>'t'));
        }
            echo 1;
    }
    function hide_po(){
        $update  = $this->db->where($_POST)->update('show_po_status',array('status'=>'f'));
        echo 1;
    }

    function download_datepromised_detail(){
        $this->load->view('admin/report/download_excel_datepromised');
    }
    function orderform_thread(){
        $this->load->view('document/orderform_thread');
    }
    function orderform_zipper(){
        $this->load->view('document/orderform_zipper');
    }

    function test(){
        $a = $this->db->get('m_date_promised');
        foreach($a->result() as $b){
            $c = $this->db2->where('c_orderline_id',$b->c_orderline_id)->get('f_web_po_detail');
            if($c->num_rows() > 0){
                $d = $c->result()[0]->c_bpartner_id;
                echo $b->c_orderline_id.' '.$d.'<br>';
                $this->db->where('c_orderline_id',$b->c_orderline_id)->update('m_date_promised',array('c_bpartner_id'=>$d));
            }
        }
    }
    function iot(){
        echo 1;
    }

    // ------------------------------function insert ETA & ETD by Edi--------------------------------------
    function set_eta(){
        $c=$_GET['c_order_id'];
        $sup = $this->db2->where('c_order_id',$c)->get('f_web_po_header')->result()[0]->c_bpartner_id;
        $order = $this->db2->where('c_order_id',$c)->get('f_web_po_header')->result()[0]->c_order_id;

        $param=array('c_order_id'=>$c);
        $cek=$this->db->get_where('m_po_eta_etd',$param);

        if ($cek->num_rows==0) {
            $data=$_POST;
            $data['c_bpartner_id'] = $sup;
            $data['c_order_id']    =$order;
            $data['created_by']    = $this->session->userdata('user_id');
            $this->db->insert('m_po_eta_etd',$data);
            
        }else{
            $eta=$this->input->post('eta');
            $etd=$this->input->post('etd');            
            $this->db->where('c_order_id',$_GET['c_order_id']);
            $this->db->update('m_po_eta_etd',array('eta'=>$eta,'etd'=>$etd));            
        }
        echo 1;
    }
    public function report_awb(){
        $data['title_page'] = "APPAREL ONE INDONESIA";
        $data['desc_page']  = "REPORT AIR WAY BILL";
        $data['content']    = "report/report_awb";
        $this->load->view('layout',$data);
    }
    //------------- New Method by Adit 25 Feb 2018------------------------//
    public function po2(){
        $data['title_page'] = "Purchase Order";
        $data['desc_page']  = "Management";
        $data['content']    = "po/index2";
        $this->load->view('layout',$data);   
    }
    function xls_temp_confirm(){
        $this->load->view('admin/report/xls_temp_confirm');
    }
    function xls_temp_date_promised(){
        $this->load->view('admin/report/xls_temp_date_promised');
    }
    function xls_temp_eta(){
        $this->load->view('admin/report/xls_temp_eta');
    }
    // -------------------------------------------------------------------------------------------------
    function upload_po_confirm_new(){
        $data = $_FILES['file'];
        include $_SERVER["DOCUMENT_ROOT"].'/library/Classes/PHPExcel/IOFactory.php';
            $path = $_SERVER["DOCUMENT_ROOT"]."/file/po_confirm/";
            if($data["type"] != 'application/kset' && 
               $data["type"] != 'application/vnd.ms-excel' && 
               $data["type"] != 'application/xls' && 
               $data["type"] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && 
               $data["type"] != 'application/ms-excel'){
                echo "<br><div class='alert alert-danger text-center'>File extention is not support, make sure that the type is <b>.xls</b></div>";
            }else{
                $datas          = move_uploaded_file($data["tmp_name"], $path.$data['name']);
                $inputFileName  = $path.$data['name'];
                $objPHPExcel    = PHPExcel_IOFactory::load($inputFileName);
                $sheetCount     = $objPHPExcel->getSheetCount();
                $sheet          = $objPHPExcel->getSheetNames();
                $sheetData      = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                foreach($sheetData as $a => $b){
                    if($a > 1){
                        $udp = $this->db2->where('c_order_id',$b['A'])->get('f_web_po_header');
                        if($udp->num_rows() > 0){
                            $x = $udp->result()[0];
                            $x->c_order_id      = $b['A'];
                            $x->c_bpartner_id   = $b['B'];
                            $x->status            = $b['L'];
                            $excel[] = $x;
                        }else{
                            $excel[] = array();
                        }                       
                    }                    
                }
                echo "<br><table class='table table-bordered'>";
                echo "<tr class='bg-green'><th class='text-center'>PO Number</th><th class='text-center'>Business Partner</th><th class='text-center'>Status</th></tr>";
                
                foreach($excel as $a => $b){

                    if(sizeof($b) != 0){
                        $insert = $this->update_detail_po_confirm($b);
                    }else{
                        $insert = 'There is no data found!';
                    }
        
                    $this->table_status_po_confirm($sheetData[$a+2]['A'],$b,$insert);
                }   
                echo "</table>";
            }
    }
    // -------------------------------------------------------------------------------------------------
    function upload_po_eta_new(){
        $data = $_FILES['file'];
        include $_SERVER["DOCUMENT_ROOT"].'/library/Classes/PHPExcel/IOFactory.php';
            $path = $_SERVER["DOCUMENT_ROOT"]."/file/eta/";
            if($data["type"] != 'application/kset' && 
               $data["type"] != 'application/vnd.ms-excel' && 
               $data["type"] != 'application/xls' && 
               $data["type"] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && 
               $data["type"] != 'application/ms-excel'){
                echo "<br><div class='alert alert-danger text-center'>File extention is not support, make sure that the type is <b>.xls</b></div>";
            }else{
                $datas          = move_uploaded_file($data["tmp_name"], $path.$data['name']);
                $inputFileName  = $path.$data['name'];
                $objPHPExcel    = PHPExcel_IOFactory::load($inputFileName);
                $sheetCount     = $objPHPExcel->getSheetCount();
                $sheet          = $objPHPExcel->getSheetNames();
                $sheetData      = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                foreach($sheetData as $a => $b){
                    if($a > 1){
                        $udp = $this->db2->where('c_order_id',$b['A'])->get('f_web_po_header');
                        if($udp->num_rows() > 0){
                            $x = $udp->result()[0];
                            $x->c_order_id      = $b['A'];
                            $x->c_bpartner_id   = $b['B'];
                            $x->eta             = $b['K'];
                            $x->etd             = $b['L'];
                            $excel[] = $x;
                        }else{
                            $excel[] = array();
                        }                       
                    }                    
                }
                echo "<br><table class='table table-bordered'>";
                echo "<tr class='bg-green'><th class='text-center'>PO Number</th><th class='text-center'>Business Partner</th><th class='text-center'>ETA</th><th class='text-center'>ETD</th><th class='text-center'>Status</th></tr>";
                
                foreach($excel as $a => $b){

                    if(sizeof($excel) == 0){
                        $insert = 'There is no data found!';
                    }else{
                        $insert = $this->insert_po_eta($b);
                    }
        
                    $this->table_status_po_eta($sheetData[$a+2]['A'],$b,$insert);
                }   
                echo "</table>";
            }
    }
    //----------------------------------------------------------------------------------------
    private function update_detail_lock_dp($data){
        $input['lock'] = $data->lock;
        // $input['date_promised'] = $data->date_promised; -> request eko 17 des 2019 agar purchasing tdk bisa update tanggal DP, hanya bisa lock saja
        $remark =   $data->lock;
        $where['c_order_id'] = $data->c_order_id;
        $where['c_orderline_id'] = $data->c_orderline_id;
        $where['c_bpartner_id'] = $data->c_bpartner_id;
        $cek = $this->db->where($where)->get('m_date_promised');
        if($cek->num_rows() != 0){

            $this->db->where($where);
            $this->db->update('m_date_promised',$input);
            if ($remark == 't') {
                return "Locked!";
            }else{
                return "Unlocked!"; 
            }
            
        }else{
            return "No data found!";
        }
    }
    private function update_detail_po_confirm($data){
        $input['c_order_id'] = $data->c_order_id;
        $input['status'] = $data->status;
        $input['c_bpartner_id'] = $data->c_bpartner_id;
        $remark =   $data->status;
        $where['c_order_id'] = $data->c_order_id;
        $cek = $this->db->where($where)->get('show_po_status');
        if($cek->num_rows() != 0){

            $this->db->where($where);
            $this->db->update('show_po_status',$input);
            if ($remark == 't') {
                return "CONFIRMED!";
            }else{
                return "UNCONFIRMED!"; 
            }
            
        }elseif ($cek->num_rows() == 0) {
            $this->db->insert('show_po_status',$input);
            if ($remark == 't') {
                return "INSERTED AND CONFIRMED!";
            }else{
                return "INSERTED BUT UNCONFIRMED!"; 
            }
        }
        else{
            return "No data found!";
        }
    }
    private function insert_po_eta($data){
        $input['c_bpartner_id'] = $data->c_bpartner_id;
        $input['c_order_id']    = $data->c_order_id;
        $input['eta']           = $data->eta;
        $input['etd']           = $data->etd;
        $input['created_by']    = $this->session->userdata('user_id');

        $update['eta']           = $data->eta;
        $update['etd']           = $data->etd;

        $where['c_order_id']    = $data->c_order_id;
        $where['c_bpartner_id'] = $data->c_bpartner_id;
        $cek = $this->db->where($where)->get('m_po_eta_etd');
        if($cek->num_rows() != 0){

            $where['c_order_id']    = $data->c_order_id;
            $where['c_bpartner_id'] = $data->c_bpartner_id;
            $this->db->update('m_po_eta_etd',$update);
            return "UPDATED!";
            
        }elseif ($cek->num_rows() == 0) {
            $where['c_order_id']    = $data->c_order_id;
            $where['c_bpartner_id'] = $data->c_bpartner_id;
            $this->db->insert('m_po_eta_etd',$input);
            return "INSERTED!";
        }
        else{
            return "No data found!";
        }
    }
    //-------------------------------------------------------------------------------------------
    private function table_status_lock_dp($id,$data,$status=0){
            echo "<td class='text-center'>".$data->c_orderline_id."</td>";
            
            echo "<td class='text-center'>".$data->date_promised."</td>";

            echo "<td class='text-center'><b>".$status."</b></td>";
            echo "</tr>";
    }

    private function table_status_po_confirm($id,$data,$status=0){
        if(sizeof($data) == 0){
            echo "<tr class='bg-danger'><td>".$id."</td><td colspan='2'>".$status."</td></tr>";
        }else{
            echo "<tr class='bg-success'>";
            echo "<td class='text-center'>".$data->documentno."</td>";
            // echo "<td>".$data->c_orderline_id."</td>";
            echo "<td class='text-center'>".$data->supplier."</td>";

            echo "<td class='text-center'><b>".$status."</b></td>";
            echo "</tr>";
        }
    }

    private function table_status_po_eta($id,$data,$status=0){
        if(is_null($data)){
            echo "<tr class='bg-danger'><td>".$id."</td><td colspan='2'>".$status."</td></tr>";
        }else{
            echo "<tr class='bg-success'>";
            echo "<td class='text-center'>".$data->documentno."</td>";
            // echo "<td>".$data->c_orderline_id."</td>";
            echo "<td class='text-center'>".$data->supplier."</td>";
            echo "<td class='text-center'>".$data->eta."</td>";
            echo "<td class='text-center'>".$data->etd."</td>";

            echo "<td class='text-center'><b>".$status."</b></td>";
            echo "</tr>";
        }
    }
    // -------------------------------------------------------------------------------------------------
    function upload_lock_dp_new(){
        $data = $_FILES['file'];
        include $_SERVER["DOCUMENT_ROOT"].'/library/Classes/PHPExcel/IOFactory.php';
            $path = $_SERVER["DOCUMENT_ROOT"]."/file/lock_dp/";
            if($data["type"] != 'application/kset' && 
               $data["type"] != 'application/vnd.ms-excel' && 
               $data["type"] != 'application/xls' && 
               $data["type"] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' && 
               $data["type"] != 'application/ms-excel'){
                echo "<br><div class='alert alert-danger text-center'>File extention is not support, make sure that the type is <b>.xls</b></div>";
            }else{
                $datas          = move_uploaded_file($data["tmp_name"], $path.$data['name']);
                $inputFileName  = $path.$data['name'];
                $objPHPExcel    = PHPExcel_IOFactory::load($inputFileName);
                $sheetCount     = $objPHPExcel->getSheetCount();
                $sheet          = $objPHPExcel->getSheetNames();
                $sheetData      = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
                foreach($sheetData as $a => $b){
                    if($a > 1){
                        $udp = $this->db->where('c_orderline_id',$b['B'])->get('m_date_promised');
                        if($udp->num_rows() > 0){
                            $x = $udp->result()[0];
                            $x->c_order_id      = $b['A'];
                            $x->c_orderline_id  = $b['B'];
                            $x->c_bpartner_id   = $b['C'];
                            $x->lock       = $b['J'];
                            $x->date_promised = $b['I'];
                            $excel[] = $x;
                        }else{
                            $excel[] = array();
                        }                       
                    }                    
                }
                echo "<br><table class='table table-bordered'>";
                echo "<tr class='bg-green'><th class='text-center'>C_Orderline_ID</th><th class='text-center'>Date Promised</th><th class='text-center'>Status</th></tr>";
                
                foreach($excel as $a => $b){

                    if(sizeof($excel) != 0)
                        $insert = $this->update_detail_lock_dp($b);                         
                    else
                         $insert = 'There is no record found!';                      
        
                    $this->table_status_lock_dp($sheetData[$a+2]['A'],$b,$insert);
                }   
                echo "</table>";
            }
    }
    function report_bc(){
        $data['title_page'] = "REPORT";
        $data['desc_page']  = "Bea Cukai";
        $data['content']    = "report/report_bc";
        $this->load->view('layout',$data);      
    }
    function xls_report_bc(){
        $this->load->view('admin/report/xls_report_bc');
    }
    function report_status_invoice(){
        $data['title_page'] = "REPORT";
        $data['desc_page']  = "Bea Cukai";
        $data['content']    = "report/status_invoice";
        $this->load->view('layout',$data);      
    }
    function xls_status_invoice(){
        $this->load->view('admin/report/xls_status_invoice');
    }
    function list_po(){
        $draw   = $this->input->post('draw');

        //
        $supplier   = $this->listpo_model->get_datatables();
        $data       = array();
        $nomor_urut = 0;
        $i          = 0;

        foreach ($supplier as $sp) {
            $count_unconfirmed[$i]  = $this->listpo_model->get_unconfirmed($sp->user_id);

            $count_unlock[$i]       = $this->listpo_model->get_unlock($sp->user_id);
            $_temp = $sp->user_id.',"'.trim($sp->nama).'"';
            //array untuk dikirim ke datatables
            $nestedData['no']                   = (($draw-1) * 10) + (++$nomor_urut);
            $nestedData['nama']                 = $sp->nama;
            $nestedData['unconfirmed']          = intval($count_unconfirmed[$i]);
            $nestedData['unlock']               = intval($count_unlock[$i]);
            $nestedData['detail']               = "<a onclick='return detail($_temp)' href='javascript:void(0)'>Detail</a>";
            $nestedData['option']               = "<a onclick='return upload__($_temp)' href='javascript:void(0)'>Add File & Confirm</a>";
            $data[] = $nestedData;
            $i++;
        }

        //
        $output = array(
            "draw" => intval($draw),
            "recordsTotal" => intval($this->listpo_model->count_all()),
            "recordsFiltered" => intval($this->listpo_model->count_filtered()),
            "data" => $data,
        );
        echo json_encode($output);
    }
    function upload_add_file(){
        $c_bpartner_id = $this->input->post('c_bpartner_id');
        $draw   = $this->input->post('draw');

        //
        $f_header   = $this->upload_add_model->get_datatables($c_bpartner_id);
        $data       = array();
        $nomor_urut = 0;
        $i          = 0;

        foreach ($f_header as $fh) {
            $file = $this->db->limit(1)->order_by('created_date','DESC')->where('c_order_id',$fh->c_order_id)->get('m_po_file_additional');
            $eta=$this->db->where('c_order_id',$fh->c_order_id)->get('m_po_eta_etd');
            $count = $file->num_rows();
            $sh = $this->db->where('c_order_id',$fh->c_order_id)->where('status','t')->get('show_po_status')->num_rows();

            $this->db->where('c_order_id',$fh->c_order_id);
            $last = $this->db->get('show_po_status');

            $this->db2->where('c_order_id',$fh->c_order_id);
            $docstatus = $this->db2->get('adt_docstatus_po_header');
            $status = $docstatus->result()[0]->docstatus;
            
            //ETA
            if ($eta->num_rows() > 0) {
                $date_eta = $eta->result()[0]->eta;
            }else{
                $date_eta = '';
            }
            //ETD
            if ($eta->num_rows() > 0) {
                $date_etd = $eta->result()[0]->etd;
            }else{
                $date_etd = '';
            }
            //Last Confirm
            if ($last->num_rows() > 0) {
                $date_last = $last->result()[0]->created_time;
            }else{
                $date_last = '';
            }
            $_temp = $fh->c_order_id.',"'.trim($fh->documentno).'"'.',"'.trim($c_bpartner_id).'"';
            $_eta = $fh->c_order_id.',"'.trim($fh->documentno).'"'.',"'.trim($c_bpartner_id).'"';
            
            //array untuk dikirim ke datatables
            $nestedData['no']                   = (($draw-1) * 10) + (++$nomor_urut);
            $nestedData['documentno']           = $fh->documentno;
            $nestedData['docstatus']            = $status;
            if ($count==0) {
                $nestedData['status']        = '<div class="label label-danger">No Additional File</div>';
            }else{
                $nestedData['status']        = '<div class="label label-success">Uploaded</div>';
            }
            if ($count==0) {
                $nestedData['revisi']        = '<center>-</center>';
            }else{
                $nestedData['revisi']        = '<center><a href="javascript:void(0)" onclick=\'return revisi_file('.$fh->c_order_id.',"'.$fh->documentno.'")\'>'.date("d-m-Y H:i:s",strtotime($file->result()[0]->created_date)).'</a></center>';
            }
            $nestedData['upload']            = "<center><a onclick='return upload_file($_temp)' href='javascript:void(0)'><i class='fa fa-upload'></a></center>";
            $nestedData['seteta']            = "<center><a onclick='return set_eta($_eta)' href='javascript:void(0)'><i class='fa fa-calendar'></i></a></center>";
            $nestedData['date_eta']            = $date_eta;
            $nestedData['date_etd']            = $date_etd;
            if($sh == 0){
                $nestedData['show']  = '<a onclick="return show('.$fh->c_order_id.',1)" class="btn btn-xs btn-success" href="javascript::void(0)">UNCONFIRMED</a>';
            }else{
                $nestedData['show'] = '<a onclick="return show('.$fh->c_order_id.',2)" class="btn btn-xs btn-default" href="javascript:void(0)">CONFIRMED</a>';
            }
            $nestedData['date_last']            = $date_last;
            $nestedData['pi']            = "<center><a onclick='return upload_pi($_temp)' href='javascript:void(0)' class='btn btn-success  btn-xs'><i class='fa fa-upload'></i></a><a onclick='return download_pi($_temp)' href='javascript:void(0)' class='btn btn-primary btn-xs'><i class='fa fa-download'></i></a></center>";
            $data[] = $nestedData;
            
            $i++;
        }

        //
        $output = array(
            "draw" => intval($draw),
            "recordsTotal" => intval($this->upload_add_model->count_all($c_bpartner_id)),
            "recordsFiltered" => intval($this->upload_add_model->count_filtered($c_bpartner_id)),
            "data" => $data,
        );
        echo json_encode($output);
    }
    function unlock(){
        $role = $this->session->userdata('role');
        if ($role==1||$role==2) {
            $this->db->order_by('nama','asc');
            $supplier = $this->db->get('m_user');
            $data['supplier']   = $supplier;
            $data['title_page'] = "APPAREL ONE INDONESIA";
            $data['desc_page']  = "UNLOCK PACKING LIST SUPPLIER";
            $data['content']    = "unlock/unlock_pl";
            $this->load->view('layout',$data);
        }else{
            $this->load->view("404");
        }
        
    }
    function list_sup_unlock()
    {
        $role = $this->session->userdata('role');
        if ($role==1||$role==2) {
            $user['c_bpartner_id'] = $this->input->post('c_bpartner_id');
            $this->load->view('admin/unlock/unlock_pl_view',$user);
        }else{
            $this->load->view("404");
        }
    }
    function list_sup_ajax()
    {        
            $limit = $this->input->post('length');
            $start = $this->input->post('start');
            $draw   = $this->input->post('draw');
            $user = $this->input->post('c_bpartner_id');
            $posts = $this->Unlock_Model->get_datatables($user); 
            // print_r($posts);exit();
            $data = array();
            
                $nomor_urut = 0;
                foreach ($posts as $key => $post)
                {
                    $_temp = '"'.$post->no_packinglist.'","'.trim($post->kst_invoicevendor).'"'.',"'.trim($post->kst_resi).'","'.$this->session->userdata('session_id').'"';
                    $nestedData['no']                       = (($draw-1) * 10) + (++$nomor_urut);
                    $nestedData['no_packinglist']           = $post->no_packinglist;
                    $nestedData['kst_suratjalanvendor']     = $post->kst_suratjalanvendor;
                    $nestedData['kst_invoice']              = $post->kst_invoicevendor;
                    $nestedData['kst_resi']                 = $post->kst_resi;
                    $nestedData['lock']                     = "<center><a onclick='return lock($_temp)' href='javascript:void(0)' class='btn btn-xs btn-primary'><i class='fa fa-lock'></i></a></center>";

                    $data[] = $nestedData;
                }
              
            $output = array(
                "draw" => intval($this->input->post('draw')),
                "recordsTotal" => intval($this->Unlock_Model->count_all($user)),
                "recordsFiltered" => intval($this->Unlock_Model->count_filtered($user)),
                "data" => $data,
            );
            //output to json format
            echo json_encode($output); 
    }
    function upload_pi(){
        if ($_GET['token']==$this->session->userdata('session_id')) {
            $name = str_replace(array('#'),'_',$_FILES['po']['name']);
            //$data = $_GET['c_order_id'];
            $data['c_order_id'] = $_GET['c_order_id'];
            $data['file_upload_admin']  = substr(md5(date('Y-m-d H:i:s').$data['c_order_id']),0,10).'-'.$name;
            $data['c_bpartner_id'] = $this->session->userdata('user_id');
            $data['user_id']    = $this->session->userdata('user_id');
            $upl = move_uploaded_file($_FILES['po']['tmp_name'], $_SERVER['DOCUMENT_ROOT'].'/file/pi_purchase/'.$data['file_upload_admin']);
            if($upl){
                $a = $this->db->insert('m_pi_file',$data);
                echo 1;
            }else{
                echo 0;
            }
        }else{
            redirect(base_url('user/login'));
        }       
    } 
    function listpi_supplier(){
        if ($_GET['token']==$this->session->userdata('session_id')) {
            $this->load->view('admin/po/list_pi_supplier');
        }else{
            redirect(base_url('user/login'));
        } 
    } 
    function download_pi_supplier(){
        if ($_GET['token']==$this->session->userdata('session_id')) {
            $file = $this->db->where('m_pi_file_id',$_GET['id'])->get('m_pi_file')->result()[0]->file_upload_supplier;
            header('location:'.base_url('file/pi_supplier/'.$file));
        }else{
            redirect(base_url('user/login'));
        } 
    }
    function listpi_file(){
        if ($_GET['token']==$this->session->userdata('session_id')) {
            $this->db->order_by('create_date','DESC');
            $this->db->join('m_admin','m_admin.user_id = m_pi_file.user_id');
            $pi_file = $this->db->where('c_order_id',$_GET['id'])->get('m_pi_file')->result();
            $draw = "<table class='table table-striped table-bordered table-responsive'>
                        <thead class='bg-green'>
                            <tr>
                                <th width='10px'>No.</th>
                                <th>Upload Date</th>
                                <th>file Name</td>
                                <th>Uploaded By</td>
                            </tr>
                        </thead>
                        <tbody>";
            $no=1;
            foreach ($pi_file as $pi) {
                $draw .= '<tr>'.
                    '<td class="text-center">'.$no++.'</td>'.
                    '<td>'.date("d-m-Y H:i:s",strtotime($pi->create_date)).'</td>'.
                    '<td>'.str_replace(explode('-',$pi->file_upload_admin)[0].'-','',$pi->file_upload_admin).'</td>'.
                    '<td>'.$pi->nama.'</td>'.

                '</tr>';

            }

            $draw .= '</tbody></table>';
            echo $draw;
        }else{
            redirect(base_url('user/login'));
        } 
        
    }
    public function report_unlocked_pl(){
        $data['title_page'] = "APPAREL ONE INDONESIA";
        $data['desc_page']  = "REPORT UNLOCKED PACKING LIST";
        $data['content']    = "report/report_unlocked_pl";
        $this->load->view('layout',$data);
    }
    public function xls_pilih_unlocked_pl(){
        $this->load->view('admin/report/xls_pilih_unlocked_pl');
    }
    public function get_rmafabric()
     {
        // $this->db2->where_in('documentno', 'POAO1807A/E0769');
         // $this->db2->where('to_char(kst_etadate,\'YYYY-mm\')','2018-06');
         echo "<table class='table table-striped table-bordered'>";
         // $y = preg_replace( "/\r|\n/", "",trim($_POST['po']));
         $this->db2->where('to_char(kst_etadate,\'YYYY-mm\')','2018-06');
         // $this->db2->where('documentno', 'POA21806A/E2109');
         // $this->db2->where_in('documentno', $y);
         // $this->db2->distinct();
         // $this->db2->select('kst_packinglist,kst_invoicevendor,kst_resi,documentno,item,c_bpartner_id');
         $dataerp =  $this->db2->get('bw_mrc_erp_fabric_v2');
        
         foreach ($dataerp->result() as $key => $data) {
            if ($dataerp->num_rows()!=0) {
                 $this->db->where(array('no_packinglist'=>$data->kst_packinglist,'kst_invoicevendor'=>$data->kst_invoicevendor,'kst_resi'=>$data->kst_resi,'documentno'=>$data->documentno,'item'=>$data->item,'c_bpartner_id'=>$data->c_bpartner_id,'c_orderline_id'=>$data->c_orderline_id));
                $cekpodetail = $this->db->get('po_detail');
                if ($cekpodetail->num_rows()==0) {
                    if (strlen($data->season)>4) {
                        $season = NULL;
                    }else{
                        $season = $data->season;
                    }
                    $detail = array(
                        'c_order_id'           => $data->c_order_id, 
                        'c_orderline_id'       => $data->c_orderline_id, 
                        'c_bpartner_id'        => $data->c_bpartner_id, 
                        'supplier'             => $data->supplier, 
                        'documentno'           => $data->documentno, 
                        'item'                 => $data->item, 
                        'm_product_id'         => $data->m_product_id, 
                        'qtyordered'           => $data->qtyordered, 
                        'qtyentered'           => $data->qtyentered, 
                        'c_uom_id'             => $data->c_uom_id, 
                        'desc_product'         => $data->desc_product, 
                        'category'             => $data->category, 
                        'foc'                  => 0, 
                        'create_date'          => $data->kst_etadate, 
                        'type_po'              => 1, 
                        'no_packinglist'       => $data->kst_packinglist, 
                        'datepromised'         => $data->datepromised, 
                        'qty_carton'           => 1, 
                        'kst_resi'             => $data->kst_resi, 
                        'kst_suratjalanvendor' => $data->kst_suratjalanvendor, 
                        'kst_suratjalanvendor' => $data->kst_suratjalanvendor, 
                        'kst_invoicevendor'    => $data->kst_invoicevendor, 
                        'pobuyer'              => $data->pobuyer, 
                        'm_warehouse_id'       => $data->m_warehouse_id, 
                        'flag_erp'             => 't', 
                        'kst_etddate'          => $data->kst_etddate, 
                        'kst_etddate'          => $data->kst_etddate, 
                        'is_locked'            => 't', 
                        'kst_etadate'          => $data->kst_etadate, 
                        'flag_wms'             => 'f', 
                        'createdby'            => $data->created_by, 
                        'priceentered'         => $data->priceentered, 
                        'uomsymbol'            => $data->uomsymbol, 
                        'qtydelivered'         => $data->qtydelivered, 
                        'status'               => $data->status, 
                        'pricelist'            => $data->pricelist, 
                        'kst_joborder'         => $data->kst_joborder, 
                        'kst_season'           => $season, 
                        'update_by'            => 'manual', 
                        'flag_wms_get_pobuyer' => 'f'
                    );
                    $this->db->insert('po_detail',$detail);

                    $this->db->where(array('no_packinglist'=>$data->kst_packinglist,'kst_invoicevendor'=>$data->kst_invoicevendor,'kst_resi'=>$data->kst_resi,'documentno'=>$data->documentno,'item'=>$data->item,'c_bpartner_id'=>$data->c_bpartner_id,'c_orderline_id'=>$data->c_orderline_id));
                    $podetail_id = $this->db->get('po_detail')->row_array();

                    $this->db2->where(array('kst_packinglist'=>$data->kst_packinglist,'kst_invoicevendor'=>$data->kst_invoicevendor,'kst_resi'=>$data->kst_resi,'documentno'=>$data->documentno,'item'=>$data->item,'c_bpartner_id'=>$data->c_bpartner_id,'c_orderline_id'=>$data->c_orderline_id));
                    $material =  $this->db2->get('bw_mrc_erp_fabric_v2');
                    foreach ($material->result() as $key => $m) {
                        $detail_material = array(
                                'nomor_roll' => $m->kst_noroll, 
                                'qty' => $m->qty, 
                                'uomsymbol' => $m->uomsymbol, 
                                'batch_number' => $m->kst_noroll,
                                'po_detail_id'=> $podetail_id['po_detail_id'],
                                'c_orderline_id'=>$data->c_orderline_id
                            );
                        $this->db->insert('m_material',$detail_material);
                    }
                    $a = $this->db->select_sum('qty')->where('po_detail_id',$podetail_id['po_detail_id'])->get('m_material')->result();

                    foreach($a as $a){
                        $this->db->where('po_detail_id',$podetail_id['po_detail_id'])->update('po_detail',array('qty_upload'=>$a->qty));
                    }
                    echo "<tr><td>".$data->documentno.'</td><td>"'.$data->item.'"</td><td>SUKSES</td></tr>';
                }else{
                    echo "<tr><td>".$data->documentno.'</td><td>"'.$data->item.'"</td><td>SUDAH ADA</td></tr>';
                }
            }else{
                echo "<tr><td colspan='2'>PO ".$y.' TIDAK ADA DI ERP </td></tr>';
            }

         }
         echo "</table>";
     } 
    // --------------------------------function update awb-------------------------------------------------------
     public function update_awb(){
        $data['title_page'] = "Update AWB";
        $data['desc_page']  = "For EXIM";
        $data['content']    = "data/update_awb";
        $this->load->view('layout',$data);   
    }

    function xls_update_awb(){
        $this->load->view('supplier/report/xls_update_awb');
    }
    
    function edit_awb(){
        $data = $_FILES['file'];
        include $_SERVER["DOCUMENT_ROOT"].'/library/Classes/PHPExcel/IOFactory.php';
        $path = $_SERVER["DOCUMENT_ROOT"]."/file/awb/";
        if($data["type"] != 'application/kset' &&
           $data["type"] != 'application/vnd.ms-excel' &&
           $data["type"] != 'application/xls' &&
           $data["type"] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
           $data["type"] != 'application/ms-excel'){
            echo "<br><div class='alert alert-danger text-center'>File extention is not support, make sure that the type is <b>.xls</b></div>";
        }else{
            $datas          = move_uploaded_file($data["tmp_name"], $path.$data['name']);
            $inputFileName  = $path.$data['name'];
            $objPHPExcel    = PHPExcel_IOFactory::load($inputFileName);
            $sheetCount     = $objPHPExcel->getSheetCount();
            $sheet          = $objPHPExcel->getSheetNames();
            $sheetData      = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
            foreach($sheetData as $a => $b){
                if($a > 1){
      //            $where = array(
                        // 'no_packinglist'    => $b['A'],
                        // 'kst_invoicevendor' => $b['B'],
                        // 'c_bpartner_id'     => $b['D']
      //             );
                    $no_pl = $b['A'];
                    $inv   = $b['B'];
                    $bp    = $b['D'];
                //     $sql = "select * from po_detail where no_packinglist = '$no_pl' and kst_invoicevendor = '$inv' and c_bpartner_id = $bp ";
                //    $cekpodetail =  $this->db->query($sql);


                   $this->db->where('no_packinglist',$no_pl);
                   $this->db->where('kst_invoicevendor',$inv);
                   $this->db->where('c_bpartner_id',$bp);
                   $cekpodetail = $this->db->get('po_detail');

                    // $this->db->where($where);
                 //     $cekpodetail = $this->db->get('po_detail');


                    if ($cekpodetail->num_rows()>0) {
                        $x = $cekpodetail->result()[0];
                        $x->no_packinglist    = $b['A'];
                        $x->kst_invoicevendor = $b['B'];
                        $x->kst_resi          = $b['C'];
                        $x->c_bpartner_id     = $b['D'];
                        // ------------------------adding---------------------------------
                        $x->bc_no             = $b['G'];
                        $x->car_no            = $b['H'];
                        $x->hs_code           = $b['L'];
                        $x->kurs              = $b['I'];
                        $x->bc_date           = $b['J'];
                        $x->no_kpbc           = $b['K'];
                        $x->jenis_bc          = $b['N'];
                        $x->eta_port          = $b['M'];

                        $x->aoi_gross_weight  = $b['O'];
                        $x->aoi_cbm           = $b['P'];
                        $x->aoi_fcl_type      = $b['Q'];
                        $x->aoi_pod_type      = $b['R'];
                        $x->aoi_remarks       = $b['S'];
                        $x->c_currency_id      = $b['T'];
                        $x->aoi_bm            = $b['U'];
                        $x->aoi_ppn           = $b['V'];
                        $x->aoi_pph           = $b['W'];
                        // ------------------------end of adding---------------------------------
                        $excel[]              = $x;
                    }else{
                        $excel[] = array();
                    }
                }
            }
            echo "<br><table class='table table-bordered'>";
            echo "<tr class='bg-green'><th class='text-center'>No Packinglist</th><th class='text-center'>Invoice</th><th class='text-center'>AWB<th class='text-center'>Status</th></tr>";

            foreach($excel as $a => $b){
                if (!empty($b->no_packinglist)&&!empty($b->kst_invoicevendor)&&!empty($b->kst_resi)) {
                    if(sizeof($excel) != 0){
                        $insert = $this->update_awb_action($b);
                    }else{
                        $insert = 'There is no record found!';
                    }
                    $this->table_status_update_awb($sheetData[$a+2]['A'],$b,$insert);
                }


            }
            echo "</table>";
        }
    }

    private function update_awb_action($data){
        $input['kst_resi']          = $data->kst_resi;
        // ------------------------adding---------------------------------
        $input['is_ic_updated']     = 'TRUE';
        $input['ic_updated']        = date('Y-m-d H:i:s');
        $input['ic_updatedby']      = $this->session->userdata('user_id');
        $input['bc_no']             = $data->bc_no;
        $input['car_no']            = $data->car_no;
        $input['hs_code']           = $data->hs_code;
        $input['sppb_no']           = $data->sppb_no;
        $input['sppb_date']         = $data->sppb_date;
        $input['kurs']              = $data->kurs;
        $input['bc_date']           = $data->bc_date;
        $input['no_kpbc']           = $data->no_kpbc;
        $input['jenis_bc']          = $data->jenis_bc;
        $input['eta_port']          = $data->eta_port;

        $input['aoi_gross_weight']  = $data->aoi_gross_weight;
        $input['aoi_cbm']           = $data->aoi_cbm;
        $input['aoi_fcl_type']      = $data->aoi_fcl_type;
        $input['aoi_pod_type']      = $data->aoi_pod_type;
        $input['aoi_remarks']       = $data->aoi_remarks;
        $input['c_currency_id']      = $data->c_currency_id;
        $input['aoi_bm']            = $data->aoi_bm;
        $input['aoi_ppn']           = $data->aoi_ppn;
        $input['aoi_pph']           = $data->aoi_pph;
        
        // -----------------------end of adding----------------------------------
        $where['no_packinglist']    = $data->no_packinglist;
        $where['kst_invoicevendor'] = $data->kst_invoicevendor;
        $where['c_bpartner_id']     = $data->c_bpartner_id;

        $cek = $this->db->where($where)->get('po_detail');
        
        
        if($cek->num_rows() != 0){

            $this->db->where($where);
            
        // var_dump($input);
            $this->db->update('po_detail',$input);

            $where_erp = array(
                'kst_packinglist'   => $data->no_packinglist,
                'kst_invoicevendor' => $data->kst_invoicevendor,
                'c_bpartner_id'     => $data->c_bpartner_id
            );

            $this->db2->where($where_erp);
            $cekerp = $this->db2->get('kst_invoicevendor');

            
            if ($cekerp->num_rows()!=0) {
                $updated = date('Y-m-d H:i:s');
                $detail = array(
                    'kst_resi'  => $data->kst_resi,
                    'updated'   => $updated,
                    'updatedby' => '1000000'
                );
                $this->db2->where($where_erp);
                $this->db2->update('kst_invoicevendor', $detail);
                // ------------------------adding---------------------------------
                $exim = array(
                    'kst_bcno'      => $data->bc_no,
                    'kst_carno'     => $data->car_no,
                    'kst_hscode'    => $data->hs_code,
                    'updated'       => $updated,
                    'updatedby'     => '1000000',
                    'kst_npeno'     => $data->sppb_no,
                    'kst_npedate'   => $data->sppb_date,
                    'kst_kurs'      => $data->kurs,
                    'kst_bcdate'    => $data->bc_date,
                    'kst_kpbc'      => $data->no_kpbc,
                    'tipe_bc'       => $data->jenis_bc,
                    'kst_etadate2'  => $data->eta_port,
                    
                    'aoi_gross_weight'  => $data->aoi_gross_weight,
                    'aoi_cbm'           => $data->aoi_cbm,
                    'aoi_fcl_type'      => $data->aoi_fcl_type,
                    'aoi_pod_type'      => $data->aoi_pod_type,
                    'aoi_remarks'       => $data->aoi_remarks,
                    'c_currency_id'      => $data->c_currency_id,
                    'aoi_bm'            => $data->aoi_bm,
                    'aoi_ppn'           => $data->aoi_ppn,
                    'aoi_pph'           => $data->aoi_pph,
                    );

                $this->db2->where($where_erp);
                $this->db2->select('kst_invoicevendor_id');
                $this->db2->distinct();
                $this->db2->limit(1);
                $line_ic = $this->db2->get('kst_invoicevendor')->row_array()['kst_invoicevendor_id'];
                    
                    
                        $this->db2->where('kst_invoicevendor_id',$line_ic);
                        $this->db2->update('kst_invoicevendor_exim',$exim);
                // ------------------------end of adding---------------------------------
            }
            // --------------------- update resi di wms----------------------------------
            $wms_data = array(
                'no_resi' => $data->kst_resi
            );
             
                         $this->wms->where('no_packing_list',$data->no_packinglist);
                         $this->wms->where('no_invoice',$data->kst_invoicevendor);
            $check_wms = $this->wms->get('item_packing_lists')->num_rows();
                        if($check_wms > 0){
                            $this->wms->where('no_invoice',$data->kst_invoicevendor);
                            $result_wms = $this->wms->update('item_packing_lists',$wms_data);
                            if($result_wms){
                                $this->wms->where('no_invoice',$data->kst_invoicevendor);
                                $this->wms->update('material_arrivals',$wms_data);
                            }
                        }
            // --------------------- end of update resi di wms----------------------------------
            return "Success!";

        }else{
            return "No data found!";
        }
    }
    private function table_status_update_awb($id,$data=NULL,$status=0)
    {

        if ($data!=NULL) {
            echo "<tr>";
            echo "<td class='text-center'>".$data->no_packinglist."</td>";
            echo "<td class='text-center'>".$data->kst_invoicevendor."</td>";
            echo "<td class='text-center'>".$data->kst_resi."</td>";
            echo "<td class='text-center'><b>".$status."</b></td>";
            echo "</tr>";
        }
    }

    public function bp(){
        $data['title_page'] = "APPAREL ONE INDONESIA";
        $data['desc_page']  = "BUSINESS PARTNER";
        $data['content']    = "bp/index";
        $this->load->view('layout',$data);
    }

    public function add_bp(){
        $data = $_GET;
        $erp = $this->db2->where('value',$_GET['value'])->select('c_bpartner_id, name')->get('c_bpartner')->result()[0];

            $insert['user_id']    = $erp->c_bpartner_id;
            $insert['nama']       = $erp->name;
            $insert['username']   = $erp->c_bpartner_id;
            $insert['created_by']   = $this->session->userdata('user_id');
        
        $exe     = $this->db->insert('m_user',$insert);
        $exe_new = $this->db->insert('m_user_new',$insert);
        if ($exe && $exe_new) {
            redirect(base_url('admin/bp'));
        }else {
            redirect(base_url());
        }
    }
    
    public function edit_bp(){
        $data['title_page'] = "APPAREL ONE INDONESIA";
        $data['desc_page']  = "BUSINESS PARTNER";
        $data['content']    = "bp/edit_bp";
        $this->load->view('layout',$data);
    }

    public function save_edit_bp(){
        $data = $_POST;
        $data['updated_by']     = $this->session->userdata('user_id');
        $data['updated']        = date('Y-m-d H:i:s');
        
		if($data['password'] == NULL){
			unset($data['password'],$data['password2']);
		}else{
			$data['password'] = md5('KhamimNurhudaAOI-'.$data['password']);				
		}
        // $this->db->where('user_id',$data['user_id'])->update('m_user',$data); --admin hanya bisa edit m_user_new
        $this->db->where('user_id',$data['user_id'])->update('m_user_new',$data);
		redirect(base_url('admin/bp'));
    }

    function delete_bp(){
		$this->db->where('user_id',$_GET['user_id']);
        $stat = $this->db->delete('m_user');
        if($stat){
            $this->delete_bp_new();
        }
    }
    function delete_bp_new(){
        $this->db->where('user_id',$_GET['user_id']);
        $stat = $this->db->delete('m_user_new');
        redirect(base_url('admin/bp'));        
    }
    
    function bp_inactive(){
        $data = $_GET;
        $data['updated_by']     = $this->session->userdata('user_id');
        $data['updated']        = date('Y-m-d H:i:s');
        $data['isactive']       = 'false';

        $this->db->where('user_id',$_GET['user_id'])->update('m_user',$data);
        $this->db->where('user_id',$_GET['user_id'])->update('m_user_new',$data);
		redirect(base_url('admin/bp'));
    }

    function bp_active(){
        $data = $_GET;
        $data['updated_by']     = $this->session->userdata('user_id');
        $data['updated']        = date('Y-m-d H:i:s');
        $data['isactive']       = 'true';

        $this->db->where('user_id',$_GET['user_id'])->update('m_user',$data);
        $this->db->where('user_id',$_GET['user_id'])->update('m_user_new',$data);
		redirect(base_url('admin/bp'));
    }
      
// =========================================TRACKING by HUSSEIN===============================================

    function tracking(){
            
        $this->db->order_by('nama','asc');
        $supplier = $this->db->get('m_user');
        
        $data['title_page'] = "Purchase Order";
        $data['desc_page']  = "Supplier List";
        $data['content']    = "tracking/index";
        $data['supplier']   = $supplier;
        $this->load->view('layout',$data);   
    }

    function list_supplier(){
        $draw   = $this->input->post('draw');

        //
        $supplier   = $this->listpo_model->get_datatables();
        $data       = array();
        $nomor_urut = 0;
        $i          = 0;

        foreach ($supplier as $sp) {
            $count_unconfirmed[$i]  = $this->listpo_model->get_unconfirmed($sp->user_id);

            $count_unlock[$i]       = $this->listpo_model->get_unlock($sp->user_id);
            $_temp = $sp->user_id.',"'.trim($sp->nama).'"';
            //array untuk dikirim ke datatables
            $nestedData['no']                   = (($draw-1) * 10) + (++$nomor_urut);
            $nestedData['nama']                 = $sp->nama;
            $nestedData['unconfirmed']          = intval($count_unconfirmed[$i]);
            $nestedData['unlock']               = intval($count_unlock[$i]);
            $nestedData['packinglist']          = "<a onclick='return packinglist($_temp)' href='javascript:void(0)'>Packing List</a>";
            $nestedData['detail']               = "<a onclick='return detail($_temp)' href='javascript:void(0)'>Detail</a>";
            $nestedData['option']               = "<a onclick='return upload__($_temp)' href='javascript:void(0)'>Add File & Confirm</a>";
            $data[] = $nestedData;
            $i++;
        }

        //
        $output = array(
            "draw" => intval($draw),
            "recordsTotal" => intval($this->listpo_model->count_all()),
            "recordsFiltered" => intval($this->listpo_model->count_filtered()),
            "data" => $data,
        );
        echo json_encode($output);
    }

    function detail_packinglist(){
        $id = $_GET['id'];
        $data = array(
            'id' => $id,
        );
        $this->load->view('admin/tracking/detail_pl',$data);
    }

    function packinglist(){
        $from = $this->input->post('from');
        $to = $this->input->post('to');
        $limit = $this->input->post('length');
        $start = $this->input->post('start');

        if($from!='' && $to!='')
        {
            $from = date('Y-m-d',strtotime($from));
            $to = date('Y-m-d',strtotime($to));
        }
        
        $user = $this->input->post('id');
        // $user = $this->session->userdata('user_id');
        $posts = $this->packinglist_model->get_datatables($user,$from,$to,$user);
        // print_r($posts);exit();
        $data = array();
            $no = 1;


            foreach ($posts as $key => $post)
            {

                $nestedData['no'] 	= $no;
                $nestedData['no_packinglist'] = $post->no_packinglist;
                $nestedData['print_packing_list'] = base_url('data/print_pl?token='.$this->session->userdata('session_id').'&no_packinglist='.$post->no_packinglist.'&invoice='.$post->kst_invoicevendor.'&sj='.$post->kst_suratjalanvendor.'&awb='.str_replace(' ', '+', $post->kst_resi));
                $nestedData['kst_invoicevendor'] = $post->kst_invoicevendor;
                $nestedData['qc_check_report'] = $post->packing_list_total;
                $nestedData['created_at'] = $post->packing_list_total;
                $nestedData['kst_suratjalanvendor'] = $post->kst_suratjalanvendor;
                $nestedData['url_upload'] = base_url('data/up_pl_qc');
                $nestedData['kst_resi'] = $post->kst_resi;
                $nestedData['url_download'] = base_url('data/download_qc_check/?token='.$this->session->userdata('session_id').'&no_packinglist='.trim($post->no_packinglist).'&kst_suratjalanvendor='.$post->kst_suratjalanvendor);
                $nestedData['url_resi'] = base_url('admin/xml_track/'.$post->kst_resi);
                $nestedData['type_po'] = $post->type_po;
                $nestedData['is_locked'] = $post->is_locked;
                $nestedData['data'] = 'pl='.$post->no_packinglist.'&sj='.$post->kst_suratjalanvendor.'&inv='.$post->kst_invoicevendor.'&awb='.str_replace(' ', '+', $post->kst_resi).'&token='.$this->session->userdata('session_id');
                $nestedData['url_print_fb'] = base_url('label/label_fb_new?token='.$this->session->userdata('session_id').'&nopl='.$post->no_packinglist).'&sj='.$post->kst_suratjalanvendor.'&inv='.$post->kst_invoicevendor.'&awb='.$post->kst_resi;
                $nestedData['url_print_acc'] = base_url('label/acc?token='.$this->session->userdata('session_id').'&nopl='.$post->no_packinglist).'&sj='.$post->kst_suratjalanvendor.'&inv='.$post->kst_invoicevendor.'&awb='.$post->kst_resi;
                $nestedData['url_edit_header'] = base_url('data/edit_header/?token='.$this->session->userdata('session_id').'&no_packinglist='.trim($post->no_packinglist)."&kst_invoicevendor=".$post->kst_invoicevendor.'&awb='.$post->kst_resi);
                $nestedData['url_isactive'] = base_url('data/isactive');
                $nestedData['url_lock'] = base_url('data/lock');
                $nestedData['kst_resi'] = $post->kst_resi;
                $no++;
                $data[] = $nestedData;

            }

        $output = array(
            "draw" => intval($this->input->post('draw')),
            "recordsTotal" => intval($this->packinglist_model->count_all($user)),
            "recordsFiltered" => intval($this->packinglist_model->count_filtered($user,$from,$to)),
            "data" => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    function xml_track($awb){
        $panjang = strlen($awb);
        
        //DHL
        if($panjang == 10){ 
            $url = 'http://xmlpi-ea.dhl.com/XMLShippingServlet';

            $input_xml = '
                <Request>
                    <ServiceHeader>
                        <MessageTime>2002-06-25T11:28:56-08:00</MessageTime>
                        
            <MessageReference>1234567890123456789012345678</MessageReference>
                        <SiteID>v62_MLBHjCSIgs</SiteID>
                        <Password>Zhf1Qi8MNj</Password>
                    </ServiceHeader>
                </Request>
                <LanguageCode>en</LanguageCode>
                <AWBNumber>'.$awb.'</AWBNumber>
                <LevelOfDetails>ALL_CHECK_POINTS</LevelOfDetails>
                <PiecesEnabled>S</PiecesEnabled>';
            
            //setting the curl parameters.
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            
            // Following line is compulsary to add as it is:
            curl_setopt($ch, CURLOPT_POSTFIELDS,
                        '<?xml version="1.0" encoding="UTF-8"?>
                        <req:KnownTrackingRequest xmlns:req="http://www.dhl.com" 
                                                
                        xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                                                
                        xsi:schemaLocation="http://www.dhl.com
                                                TrackingRequestKnown.xsd">' . $input_xml.'
                                                </req:KnownTrackingRequest>');
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
            $data = curl_exec($ch);
            curl_close($ch);

            //convert the XML result into array
            $array_data = json_decode(json_encode(simplexml_load_string($data)), true);

            // print_r('<pre>');
            // print_r($array_data);
            // print_r('</pre>');
            if(!empty($array_data['AWBInfo']['Status']['ActionStatus'])){
                
                if($array_data['AWBInfo']['Status']['ActionStatus']=='success'){
                    $alert = '200';
                }
                else{
                    $alert = '404';
                } 
            }
            else{
                if($array_data['Response']['Status']['ActionStatus']=='Failure'){
                    $alert = '404';
                }
                else{
                    $alert = '404';
                } 
            } 
            
            $data = array(
                'hasil'     => $array_data,
                'status'    => $alert 
            );
            
            
            $this->load->view('admin/tracking/detail_track',$data);
        }
        
        //FEDEX
        if($panjang == 12){

            // require_once('fedex-common.php5');

            //The WSDL is not included with the sample code.
            //Please include and reference in $path_to_wsdl variable.
            
            
            $paths = base_url()."file/";

            $path_to_wsdl = $paths."TrackService_v18.wsdl";

            ini_set("soap.wsdl_cache_enabled", "0");

            $opts = array(
                'ssl' => array('verify_peer' => false, 'verify_peer_name' => false)
                );
            $client = new SoapClient($path_to_wsdl, array('trace' => 1,'stream_context' => stream_context_create($opts)));  // Refer to http://us3.php.net/manual/en/ref.soap.php for more information

            $request['WebAuthenticationDetail'] = array(
                'ParentCredential' => array(
                    'Key' => 'NBvNjtZycTdIMqyv', 
                    'Password' => 'RAJ1kusHISaRaV7Yoeg565Iqa'
                ),
                'UserCredential' => array(
                    'Key' => 'NBvNjtZycTdIMqyv', 
                    'Password' => 'RAJ1kusHISaRaV7Yoeg565Iqa'
                )
            );

            $request['ClientDetail'] = array(
                'AccountNumber' => '510087500', 
                'MeterNumber' => '114040040'
            );
            $request['TransactionDetail'] = array('CustomerTransactionId' => '*** Track Request using PHP ***');
            $request['Version'] = array(
                'ServiceId' => 'trck', 
                'Major' => '18', 
                'Intermediate' => '0', 
                'Minor' => '0'
            );
            $request['SelectionDetails'] = array(
                'PackageIdentifier' => array(
                    'Type' => 'TRACKING_NUMBER_OR_DOORTAG',
                    'Value' => $awb // Replace 'XXX' with a valid tracking identifier
                )
            );

            // if(setEndpoint('changeEndpoint')){
            //     $newLocation = $client->__setLocation(setEndpoint('endpoint'));
            // }
            
            $response = $client->track($request);

            // print_r('<pre>');
            // print_r($array_data);
            // print_r('</pre>');
            if(!empty($response->HighestSeverity)){
                
                if($response->HighestSeverity=='SUCCESS'){
                    $alert = '200';
                }
                else{
                    $alert = '404';
                } 
            }
            else{
                if($response->HighestSeverity=='FAILURE'){
                    $alert = '404';
                }
                else{
                    $alert = '404';
                } 
            }

            $data = array(
                'hasil'     => $response,
                'status'    => $alert 
            );
            
            print_r($response);
            die();
            $this->load->view('admin/tracking/detail_track_fedex',$data);
        }
    }
    
    function xml_track_lama($awb){

        $url = 'http://xmlpi-ea.dhl.com/XMLShippingServlet';

        $input_xml = '
            <Request>
                <ServiceHeader>
                    <MessageTime>2002-06-25T11:28:56-08:00</MessageTime>
                    
        <MessageReference>1234567890123456789012345678</MessageReference>
                    <SiteID>v62_MLBHjCSIgs</SiteID>
                    <Password>Zhf1Qi8MNj</Password>
                </ServiceHeader>
            </Request>
            <LanguageCode>en</LanguageCode>
            <AWBNumber>'.$awb.'</AWBNumber>
            <LevelOfDetails>ALL_CHECK_POINTS</LevelOfDetails>
            <PiecesEnabled>S</PiecesEnabled>';
        
        // var_dump($input_xml);
        //setting the curl parameters.
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        
        // Following line is compulsary to add as it is:
        curl_setopt($ch, CURLOPT_POSTFIELDS,
                    '<?xml version="1.0" encoding="UTF-8"?>
                    <req:KnownTrackingRequest xmlns:req="http://www.dhl.com" 
                                            
                    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" 
                                            
                    xsi:schemaLocation="http://www.dhl.com
                                            TrackingRequestKnown.xsd">' . $input_xml.'
                                            </req:KnownTrackingRequest>');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);
        $data = curl_exec($ch);
        curl_close($ch);

        //convert the XML result into array
        $array_data = json_decode(json_encode(simplexml_load_string($data)), true);

        // print_r('<pre>');
        // print_r($array_data);
        // print_r('</pre>');
        if(!empty($array_data['AWBInfo']['Status']['ActionStatus'])){
            
            if($array_data['AWBInfo']['Status']['ActionStatus']=='success'){
                $alert = '200';
            }
            else{
                $alert = '404';
            } 
        }
        else{
            if($array_data['Response']['Status']['ActionStatus']=='Failure'){
                $alert = '404';
            }
            else{
                $alert = '404';
            } 
        } 
        
        $data = array(
            'hasil'     => $array_data,
            'status'    => $alert 
        );
        
        
        $this->load->view('admin/tracking/detail_track',$data);
    }



    function pl_detail(){
        $this->load->view('admin/tracking/detail_invoice',$_POST);
    }
} 