<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
class Data extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$CI = &get_instance();
		$this->db2 = $CI->load->database('db2', TRUE);
		$this->load->model(array('data_model', 'packinglist_model', 'm_activepo', 'm_new_pl'));
	}

	public function index()
	{
		$data['title_page'] = "DATA";
		$data['desc_page']	= "HASIL UPLOAD";
		$data['content']	= "data/index";
		$this->load->view('layout', $data);
	}

	public function home()
	{
		$data['title_page'] = "PURCHASE ORDER";
		$data['desc_page']	= "WELCOME";
		$data['content']	= "data/home";
		$this->load->view('layout', $data);
	}

	public function po()
	{
		$data['title_page'] = "PURCHASE ORDER";
		$data['desc_page']	= "ACTIVE";
		$data['content']	= "data/po";
		$this->load->view('layout', $data);
	}

	function po_detail($c_order_id = 0)
	{
		$data['title_page'] = "PURCHASE ORDER";
		$data['desc_page']	= "DETAIL";
		$data['content']	= "data/po_detail";
		$data['c_order']	= $c_order_id;
		$this->load->view('layout', $data);
	}

	function form_upload($id = 0, $c_order = 0)
	{
?>
		<form method="POST" action="<?= base_url('data/upload_pl/' . $id . '/' . $c_order); ?>" enctype="multipart/form-data">
			<label>Packing List</label>
			<input type="file" name="data" class="form-control" placeholder="Insert Quantity FOC" required>
			<br>
			<button class="btn btn-success" name="upload">Save Changes</button>
		</form>
		<?php
	}

	function input_nopl($id)
	{
		$data = $this->db2->where('c_orderline_id', $id)->get('f_web_po_detail')->result()[0];
		$data->no_packinglist = $_POST['nopl'];
		$po_header = $this->db->where('c_order_id', $data->c_order_id)->get('po_header')->num_rows();
		if ($po_header == 0) {
			$header = $this->db2->where('c_order_id', $data->c_order_id)->get('f_web_po_header')->result()[0];
			$insert_header = $this->db->insert('po_header', $header);
		}
		$po_detail = $this->db->where('c_orderline_id', $data->c_orderline_id)->get('po_detail')->num_rows();
		if ($po_detail == 0) {
			$this->db->insert('po_detail', $data);
		} else {
			$this->db->where('c_orderline_id', $data->c_orderline_id)->update('po_detail', $data);
		}
	}
	function input_nopl_($id)
	{
		$data['no_packinglist'] = $_POST['nopl'];
		$this->db->where('po_detail_id', $id)->update('po_detail', $data);
	}
	function upload_pl($id = 0)
	{
		if ($id != 0) {
			include $_SERVER["DOCUMENT_ROOT"] . '/library/Classes/PHPExcel/IOFactory.php';
			if (isset($_POST["upload"])) {
				$path = $_SERVER["DOCUMENT_ROOT"] . "/file/";
				if (
					$_FILES["data"]["type"] != 'application/kset' &&
					$_FILES["data"]["type"] != 'application/vnd.ms-excel' &&
					$_FILES["data"]["type"] != 'application/xls' &&
					$_FILES["data"]["type"] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
					$_FILES["data"]["type"] != 'application/ms-excel'
				) {
					echo "<div class='alert alert-danger text-center'>File tidak support, pastikan <b>.xls</b></div>";
					echo "<div><a href='import'>Back</a></div>";
				} else {
					$data 			= move_uploaded_file($_FILES["data"]["tmp_name"], $path . $_FILES['data']['name']);
					$inputFileName 	= $path . $_FILES['data']['name'];
					$objPHPExcel 	= PHPExcel_IOFactory::load($inputFileName);
					$sheetCount 	= $objPHPExcel->getSheetCount();
					$sheet 			= $objPHPExcel->getSheetNames();
					$sheetData 		= $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
					$data = array();
					foreach ($sheetData as $a => $b) {
						if ($a > 1) {
							$data = array(
								'po_detail_id' 		=> $id,
								'nomor_roll' 		=> $b['A'],
								'qty'	 	 		=> $b['B'],
								'uomsymbol'			=> $b['C'],
								'batch_number'	 	=> $b['D']
							);
							$this->db->insert('m_material', $data);
						}
					}
				}
			}
			redirect(base_url('data/po_detail/' . $c_order . '?status=success'));
		}
	}
	function new_pl_submit()
	{
		$type = $_POST['type_po'];
		unset($_POST['type_po']);
		$data = $_POST;
		$no_packinglist 		= preg_replace('/\s+/', '', $data['no_packinglist']);
		$kst_suratjalanvendor	= preg_replace('/\s+/', '', $data['kst_suratjalanvendor']);
		if ($data['kst_invoicevendor'] == '-') {
			$kst_invoicevendor  = preg_replace('/\s+/', '', $data['no_packinglist']);
		} else {
			$kst_invoicevendor	= preg_replace('/\s+/', '', $data['kst_invoicevendor']);
		}
		$kst_resi				= preg_replace('/\s+/', '', $data['kst_resi']);
		$kst_etddate			= $data['kst_etddate'];
		$kst_etadate			= $data['kst_etadate'];
		unset($data['no_packinglist'], $data['kst_suratjalanvendor'], $data['kst_invoicevendor'], $data['kst_resi'], $data['kst_etddate'], $data['kst_etadate']);
		foreach ($data as $a => $b) {
			$c_orderline[] = $a;
		}
		$cek_lock = $this->db->query("select * from po_detail where no_packinglist='$no_packinglist' and kst_invoicevendor='$kst_invoicevendor' and kst_resi='$kst_resi' and is_locked='t'");
		if ($cek_lock->num_rows() == 0) {
			if (isset($c_orderline)) {
				$this->db2->where_in('c_orderline_id', $c_orderline);
				$asli = $this->db2->get('f_web_po_detail');
				$this->db->trans_start();
				foreach ($asli->result() as $b) {
					///////------------------------------------------------------------------------------------------------------
					$po_header = $this->db->where('c_order_id', $b->c_order_id)->get('po_header');
					if ($po_header->num_rows() == 0) {
						$aa = $this->db2->where('c_order_id', $b->c_order_id)->get('f_web_po_header')->result()[0];
						$this->db->insert('po_header', $aa);
					}
					$this->copy_line($b->c_orderline_id);
					$b->no_packinglist 			= $no_packinglist;
					$b->kst_suratjalanvendor 	= $kst_suratjalanvendor;
					$b->kst_invoicevendor 		= $kst_invoicevendor;
					$b->kst_resi				= $kst_resi;
					$b->kst_etddate				= $kst_etddate;
					$b->kst_etadate				= $kst_etadate;
					if ($type == 2) {
						$b->qty_upload 			= $data[$b->c_orderline_id]['qd'];
						$b->qty_carton 			= $data[$b->c_orderline_id]['car'];
					}
					$b->foc 					= $data[$b->c_orderline_id]['foc'];
					$ins = $this->db->insert('po_detail', $b);
					if ($type == 1) {
						$where['c_order_id'] 	 = $b->c_order_id;
						$where['c_orderline_id'] 	 = $b->c_orderline_id;
						$where['no_packinglist'] = $b->no_packinglist;
						$file = $_FILES[$b->c_orderline_id];
						$cek_id = $this->db->where($where)->order_by('po_detail_id', 'DESC')->limit(1)->get('po_detail')->result()[0]->po_detail_id;
						$this->upload_pl_fb($cek_id, $file);
					}
					/////
				}
				$this->db->trans_complete();
			}
			redirect(base_url('data/pl_list_ori'));
		} else {
			$this->session->set_flashdata(
				'lock',
				'<div class="alert alert-danger alert-dismissable">
                	<i class="fa fa-ban"></i>
                	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                    <h4>Sorry</h4>
                    <p>Packinglist Already Lock</p>
                </div>'
			);
			redirect(base_url('data/new_pl'));
		}
	}
	function upload_pl_fb($id = 0, $data)
	{
		if ($id != 0) {
			include $_SERVER["DOCUMENT_ROOT"] . '/library/Classes/PHPExcel/IOFactory.php';
			$path = $_SERVER["DOCUMENT_ROOT"] . "/file/";
			if (
				$data["type"] != 'application/kset' &&
				$data["type"] != 'application/vnd.ms-excel' &&
				$data["type"] != 'application/xls' &&
				$data["type"] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
				$data["type"] != 'application/ms-excel'
			) {
				echo "<div class='alert alert-danger text-center'>File tidak support, pastikan <b>.xls</b></div>";
				echo "<div><a href='import'>Back</a></div>";
			} else {
				$datas 			= move_uploaded_file($data["tmp_name"], $path . $data['name']);
				$inputFileName 	= $path . $data['name'];
				$objPHPExcel 	= PHPExcel_IOFactory::load($inputFileName);
				$sheetCount 	= $objPHPExcel->getSheetCount();
				$sheet 			= $objPHPExcel->getSheetNames();
				$sheetData 		= $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
				$datas = array();
				foreach ($sheetData as $a => $b) {
					if ($a > 1) {
						$datas = array(
							'po_detail_id' 		=> $id,
							'nomor_roll' 		=> $b['A'],
							'qty'	 	 		=> $b['B'],
							'uomsymbol'			=> $b['C'],
							'batch_number'	 	=> $b['D']
						);
						$this->db->insert('m_material', $datas);
					}
					$a = $this->db->select_sum('qty')->where('po_detail_id', $id)->get('m_material')->result();
					foreach ($a as $ba) {
						$this->db->where('po_detail_id', $id)->update('po_detail', array('qty_upload' => $ba->qty));
					}
				}
			}
		}
	}
	function input_qty($id = 0)
	{
		$data = $this->db2->where('c_orderline_id', $id)->get('f_web_po_detail')->result()[0];
		$data->qty_upload = $_POST['qty'];
		$po_header = $this->db->where('c_order_id', $data->c_order_id)->get('po_header')->num_rows();
		if ($po_header == 0) {
			$header = $this->db2->where('c_order_id', $data->c_order_id)->get('f_web_po_header')->result()[0];
			$insert_header = $this->db->insert('po_header', $header);
		}
		$po_detail = $this->db->where('c_orderline_id', $data->c_orderline_id)->get('po_detail')->num_rows();
		if ($po_detail == 0) {
			$this->db->insert('po_detail', $data);
		} else {
			$this->db->where('c_orderline_id', $data->c_orderline_id)->update('po_detail', $data);
		}
	}
	function input_qty_($id = 0)
	{
		$data['qty_upload'] = $_POST['qty'];
		$this->db->where('po_detail_id', $id)->update('po_detail', $data);
	}
	function input_foc($id = 0)
	{
		$data = $this->db2->where('c_orderline_id', $id)->get('f_web_po_detail')->result()[0];
		$data->foc = $_POST['foc'];
		$po_header = $this->db->where('c_order_id', $data->c_order_id)->get('po_header')->num_rows();
		if ($po_header == 0) {
			$header = $this->db2->where('c_order_id', $data->c_order_id)->get('f_web_po_header')->result()[0];
			$insert_header = $this->db->insert('po_header', $header);
		}
		$po_detail = $this->db->where('c_orderline_id', $data->c_orderline_id)->get('po_detail')->num_rows();
		if ($po_detail == 0) {
			$this->db->insert('po_detail', $data);
		} else {
			$this->db->where('c_orderline_id', $data->c_orderline_id)->update('po_detail', $data);
		}
	}
	function input_foc_($id = 0)
	{
		$data['foc'] = $_POST['foc'];
		$this->db->where('po_detail_id', $id)->update('po_detail', $data);
	}
	function input_carton($id = 0)
	{
		$data = $this->db2->where('c_orderline_id', $id)->get('f_web_po_detail')->result()[0];
		$data->qty_carton = $_POST['qty_carton'];
		$po_header = $this->db->where('c_order_id', $data->c_order_id)->get('po_header')->num_rows();
		if ($po_header == 0) {
			$header = $this->db2->where('c_order_id', $data->c_order_id)->get('f_web_po_header')->result()[0];
			$insert_header = $this->db->insert('po_header', $header);
		}
		$po_detail = $this->db->where('c_orderline_id', $data->c_orderline_id)->get('po_detail')->num_rows();
		if ($po_detail == 0) {
			$this->db->insert('po_detail', $data);
		} else {
			$this->db->where('c_orderline_id', $data->c_orderline_id)->update('po_detail', $data);
		}
	}
	function input_carton_($id = 0)
	{
		$data->qty_carton = $_POST['qty_carton'];
		$this->db->where('po_detail_id', $id)->update('po_detail', $data);
	}

	function input_sj($id)
	{
		$data = $this->db2->where('c_orderline_id', $id)->get('f_web_po_detail')->result()[0];
		$data->kst_suratjalanvendor = $_POST['sj'];
		$po_header = $this->db->where('c_order_id', $data->c_order_id)->get('po_header')->num_rows();
		if ($po_header == 0) {
			$header = $this->db2->where('c_order_id', $data->c_order_id)->get('f_web_po_header')->result()[0];
			$insert_header = $this->db->insert('po_header', $header);
		}
		$po_detail = $this->db->where('c_orderline_id', $data->c_orderline_id)->get('po_detail')->num_rows();
		if ($po_detail == 0) {
			$this->db->insert('po_detail', $data);
		} else {
			$this->db->where('c_orderline_id', $data->c_orderline_id)->update('po_detail', $data);
		}
	}
	function input_sj_($id)
	{
		$data['kst_suratjalanvendor'] = $_POST['sj'];
		$this->db->where('po_detail_id', $id)->update('po_detail', $data);
	}

	function input_inv($id)
	{
		$data = $this->db2->where('c_orderline_id', $id)->get('f_web_po_detail')->result()[0];
		$data->kst_invoicevendor = $_POST['inv'];
		$po_header = $this->db->where('c_order_id', $data->c_order_id)->get('po_header')->num_rows();
		if ($po_header == 0) {
			$header = $this->db2->where('c_order_id', $data->c_order_id)->get('f_web_po_header')->result()[0];
			$insert_header = $this->db->insert('po_header', $header);
		}
		$po_detail = $this->db->where('c_orderline_id', $data->c_orderline_id)->get('po_detail')->num_rows();
		if ($po_detail == 0) {
			$this->db->insert('po_detail', $data);
		} else {
			$this->db->where('c_orderline_id', $data->c_orderline_id)->update('po_detail', $data);
		}
	}
	function input_inv_($id)
	{
		$data['kst_invoicevendor'] = $_POST['inv'];
		$this->db->where('po_detail_id', $id)->update('po_detail', $data);
	}
	function input_resi($id)
	{
		$data = $this->db2->where('c_orderline_id', $id)->get('f_web_po_detail')->result()[0];
		$data->kst_resi = $_POST['resi'];
		$po_header = $this->db->where('c_order_id', $data->c_order_id)->get('po_header')->num_rows();
		if ($po_header == 0) {
			$header = $this->db2->where('c_order_id', $data->c_order_id)->get('f_web_po_header')->result()[0];
			$insert_header = $this->db->insert('po_header', $header);
		}
		$po_detail = $this->db->where('c_orderline_id', $data->c_orderline_id)->get('po_detail')->num_rows();
		if ($po_detail == 0) {
			$this->db->insert('po_detail', $data);
		} else {
			$this->db->where('c_orderline_id', $data->c_orderline_id)->update('po_detail', $data);
		}
	}
	function input_resi_($id)
	{
		$data['kst_resi'] = $_POST['resi'];
		$this->db->where('po_detail_id', $id)->update('po_detail', $data);
	}

	function detail($id = 0)
	{
		$a = $this->db->where('po_detail_id', $id)->get('m_material');
		if ($a->num_rows() > 0) {
		?>
			<table class="table table-striped table-bordered">
				<thead class='bg-green'>
					<th class="text-center">#</th>
					<th>ROLL NUM</th>
					<th class="text-right">QTY</th>
					<th>UOM</th>
					<th>BATCH</th>
				</thead>
				<tbody>
					<?php
					$nomor = 1;
					foreach ($a->result() as $b) {
					?>
						<tr>
							<td class="text-center"><?= $nomor++; ?></td>
							<td><?= $b->nomor_roll; ?></td>
							<td class="text-right"><?= $b->qty; ?></td>
							<td><?= $b->uomsymbol; ?></td>
							<td><?= $b->batch_number; ?></td>
						</tr>
					<?php } ?>
				</tbody>
			</table>
<?php
		} else {
			echo "<h3>No Material</h3>";
		}
	}
	function add_pl($id = 0)
	{
		$this->db->where('master', 1);
		$this->db->where('po_detail_id', $id);
		$a = $this->db->get('po_detail');
		foreach ($a->result() as $b) {
			$b->master 			= 2;
			$b->no_packinglist  = NULL;
			$b->po_detail_id 	= 1;
			$b->qty_carton 		= NULL;
			$b->foc 	 		= NULL;
			$this->db->insert('po_detail', $b);
		}
		redirect(base_url('data/po_detail/' . $b->c_order_id));
	}
	function complete()
	{
		print_r($_GET);
	}
	function new_pl()
	{
		if (isset($_POST['m_warehouse_id']) && $_POST['m_warehouse_id'] != NULL) {
			$detail = json_decode(base64_decode($_POST['detail_list']));
			$warehouse = json_decode(base64_decode($_POST['warehouse_list']));

			$users = $this->m_new_pl->get_detail($_POST['m_warehouse_id'], 1);
			// $map = $this->m_new_pl->get_adt_mapping_bp();
			if (count($users) > 0) {
				$data['users']	= $users[0]->type_po;
				$data['select_warehouse'] = $_POST['m_warehouse_id'];
				$data['detail_type_po'] = $detail[0]->type_po;
			} else {
				$data['users'] = NULL;
				$data['select_warehouse'] = NULL;
				$data['detail_type_po'] = NULL;
			}
			// $data['map'] = $map;
		} else {
			$detail = $this->m_new_pl->get_detail(NULL, 1);
			if (count($detail) != 0) {
				$warehouse = $this->m_new_pl->get_warehouse($detail[0]->type_po);
			} else {
				$warehouse = 0;
			}
		}
		$data['detail'] 	= $detail;
		$data['warehouse']  = $warehouse;
		$data['title_page'] = "PACKING LIST";
		$data['desc_page']	= "Create New";
		$data['content']	= "data/new_pl";
		$this->load->view('layout', $data);
	}
	function show_line()
	{
		$this->load->view('supplier/data/pl_itemlist', $_POST);
	}
	function show_line_awb()
	{
		$this->load->view('supplier/data/awb_detail', $_POST);
	}
	//DETAIL PACKING LIST
	function pl_list()
	{
		$data['title_page'] = "PACKING LIST";
		$data['desc_page']	= "All";
		$data['content']	= "data/detail/pl_list";
		$this->load->view('layout', $data);
	}
	function pl_list_ori()
	{
		$data['title_page'] = "PACKING LIST";
		$data['desc_page']	= "All";
		$data['content']	= "data/detail/pl_list_ori";
		$this->load->view('layout', $data);
	}
	function pl_detail()
	{
		$this->load->view('supplier/data/detail/pl_itemdetail', $_POST);
	}
	function up_pl_qc()
	{
		$file = $_FILES['file'];
		$nama = substr(md5(date('Y-m-d H:i:s') . 'KhaMIMNurhuda'), 0, 10) . '-' . $file['name'];
		$data = $_POST;
		$data['file'] = $nama;
		$a = $this->db->insert('packing_list_qc', $data);
		if ($a)
			move_uploaded_file($file['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . '/file/qc/' . $nama);
		echo 1;
	}
	function report_status_po_sum_acc()
	{
		$data['title_page'] = "REPORT";
		$data['desc_page']	= "Status PO Summary";
		$data['content']	= "report/status_po_sum_acc";
		$this->load->view('layout', $data);
	}
	function report_status_po_sum()
	{
		$data['title_page'] = "REPORT";
		$data['desc_page']	= "Status PO Summary";
		$data['content']	= "report/status_po_sum";
		$this->load->view('layout', $data);
	}
	function delete_pl_list()
	{
		$this->db->where('po_detail_id', $_GET['po_detail_id']);
		$this->db->update('po_detail', array('isactive' => 'false'));
		redirect(base_url('data/pl_list_ori'));
	}

	function isactive()
	{
		$post = $this->input->post();
		$where = array(
			'c_bpartner_id' => $this->session->userdata('user_id'),
			'no_packinglist' => $post['pl'],
			'kst_invoicevendor' => $post['inv'],
			'kst_resi' => $post['awb'],
			'isactive' => 't'
		);
		if ($post['token'] == $this->session->userdata('session_id')) {
			$this->db->where($where);
			$cek_lock = $this->db->get('po_detail');
			if ($cek_lock->result()[0]->is_locked == 'f') {
				$this->db->where($where);
				$this->db->update('po_detail', array('isactive' => 'false'));
				echo 1;
			} else {
				echo 0;
			}
		} else {
			redirect(base_url('user/login'));
		}
	}

	// function lock(){
	// 	$post = $this->input->post();
	// 	$where = array(
	// 					'c_bpartner_id'=>$this->session->userdata('user_id'),
	// 					'no_packinglist'=>$post['pl'],
	// 					'kst_resi'=>$post['awb'],
	// 					'kst_invoicevendor'=>$post['inv'],
	// 					'isactive' =>'t'
	// 				);
	// 	if ($post['token']==$this->session->userdata('session_id')) {
	// 		$this->db->where($where);
	// 		$cek = $this->db->get('po_detail');
	// 		if($cek->result()[0]->is_locked=='f'){
	// 			$this->db->where($where);
	// 			$this->db->update('po_detail',array('is_locked'=>'true'));
	// 			echo 1;
	// 		}else{
	// 			echo 0;
	// 		}
	// 	}else{
	// 		redirect(base_url('user/login'));
	// 	}
	// }

	function lock()
	{
		$post = $this->input->post();
		$where = array(
			'c_bpartner_id' => $this->session->userdata('user_id'),
			'no_packinglist' => $post['pl'],
			'kst_resi' => $post['awb'],
			'kst_invoicevendor' => $post['inv'],
			'isactive' => 't'
		);
		if ($post['token'] == $this->session->userdata('session_id')) {
			$this->db->where($where);
			$cek = $this->db->get('po_detail');
			if ($cek->result()[0]->is_locked == 'f') {
				if ($cek->result()[0]->status_po == 'CO') {
					$this->db->where($where);
					$this->db->update('po_detail', array('is_locked' => 'true'));
					echo 1;
				} else {
					echo "PO Status not yet completed!";
				}
			} else {
				echo 0;
			}
		} else {
			redirect(base_url('user/login'));
		}
	}

	function unlock()
	{
		$this->db->where('no_packinglist', $_GET['no_packinglist']);
		$this->db->update('po_detail', array('is_locked' => 'false'));

		redirect(base_url('data/pl_list_today'));
	}

	function edit_header()
	{
		$this->load->view('supplier/data/edit/edit_header');
	}
	function edit_header_submit()
	{
		$post = $this->input->post();
		$where = array(
			'no_packinglist' => $post['no_packinglist_before'],
			'kst_invoicevendor' => $post['invoice_before'],
			'kst_resi' => $post['awb'],
			'isactive' => 't'
		);
		$this->db->where($where);
		$cek_lock = $this->db->get('po_detail')->result()[0]->is_locked;
		if ($cek_lock == 'f') {
			if (empty($post['kst_etadate'])) {
				$detail_edit = array(
					'kst_invoicevendor' => preg_replace('/\s+/', '', $post['kst_invoicevendor']),
					'kst_etddate' => $post['kst_etddate']
				);
				//'kst_etadate' => $post['kst_etadate']);
				$this->db->where($where);
				$this->db->set($detail_edit);
				$this->db->update('po_detail');

				$this->db->where($where);
				$this->db->set('kst_invoicevendor', preg_replace('/\s+/', '', $post['kst_invoicevendor']));
				$this->db->update('packing_list_qc');
				echo 1;
			} else {
				$detail_edit = array(
					'kst_invoicevendor' => preg_replace('/\s+/', '', $post['kst_invoicevendor']),
					'kst_etddate' => $post['kst_etddate'],
					'kst_etadate' => $post['kst_etadate']
				);
				$this->db->where($where);
				$this->db->set($detail_edit);
				$this->db->update('po_detail');

				$this->db->where($where);
				$this->db->set('kst_invoicevendor', preg_replace('/\s+/', '', $post['kst_invoicevendor']));
				$this->db->update('packing_list_qc');
				echo 1;
			}
		} else {
			echo 0;
		}
	}
	function edit_pl_list()
	{
		$this->load->view('supplier/data/edit/edit_detail');
	}
	function edit_detail_submit()
	{
		$this->db->where($_POST['where'])->update('po_detail', $_POST['after']);
		redirect(base_url('data/pl_list_ori'));
	}
	function download_qc_check()
	{
		if ($_GET['token'] == $this->session->userdata('session_id')) {
			unset($_GET['token']);
			echo "<pre>";
			$this->db->where($_GET);
			$a = $this->db->get('packing_list_qc');
			header("Location:" . base_url('file/qc/' . $a->result()[0]->file));
		} else {
			echo "Session expired, go back and refresh the page";
		}
	}
	function update_material()
	{
		$this->db->where('po_detail_id', $_POST['po_detail_id'])->delete('m_material');
		$this->db->where('po_detail_id', $_POST['po_detail_id'])->update('po_detail', array('foc' => $_POST['foc']));
		$id = $_POST['po_detail_id'];
		$data = $_POST;
		$this->upload_pl_fb($id, $_FILES['file']);
		redirect(base_url('data/pl_list_ori'));
	}
	function create_dp_po()
	{
		$d = $this->db2->where('c_orderline_id', $_POST['c_orderline_id'])->get('f_web_po_detail')->result()[0];
		$data = $_POST;
		$data['c_order_id'] 	= $d->c_order_id;
		$data['c_bpartner_id'] 	= $d->c_bpartner_id;
		$this->db->insert('m_date_promised', $data);
		echo 1;
	}
	function create_dp_po_all()
	{
		$data = $_POST;
		$c_orderline = explode(';', $data['c_orderline_id']);
		unset($c_orderline[sizeof($c_orderline) - 1], $data['c_orderline_id']);
		foreach ($c_orderline as $a => $b) {
			$d = $this->db2->where('c_orderline_id', $b)->get('f_web_po_detail')->result()[0];
			$data['c_order_id'] 	= $d->c_order_id;
			$data['c_bpartner_id'] 	= $d->c_bpartner_id;
			$data['c_orderline_id'] 	= $d->c_orderline_id;
			$this->db->insert('m_date_promised', $data);
		}
		redirect('data/po_detail/' . $d->c_order_id);
	}
	function print_pl()
	{
		$this->load->view('supplier/report/packing_list');
	}
	function down_pl()
	{
		$this->load->view('supplier/report/down_packing_list');
	}
	function po_download()
	{
		$file = $this->db->where('c_order_id', $_GET['id'])->limit(1)->order_by('created_date', 'DESC')->get('m_po_file')->result()[0]->file_name;
		header('location:' . base_url('file/po/' . $file));
	}
	function pl_list_today()
	{
		$data['title_page'] = "PACKING LIST";
		$data['desc_page']	= "TODAY";
		$data['content']	= "data/detail/pl_list_today";
		$this->load->view('layout', $data);
	}
	//Document PO Per PO BUYER
	// function doc_po(){
	// 	$this->load->view('document/po');
	// }

	function doc_po()
	{
		$web = $this->db2->where('c_order_id', $_GET['id'])->get('f_web_po_header')->result()[0];
		$cek = $this->db->where('c_order_id', $_GET['id'])->get('m_download_docpo')->num_rows();
		if ($cek == 0) {
			$data = array(
				'c_order_id' 		=> $web->c_order_id,
				'documentno' 		=> $web->documentno,
				'c_bpartner_id' 	=> $web->c_bpartner_id
			);
			$this->db->insert('m_download_docpo', $data);
		} else {
			$this->db->set('download_date', date('Y-m-d H:i:s'));
			$this->db->where('c_order_id', $_GET['id']);
			$this->db->update('m_download_docpo');
		}
		$this->load->view('document/po');
	}
	//Document PO Per Delivery Date
	function doc_po_delivery()
	{
		$this->load->view('document/po_per_deliverydate');
	}
	function download_excel_po()
	{
		$this->load->view('supplier/report/download_excel_po');
	}
	function download_excel_po_today()
	{
		$this->load->view('supplier/report/download_excel_po_today');
	}
	//View PO Per PO BUYER
	function po_view()
	{
		$this->load->view('supplier/document/po_view');
	}
	//View PO Per DELIVERY DATE
	function po_view_deliverydate()
	{
		$this->load->view('supplier/document/po_view_deliverydate');
	}
	function po_file_download()
	{
		print_r($_GET);
		$file = $this->db->order_by('created_date', 'DESC')->limit(1)->where('status', 't')->get('m_po_file_additional')->result()[0]->file_name;
		header('location:' . base_url('file/additional/' . $file));
	}
	function list_file()
	{
		$this->load->view('supplier/data/detail/po_file_add');
	}
	function download_po_file()
	{
		$file = $this->db->where($_GET)->get('m_po_file_additional')->result()[0]->file_name;
		header('location:' . base_url('file/additional/' . $file));
	}
	function po_today()
	{
		$data['title_page'] = "PURCHASE ORDER";
		$data['desc_page']	= "ACTIVED TODAY";
		$data['content']	= "data/po_today";
		$this->load->view('layout', $data);
	}
	function orderform_thread()
	{
		$this->load->view('document/orderform_thread');
	}
	function orderform_zipper()
	{
		$this->load->view('document/orderform_zipper');
	}
	function upload_pl_new()
	{
		$data = $_FILES['file'];
		include $_SERVER["DOCUMENT_ROOT"] . '/library/Classes/PHPExcel/IOFactory.php';
		$path = $_SERVER["DOCUMENT_ROOT"] . "/file/";
		if (
			$data["type"] != 'application/kset' &&
			$data["type"] != 'application/vnd.ms-excel' &&
			$data["type"] != 'application/xls' &&
			$data["type"] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
			$data["type"] != 'application/ms-excel'
		) {
			echo "<br><div class='alert alert-danger text-center'>File extention is not support, make sure that the type is <b>.xls</b></div>";
		} else {
			$datas 			= move_uploaded_file($data["tmp_name"], $path . $data['name']);
			$inputFileName 	= $path . $data['name'];
			$objPHPExcel 	= PHPExcel_IOFactory::load($inputFileName);
			$sheetCount 	= $objPHPExcel->getSheetCount();
			$sheet 			= $objPHPExcel->getSheetNames();
			$sheetData 		= $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
			foreach ($sheetData as $a => $b) {
				if ($a > 1) {
					$erp = $this->db2->where('c_orderline_id', $b['A'])->get('f_web_po_detail');
					if ($erp->num_rows() > 0) {
						$erps = $erp->result()[0];
						$erps->no_packinglist 	= preg_replace('/\s+/', '', $b['F']);
						$erps->qty_carton		= $b['J'];
						$erps->barcode_id		= $b['I'];
						$erps->kst_etadate		= $b['H'];
						$erps->kst_etddate		= $b['G'];
						$erps->qty_upload		= $b['E'];
						$erps->kst_invoicevendor = preg_replace('/\s+/', '', $b['B']);
						$erps->kst_resi			= preg_replace('/\s+/', '', $b['M']);
						$erps->foc 				= $b['P'];

						$excel[] = $erps;
					} else {
						$excel[] = array();
					}
				}
			}
			echo "<br><table class='table table-bordered'>";
			echo "<tr class='bg-green'><th>c_orderline_id</th><th>Product</th><th class='text-center'>Status</th></tr>";
			foreach ($excel as $a => $b) {
				if (!empty($b->no_packinglist) && !empty($b->qty_carton) && !empty($b->kst_etadate) && !empty($b->kst_etddate) && !empty($b->qty_upload) && !empty($b->kst_resi) && !empty($b->kst_invoicevendor) && is_numeric($b->qty_carton) == TRUE && is_numeric($b->qty_upload) == TRUE) {
					if (sizeof($b) != 0) {
						$insert = $this->insert_detail($b);
					} else {
						$insert = 'Product not found';
					}

					$this->table_acc($sheetData[$a + 2]['A'], $b, $insert);
				} else {
					// $this->table_acc($sheetData[$a+2]['A'],$b);
				}
			}
			echo "</table>";
		}
	}
	private function table_status($id, $data, $status = 0)
	{
		if (sizeof($data) == 0) {
			echo "<tr class='bg-danger'><td>" . $id . "</td><td colspan='2'>" . $status . "</td></tr>";
		} else {
			echo "<tr class='bg-success'>";
			echo "<td>" . $data->c_orderline_id . "</td>";
			echo "<td>" . $data->desc_product . "</td>";
			echo "<td class='text-center'><b>" . $status . "</b></td>";
			echo "</tr>";
		}
	}
	private function table_status_dp($id, $data, $status = 0)
	{
		if (sizeof($data) == 0) {
			echo "<tr class='bg-danger'><td>" . $id . "</td><td colspan='2'>" . $status . "</td></tr>";
		} else {
			echo "<tr class='bg-success'>";
			echo "<td class='text-center'>" . $data->documentno . "</td>";
			// echo "<td>".$data->c_orderline_id."</td>";
			echo "<td class='text-center'>" . $data->date_promised . "</td>";

			echo "<td class='text-center'><b>" . $status . "</b></td>";
			echo "</tr>";
		}
	}
	private function insert_header($data)
	{
		$po_header = $this->db->where('c_order_id', $data->c_order_id)->get('po_header');
		if ($po_header->num_rows() == 0) {
			$aa = $this->db2->where('c_order_id', $data->c_order_id)->get('f_web_po_header')->result()[0];
			$this->db->insert('po_header', $aa);
		}
	}
	private function insert_detail($data)
	{
		$packinglist 	= $data->no_packinglist;
		$invoice 		= $data->kst_invoicevendor;
		$awb 			= $data->kst_resi;
		$cek_lock = $this->db->query("select * from po_detail where no_packinglist='$packinglist' and kst_invoicevendor='$invoice' and kst_resi='$awb' and is_locked='t'");
		if ($cek_lock->num_rows() == 0) {
			$flag = 0;
			$where['no_packinglist'] 		= $data->no_packinglist;
			$where['kst_resi'] 				= $data->kst_resi;
			$where['kst_invoicevendor'] 	= $data->kst_invoicevendor;
			$where['item'] 					= $data->item;
			$where['documentno'] 			= $data->documentno;
			$where['c_orderline_id'] 		= $data->c_orderline_id;
			$where['isactive'] 				= 'true';
			$cek = $this->db->where($where)->get('po_detail');
			if ($cek->num_rows() == 0) {
				$this->db->trans_begin();
				$this->insert_header($data);
				$this->db->insert('po_detail', $data);
				if ($this->db->trans_status() === FALSE) {
					$this->db->trans_rollback();
				} else {
					$this->db->trans_commit();
					$flag++;
				}
				if ($flag > 0) {
					$this->delete_double($data);
				}
				return "Success!";
			} else {
				return "PL & Inv Already Exist";
			}
		} else {
			return "PL & Inv Already Lock";
		}
	}
	private function delete_double($data)
	{
		$delete_double = array();
		$where['no_packinglist'] 		= $data->no_packinglist;
		$where['kst_resi'] 				= $data->kst_resi;
		$where['kst_invoicevendor'] 	= $data->kst_invoicevendor;
		$where['item'] 					= $data->item;
		$where['documentno'] 			= $data->documentno;

		$cek_double = $this->db->where($where)->get('edm_cekdouble');
		foreach ($cek_double->result() as $cek) {
			$delete_double[] = $cek->po_detail_id;
		}
		if (sizeof($delete_double) > 0) {
			$this->db->where($where);
			$this->db->where_not_in('po_detail_id', $delete_double);
			$this->db->delete('po_detail');
		}
	}
	private function copy_line($corderline = 0)
	{
		$a = $this->db2->where('c_orderline_id', $corderline)->get('f_web_po_detail_line');
		foreach ($a->result() as $b) {
			unset($b->qty, $b->documentno, $b->c_order_id, $b->m_product_category_value_fp, $b->componentvalue, $b->name);
			$cek = $this->db->where('c_orderline_id', $corderline)->where('poreference', $b->poreference)->get('po_detailline')->num_rows();
			if ($cek == 0) {
				$this->db->insert('po_detailline', $b);
				return 1;
			} else {
				return 2;
			}
		}
	}
	private function insert_detail_dp($data)
	{
		$input['c_order_id'] = $data->c_order_id;
		$input['c_orderline_id'] = $data->c_orderline_id;
		$input['c_bpartner_id'] = $data->c_bpartner_id;
		$input['date_promised'] = $data->date_promised;

		$where['c_order_id'] = $data->c_order_id;
		$where['c_orderline_id'] = $data->c_orderline_id;
		$where['c_bpartner_id'] = $data->c_bpartner_id;
		// $where['date_promised'] = $data->date_promised;
		$where['lock'] = 't';

		$cek = $this->db->where($where)->get('m_date_promised');
		if ($cek->num_rows() == 0) {

			$this->db->insert('m_date_promised', $input);
			return "Success!";
		} else {
			return "Already Locked";
		}
	}
	// ---------------------------------------UPLOAD DATE PROMISED----------------------------------------------------------
	function upload_dp_new()
	{
		$data = $_FILES['file'];
		include $_SERVER["DOCUMENT_ROOT"] . '/library/Classes/PHPExcel/IOFactory.php';
		$path = $_SERVER["DOCUMENT_ROOT"] . "/file/dp/";
		if (
			$data["type"] != 'application/kset' &&
			$data["type"] != 'application/vnd.ms-excel' &&
			$data["type"] != 'application/xls' &&
			$data["type"] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
			$data["type"] != 'application/ms-excel'
		) {
			echo "<br><div class='alert alert-danger text-center'>File extention is not support, make sure that the type is <b>.xls</b></div>";
		} else {
			$datas 			= move_uploaded_file($data["tmp_name"], $path . $data['name']);
			$inputFileName 	= $path . $data['name'];
			$objPHPExcel 	= PHPExcel_IOFactory::load($inputFileName);
			$sheetCount 	= $objPHPExcel->getSheetCount();
			$sheet 			= $objPHPExcel->getSheetNames();
			$sheetData 		= $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
			foreach ($sheetData as $a => $b) {
				if ($a > 1) {
					$udp = $this->db2->where('c_orderline_id', $b['B'])->get('f_web_po_detail');
					if ($udp->num_rows() > 0) {
						$x = $udp->result()[0];
						$x->c_order_id 		= $b['A'];
						$x->c_orderline_id 	= $b['B'];
						$x->c_bpartner_id	= $b['C'];
						$x->date_promised	= $b['D'];
						$excel[] = $x;
					} else {
						$excel[] = array();
					}
				}
				//var_dump($sheetData);


			}
			echo "<br><table class='table table-bordered'>";
			echo "<tr class='bg-green'><th class='text-center'>PO Number</th><th class='text-center'>Date Promised</th><th class='text-center'>Status</th></tr>";

			foreach ($excel as $a => $b) {

				if (sizeof($b) != 0)
					$insert = $this->insert_detail_dp($b);

				else
					$insert = 'There is no record found!';

				$this->table_status_dp($sheetData[$a + 2]['A'], $b, $insert);
				// echo "<pre>";
				// print_r($excel);
			}
			echo "</table>";
		}
	}
	//--------------------------REPORT AWB-----------------------------
	function awb()
	{
		$data['title_page'] = "AIR WAY BILL";
		$data['desc_page']	= "Create New";
		$data['content']	= "data/new_awb";
		$this->load->view('layout', $data);
	}

	//---------------------DOWNLOAD TEMPLATE PL ACC & FB-----------------------
	function download_pl_acc()
	{
		$this->load->view('supplier/report/download_pl_acc');
	}
	function download_excel_pl_fbr()
	{
		$this->load->view('supplier/report/download_excel_pl_fbr');
	}
	//--------------------------UPLOAD PL ACC-------------------------------
	function upload_pl_acc()
	{
		$data = $_FILES['file'];
		include $_SERVER["DOCUMENT_ROOT"] . '/library/Classes/PHPExcel/IOFactory.php';
		$path = $_SERVER["DOCUMENT_ROOT"] . "/file/";
		if (
			$data["type"] != 'application/kset' &&
			$data["type"] != 'application/vnd.ms-excel' &&
			$data["type"] != 'application/xls' &&
			$data["type"] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
			$data["type"] != 'application/ms-excel'
		) {
			echo "<br><div class='alert alert-danger text-center'>File extention is not support, make sure that the type is <b>.xls</b></div>";
		} else {
			$datas 			= move_uploaded_file($data["tmp_name"], $path . $data['name']);
			$inputFileName 	= $path . $data['name'];
			$objPHPExcel 	= PHPExcel_IOFactory::load($inputFileName);
			$sheetCount 	= $objPHPExcel->getSheetCount();
			$sheet 			= $objPHPExcel->getSheetNames();
			$sheetData 		= $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
			foreach ($sheetData as $a => $b) {
				if ($a > 1) {
					$erp = $this->db2->where('c_orderline_id', $b['A'])->get('f_web_po_detail');
					if ($erp->num_rows() > 0) {
						$erps = $erp->result()[0];
						$erps->no_packinglist 	= preg_replace('/\s+/', '', $b['F']);
						$erps->qty_carton		= $b['J'];
						$erps->barcode_id		= $b['I'];
						$erps->kst_etadate		= $b['H'];
						$erps->kst_etddate		= $b['G'];
						$erps->qty_upload		= $b['E'];
						if (preg_replace('/\s+/', '', $b['B']) == '-') {
							$erps->kst_invoicevendor = preg_replace('/\s+/', '', $b['F']);
						} else {
							$erps->kst_invoicevendor = preg_replace('/\s+/', '', $b['B']);
						}
						$erps->kst_resi			= preg_replace('/\s+/', '', $b['M']);
						$erps->remark			= $b['L'];
						$erps->foc 				= $b['P'];
						$erps->kst_suratjalanvendor = '-';
						$excel[] = $erps;
					} else {
						$excel[] = array();
					}
				}
			}
			echo "<br><table class='table table-bordered'>";
			echo "<tr class='bg-green'><th>PackingList</th><th>Po Number</th><th>Item</th><th>Qty Order</th><th>Qty Upload</th><th class='text-center'>Status</th></tr>";
			// $eta = $b->qty_carton;
			// error_reporting(0);

			foreach ($excel as $a => $b) {
				if (
					!empty($b->no_packinglist) && !empty($b->qty_carton)
					&& !empty($b->kst_etadate) && !empty($b->kst_etddate)
					&& !empty($b->qty_upload) && !empty($b->kst_resi)
					&& !empty($b->kst_invoicevendor) && is_numeric($b->qty_carton) == TRUE
					&& is_numeric($b->qty_upload) == TRUE
				) {
					if (sizeof($b) != 0) {
						$insert = $this->insert_detail($b);
					} else {
						$insert = 'Product not found';
					}

					$this->table_acc($sheetData[$a + 2]['A'], $b, $insert);
				} else {
					// $this->table_acc($sheetData[$a+2]['A'],$b);
				}
			}

			echo "</table>";
		}
	}

	private function table_acc($id, $data, $status = 0)
	{
		if (sizeof($data) == 0) {
			echo "<tr class='bg-danger'><td>" . $id . "</td><td colspan='2'>" . $status . "</td></tr>";
		} else {
			echo "<tr class='bg-success'>";
			echo "<td>" . $data->no_packinglist . "</td>";
			echo "<td>" . $data->documentno . "</td>";
			echo "<td>" . $data->item . "</td>";
			echo "<td>" . $data->qtyentered . "</td>";
			echo "<td>" . $data->qty_upload . "</td>";
			echo "<td class='text-center'><b>" . $status . "</b></td>";
			echo "</tr>";
		}
	}
	//--------------------------------UPLOAD PL FB--------------------------------------
	// controller baru untuk upload fabric
	function upload_pl_fr()
	{
		$data = $_FILES['file_pl_fbr'];
		include $_SERVER["DOCUMENT_ROOT"] . '/library/Classes/PHPExcel/IOFactory.php';
		$path = $_SERVER["DOCUMENT_ROOT"] . "/file/";
		if (
			$data["type"] != 'application/kset' &&
			$data["type"] != 'application/vnd.ms-excel' &&
			$data["type"] != 'application/xls' &&
			$data["type"] != 'application/xlsx' &&
			$data["type"] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
			$data["type"] != 'application/ms-excel'
		) {
			echo "<br><div class='alert alert-danger text-center'>File extention is not support, make sure that the type is <b>.xls</b></div>";
		} else {
			$datas 			= move_uploaded_file($data["tmp_name"], $path . $data['name']);
			$inputFileName 	= $path . $data['name'];
			$objPHPExcel 	= PHPExcel_IOFactory::load($inputFileName);
			$sheetCount 	= $objPHPExcel->getSheetCount();
			$sheet 			= $objPHPExcel->getSheetNames();
			$sheetData 		= $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
			foreach ($sheetData as $a => $b) {
				if ($a > 1) {
					$upl = $this->db2->where('c_orderline_id', $b['A'])->get('f_web_po_detail');
					if ($upl->num_rows() > 0) {
						$upld = $upl->result()[0];
						$upld->no_packinglist 		= preg_replace('/\s+/', '', $b['B']);
						$upld->kst_invoicevendor 	= preg_replace('/\s+/', '', $b['C']);
						$upld->kst_etddate			= $b['D'];
						$upld->kst_etadate			= $b['E'];
						$upld->foc 					= $b['J'];
						$upld->qty_upload			= $b['L'];
						$upld->kst_resi		 		= preg_replace('/\s+/', '', $b['O']);
						$upld->remark				= preg_replace('/\s+/', '', $b['N']);
						$upld->nomor_roll	 		= preg_replace('/\s+/', '', $b['K']);
						$upld->batch_number 		= preg_replace('/\s+/', '', $b['M']);
						$upld->uomsymbol			= $b['I'];
						$upld->c_orderline_id		= $b['A'];
						$upld->c_bp_group_id		= $b['S'];
						$upld->status_po 			= $b['R'];
						$excel[] = $upld;
					} else {
						$excel[] = array();
					}
				}
			}
			echo "<br><table class='table table-bordered'>";
			echo "<tr class='bg-green'><th>PackingList</th><th>Po Number</th><th>Status PO</th><th>Item</th><th>Qty Upload</th><th>Roll Number</th><th class='text-center'>Status</th></tr>";
			foreach ($excel as $a => $b) {
				if (
					!empty($b->no_packinglist) && !empty($b->kst_invoicevendor)
					&& !empty($b->kst_etddate) && !empty($b->kst_etadate)
					&& !empty($b->qty_upload) && !empty($b->kst_resi)
				) {
					if (sizeof($b) != 0) {
						$insert = $this->insert_complete($b);
					} else {
						$insert = 'Product not found';
					}

					$this->table_statusfbr($sheetData[$a + 2]['A'], $b, $insert);
				} else {
					// $this->table_acc($sheetData[$a+2]['A'],$b);
				}
			}

			echo "</table>";
		}
	}
	private function insert_complete($data)
	{
		$packinglist = $data->no_packinglist;
		$invoice = $data->kst_invoicevendor;
		$awb = $data->kst_resi;
		$cek_lock = $this->db->query("select * from po_detail where no_packinglist='$packinglist' and kst_invoicevendor='$invoice' and kst_resi='$awb' and is_locked='t' and isactive='t'");
		if ($cek_lock->num_rows() == 0) {

			try {
				$this->db->trans_begin();

				$this->insert_header($data);
				$this->insert_podetail($data);
				$this->insert_material($data);

				$this->db->trans_commit();

				return "Success";
			} catch (Exception $e) {
				$this->db->trans_rollback();
				return "Upload Failed";
			}
		} else {
			return "PL & Inv Already Lock";
		}
	}
	private function insert_material($data)
	{

		$where['c_order_id'] 	 = $data->c_order_id;
		$where['c_orderline_id'] = $data->c_orderline_id;
		$where['no_packinglist'] = $data->no_packinglist;
		$where['kst_invoicevendor'] = $data->kst_invoicevendor;
		$where['kst_resi'] = $data->kst_resi;
		$where['isactive'] = 'true';
		$m_material = $this->db->where($where)->get('po_detail');
		// if ($m_material->num_rows() == 1) {
		if ($m_material->num_rows() > 0) {
			$id = $m_material->result()[0]->po_detail_id;
			$aa = array(
				'po_detail_id' => $id,
				'nomor_roll' => preg_replace('/\s+/', '', $data->nomor_roll),
				'qty' => $data->qty_upload,
				'uomsymbol' => $data->uomsymbol,
				'batch_number' => preg_replace('/\s+/', '', $data->batch_number),
				'c_orderline_id' => $data->c_orderline_id
			);
			$this->db->insert('m_material', $aa);
			$a = $this->db->select_sum('qty')->where('po_detail_id', $id)->get('m_material')->result();

			foreach ($a as $a) {
				$this->db->where('po_detail_id', $id)->update('po_detail', array('qty_upload' => $a->qty));
			}
			return "Success!";
		} else {
			return "PL & Inv Already Exist";
		}
	}
	private function insert_podetail($data)
	{
		// $this->insert_material($data);
		$where2['no_packinglist'] 	= $data->no_packinglist;
		$where2['kst_resi'] 		= $data->kst_resi;
		$where2['kst_invoicevendor'] = $data->kst_invoicevendor;
		$where2['m_product_id'] 			= $data->m_product_id;
		$where2['c_orderline_id']	= $data->c_orderline_id;
		$where2['isactive'] 		= 'true';

		$cek = $this->db->where($where2)->get('po_detail');
		if ($cek->num_rows() == 0) {
			$detail = array(
				'c_order_id'			=> $data->c_order_id,
				'c_orderline_id'		=> $data->c_orderline_id,
				'c_bpartner_id'			=> $data->c_bpartner_id,
				// 'supplier'				=> preg_replace('/\s+/', '', $data->supplier),
				'supplier'				=> $data->supplier,
				'documentno'			=> preg_replace('/\s+/', '', $data->documentno),
				'm_product_id'			=> $data->m_product_id,
				'item'					=> preg_replace('/\s+/', '', $data->item),
				'qtyordered'			=> $data->qtyordered,
				'qtydelivered'			=> $data->qtydelivered,
				'qtyentered'			=> $data->qtyentered,
				'c_uom_id'				=> $data->c_uom_id,
				'uomsymbol'				=> preg_replace('/\s+/', '', $data->uomsymbol),
				'status'				=> preg_replace('/\s+/', '', $data->status),
				'desc_product'			=> preg_replace('/\s+/', '', $data->desc_product),
				'category'				=> preg_replace('/\s+/', '', $data->category),
				'type_po'				=> $data->type_po,
				'datepromised'			=> $data->datepromised,
				'pobuyer'				=> preg_replace('/\s+/', '', $data->pobuyer),
				'kst_deliverydate'		=> $data->kst_deliverydate,
				'm_warehouse_id'		=> $data->m_warehouse_id,
				'no_packinglist'		=> preg_replace('/\s+/', '', $data->no_packinglist),
				'kst_invoicevendor'		=> preg_replace('/\s+/', '', $data->kst_invoicevendor),
				'kst_etddate'			=> $data->kst_etddate,
				'kst_etadate'			=> $data->kst_etadate,
				'foc'					=> $data->foc,
				'qty_upload'			=> $data->qty_upload,
				'kst_resi'				=> preg_replace('/\s+/', '', $data->kst_resi),
				'remark'				=> preg_replace('/\s+/', '', $data->no_packinglist),
				'pricelist'				=> $data->pricelist,
				'priceentered'			=> $data->priceentered,
				'c_bp_group_id'			=> $data->c_bp_group_id,
				'status_po'				=> $data->status_po
			);
			$this->db->insert('po_detail', $detail);

			return "Success!";
		} else {
			return "PL & Inv Already Exist";
		}
	}
	private function table_statusfbr($id, $data, $status = 0)
	{
		if (sizeof($data) == 0) {
			echo "<tr class='bg-danger'><td>" . $id . "</td><td colspan='2'>" . $status . "</td></tr>";
		} else {
			echo "<tr class='bg-success'>";
			echo "<td>" . $data->no_packinglist . "</td>";
			echo "<td>" . $data->documentno . "</td>";
			echo "<td>" . $data->status_po . "</td>";
			echo "<td>" . $data->item . "</td>";
			echo "<td>" . $data->qty_upload . "</td>";
			echo "<td>" . $data->nomor_roll . "</td>";
			echo "<td class='text-center'><b>" . $status . "</b></td>";
			echo "</tr>";
		}
	}
	function create_dp()
	{
		$data = $_POST;
		$c_order = $data['c_order_id'];
		$insert = $this->db2->where('c_order_id', $c_order)->get('f_web_po_detail')->result_array();
		foreach ($insert as $a => $d) {
			$dp = $this->db->limit(1)->order_by('created_date', 'DESC')->where('c_orderline_id', $d['c_orderline_id'])->get('m_date_promised');
			if ($dp->num_rows() == 0) {
				$data['c_order_id'] 	= $d['c_order_id'];
				$data['c_bpartner_id'] 	= $d['c_bpartner_id'];
				$data['c_orderline_id'] 	= $d['c_orderline_id'];
				$this->db->insert('m_date_promised', $data);
			} else {
				if ($dp->result()[0]->lock == 'f') {
					$data['c_order_id'] 	= $d['c_order_id'];
					$data['c_bpartner_id'] 	= $d['c_bpartner_id'];
					$data['c_orderline_id'] 	= $d['c_orderline_id'];
					$this->db->insert('m_date_promised', $data);
				}
			}
		}
		redirect('data/po_detail/' . $c_order);
	}
	function up_dp()
	{
		$data['title_page'] = "PURCHASE ORDER";
		$data['desc_page']	= "Upload & Download";
		$data['content']	= "data/up_dp";
		$this->load->view('layout', $data);
	}
	function download_excel_range()
	{
		$this->load->view('supplier/report/download_excel_po_range');
	}
	public function date_promise()
	{
		$data['title_page'] = "DOWNLOAD PURCHASE ORDER";
		$data['desc_page']	= "ACTIVE";
		$data['content']	= "data/date_promised";
		$this->load->view('layout', $data);
	}
	public function activepo()
	{
		$data['title_page'] = "PURCHASE ORDER";
		$data['desc_page']	= "ACTIVE PO";
		$data['content']	= "data/activepo";
		$this->load->view('layout', $data);
	}
	public function activepo_view()
	{
		$dari   = date("Y-m-d", strtotime($this->input->post('from')));
		$sampai = date("Y-m-d", strtotime($this->input->post('until')));

		$data['dari'] = $dari;
		$data['sampai'] = $sampai;
		$data['title_page'] = "PURCHASE ORDER";
		$data['desc_page']	= "ACTIVE PO";
		$data['content']	= "data/activepo_view";
		$this->load->view('layout', $data);
	}
	function activepo_ajax()
	{
		//
		$userid = $this->session->userdata('user_id');
		$dari 	= $this->input->post('dari');
		$sampai = $this->input->post('sampai');
		$draw   = $this->input->post('draw');

		//
		$activepo 	= $this->m_activepo->get_datatables($userid, $dari, $sampai);
		$data 		= array();
		$nomor_urut = 0;
		$i  		= 0;
		foreach ($activepo as $ap) {
			$counts[$i] = $this->m_activepo->get_f_web_po_detail($ap->c_order_id);
			$files[$i]  = $this->m_activepo->get_po_file_additional($ap->c_order_id);
			if ($files[$i] == 0) {
				$downs[$i] = '-';
			} else {
				$count[$i] = $this->m_activepo->get_po_file_additional2($userid, $ap->c_order_id);
				$downs[$i] = "<a onclick='return unduh($ap->c_order_id)' href='javascript:void(0)'><i class='fa fa-download'></i> " . (($count[$i] == 0) ? '' : '<span class="label label-danger">' . $count[$i] . '</span>') . "</a>";
			}
			$a[$i] = $this->m_activepo->get_m_date_promised($ap->c_order_id);
			$warna[$i] = $counts[$i] - $a[$i];
			if ($warna[$i] == $counts[$i]) {
				$warna2[$i] = 'bg-red';
			} else {
				if ($warna[$i] == 0) {
					$warna2[$i] = 'bg-green';
				} else {
					$warna2[$i] = 'bg-yellow';
				}
			}
			//
			switch ($warna2[$i]) {
				case 'bg-red':
					$remark_confirmation[$i] = "Nothing Confirmed";
					break;
				case 'bg-green':
					$remark_confirmation[$i] = "Already confirmed";
					break;
				case 'bg-yellow':
					$remark_confirmation[$i] = "Partially Confirmed";
					break;
				default:
					$remark_confirmation[$i] = "-";
					break;
			}
			$_temp = $ap->c_order_id . ',"' . trim($ap->documentno) . '"';
			//array untuk dikirim ke datatables
			$nestedData['no'] 					= (($draw - 1) * 10) + (++$nomor_urut);
			$nestedData['po_number'] 			= $ap->documentno;
			$nestedData['release_date'] 		= date("Y-m-d", strtotime($ap->release_date));
			$nestedData['category'] 			= $ap->category;
			$nestedData['item'] 				= $counts[$i];
			$nestedData['detail']				= "<a href='" . base_url('data/po_detail/' . $ap->c_order_id) . "'>Detail</a>";
			$nestedData['purchase_order']		= "<a onclick='return view_po($ap->c_order_id)' href='javascript:void(0)' class='btn btn-xs btn-success'><i class='fa fa-search'></i></a>
												  <a href='" . base_url('data/doc_po?id=' . $ap->c_order_id) . "' class='btn btn-primary btn-xs'><i class='fa fa-download'></i></a>";
			$nestedData['additional_file']  	= $downs[$i];
			$nestedData['orderform']			= "<a href='" . base_url('data/orderform_thread?c_order_id=' . $ap->c_order_id) . "'>Thread</a> - <a href='" . base_url('data/orderform_zipper?c_order_id=' . $ap->c_order_id) . "'>Zipper</a>";
			$nestedData['unconfirm']			= "<div class='" . $warna2[$i] . "'>" . $warna[$i] . "</div>";
			$nestedData['down_t1t2']			= "<a href='" . base_url('data/down_t1t2?order=' . json_encode(base64_encode($ap->c_order_id)) . '&type_po=' . json_encode(base64_encode($ap->type_po))) . "'><li class='fa fa-download'></li></a>";
			// $nestedData['down_t1t2_xml']		= "<a href='".base_url('data/down_t1t2_xml?order='.json_encode(base64_encode($ap->c_order_id)).'&type_po='.json_encode(base64_encode($ap->type_po)))."'><li class='fa fa-file-code-o'></li></a>";
			$nestedData['down_t1t2_xml']		= "<a href='javascript:void(0)' onclick='up_t1t2_xml(" . $ap->c_order_id . ")'><li class='fa fa-upload'></li></a>
				<a href='javascript:void(0)' onclick='down_t1t2_xml(" . $ap->c_order_id . ")'><li class='fa fa-download'></li></a>";
			$nestedData['remark_confirmation']  = $remark_confirmation[$i];
			$nestedData['pi_supplier']		= "<a onclick='return upload_pi($_temp)' href='javascript:void(0)' class='btn btn-xs btn-success'><i class='fa fa-upload'></i></a><a onclick='return download_pi($_temp)' href='javascript:void(0)' class='btn btn-xs btn-primary'><i class='fa fa-download'></i></a>";
			$data[] = $nestedData;
			$i++;
		}

		//
		$output = array(
			"draw" => intval($draw),
			"recordsTotal" => intval($this->m_activepo->count_all($userid)),
			"recordsFiltered" => intval($this->m_activepo->count_filtered($userid, $dari, $sampai)),
			"data" => $data,
		);
		echo json_encode($output);
	}
	public function filter_dp()
	{
		$data['title_page'] = "PURCHASE ORDER";
		$data['desc_page']	= "Download Template Date Promised";
		$data['content']	= "data/filter_dp";
		$this->load->view('layout', $data);
	}
	function filter_dp_view()
	{
		$data['title_page'] = "PURCHASE ORDER";
		$data['desc_page']	= "ACTIVE PO";
		$data['content']	= "data/filter_dp_view";
		$this->load->view('layout', $data);
	}
	function xls_filter_dp()
	{
		$this->load->view('supplier/report/xls_filter_dp');
	}
	function pl_list2()
	{
		$data['title_page'] = "PACKING LIST";
		$data['desc_page']	= "All";
		$data['content']	= "data/detail/pl_list2";
		$this->load->view('layout', $data);
	}
	function packinglist()
	{
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$limit = $this->input->post('length');
		$start = $this->input->post('start');

		if ($from != '' && $to != '') {
			$from = date('Y-m-d', strtotime($from));
			$to = date('Y-m-d', strtotime($to));
		}
		$user = $this->session->userdata('user_id');
		$posts = $this->packinglist_model->get_datatables($user, $from, $to, $user);
		// print_r($posts);exit();
		$data = array();
		$no = 1;


		foreach ($posts as $key => $post) {

			$nestedData['no'] 	= $no;
			$nestedData['no_packinglist'] = $post->no_packinglist;
			$nestedData['print_packing_list'] = base_url('data/print_pl?token=' . $this->session->userdata('session_id') . '&no_packinglist=' . $post->no_packinglist . '&invoice=' . $post->kst_invoicevendor . '&sj=' . $post->kst_suratjalanvendor . '&awb=' . str_replace(' ', '+', $post->kst_resi));
			$nestedData['kst_invoicevendor'] = $post->kst_invoicevendor;
			$nestedData['qc_check_report'] = $post->packing_list_total;
			$nestedData['created_at'] = $post->packing_list_total;
			$nestedData['kst_suratjalanvendor'] = $post->kst_suratjalanvendor;
			$nestedData['url_upload'] = base_url('data/up_pl_qc');
			$nestedData['kst_resi'] = $post->kst_resi;
			$nestedData['url_download'] = base_url('data/download_qc_check/?token=' . $this->session->userdata('session_id') . '&no_packinglist=' . trim($post->no_packinglist) . '&kst_suratjalanvendor=' . $post->kst_suratjalanvendor);
			$nestedData['url_detail'] = base_url('data/pl_detail');
			$nestedData['type_po'] = $post->type_po;
			$nestedData['is_locked'] = $post->is_locked;
			$nestedData['data'] = 'pl=' . $post->no_packinglist . '&sj=' . $post->kst_suratjalanvendor . '&inv=' . $post->kst_invoicevendor . '&awb=' . str_replace(' ', '+', $post->kst_resi) . '&token=' . $this->session->userdata('session_id');
			$nestedData['url_print_fb'] = base_url('label/label_fb_new?token=' . $this->session->userdata('session_id') . '&nopl=' . $post->no_packinglist) . '&sj=' . $post->kst_suratjalanvendor . '&inv=' . $post->kst_invoicevendor . '&awb=' . $post->kst_resi;
			$nestedData['url_print_acc'] = base_url('label/acc?token=' . $this->session->userdata('session_id') . '&nopl=' . $post->no_packinglist) . '&sj=' . $post->kst_suratjalanvendor . '&inv=' . $post->kst_invoicevendor . '&awb=' . $post->kst_resi;
			$nestedData['url_edit_header'] = base_url('data/edit_header/?token=' . $this->session->userdata('session_id') . '&no_packinglist=' . trim($post->no_packinglist) . "&kst_invoicevendor=" . $post->kst_invoicevendor . '&awb=' . $post->kst_resi);
			$nestedData['url_isactive'] = base_url('data/isactive');
			$nestedData['url_lock'] = base_url('data/lock');
			$no++;
			$data[] = $nestedData;
		}

		$output = array(
			"draw" => intval($this->input->post('draw')),
			"recordsTotal" => intval($this->packinglist_model->count_all($user)),
			"recordsFiltered" => intval($this->packinglist_model->count_filtered($user, $from, $to)),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	function packinglist_date_range()
	{
		$from = $this->input->post('from');
		$to = $this->input->post('to');
		$limit = $this->input->post('length');
		$start = $this->input->post('start');

		if ($from != '' && $to != '') {
			$from = date('Y-m-d', strtotime($from));
			$to = date('Y-m-d', strtotime($to));
		}
		$user = $this->session->userdata('user_id');
		$posts = $this->data_model->get_datatables($from, $to, $user);

		$data = array();
		$no = 1;


		foreach ($posts as $key => $post) {

			$nestedData['no'] 	= $no;
			$nestedData['no_packinglist'] = $post->no_packinglist;
			$nestedData['print_packing_list'] = base_url('data/print_pl?token=' . $this->session->userdata('session_id') . '&no_packinglist=' . $post->no_packinglist . '&invoice=' . $post->kst_invoicevendor . '&sj=' . $post->kst_suratjalanvendor . '&awb=' . str_replace(' ', '+', $post->kst_resi));
			$nestedData['kst_invoicevendor'] = $post->kst_invoicevendor;
			$nestedData['qc_check_report'] = $post->packing_list_total;
			$nestedData['created_at'] = $post->packing_list_total;
			$nestedData['kst_suratjalanvendor'] = $post->kst_suratjalanvendor;
			$nestedData['url_upload'] = base_url('data/up_pl_qc');
			$nestedData['kst_resi'] = $post->kst_resi;
			$nestedData['url_download'] = base_url('data/download_qc_check/?token=' . $this->session->userdata('session_id') . '&no_packinglist=' . trim($post->no_packinglist) . '&kst_suratjalanvendor=' . $post->kst_suratjalanvendor);
			$nestedData['url_detail'] = base_url('data/pl_detail');
			$nestedData['type_po'] = $post->type_po;
			$nestedData['is_locked'] = $post->is_locked;
			$nestedData['data'] = 'pl=' . $post->no_packinglist . '&sj=' . $post->kst_suratjalanvendor . '&inv=' . $post->kst_invoicevendor . '&awb=' . str_replace(' ', '+', $post->kst_resi) . '&token=' . $this->session->userdata('session_id');
			$nestedData['url_print_fb'] = base_url('label/label_fb_new?token=' . $this->session->userdata('session_id') . '&nopl=' . $post->no_packinglist) . '&sj=' . $post->kst_suratjalanvendor . '&inv=' . $post->kst_invoicevendor . '&awb=' . $post->kst_resi;
			$nestedData['url_print_acc'] = base_url('label/acc?token=' . $this->session->userdata('session_id') . '&nopl=' . $post->no_packinglist) . '&sj=' . $post->kst_suratjalanvendor . '&inv=' . $post->kst_invoicevendor . '&awb=' . $post->kst_resi;
			$nestedData['url_edit_header'] = base_url('data/edit_header/?token=' . $this->session->userdata('session_id') . '&no_packinglist=' . trim($post->no_packinglist) . "&kst_invoicevendor=" . $post->kst_invoicevendor . '&awb=' . $post->kst_resi);
			$nestedData['url_isactive'] = base_url('data/isactive');
			$nestedData['url_lock'] = base_url('data/lock');
			$no++;
			$data[] = $nestedData;
		}


		$output = array(
			"draw" => $this->input->post('draw'),
			"recordsTotal" => $this->data_model->count_all($user),
			"recordsFiltered" => $this->data_model->count_filtered($from, $to, $user),
			"data" => $data,
		);
		//output to json format
		echo json_encode($output);
	}
	//end serverside
	function down_t1t2()
	{
		if (json_decode(base64_decode($_GET['type_po'])) == 1) {
			$this->db2->where('c_order_id', json_decode(base64_decode($_GET['order'])));
			$data['fabric'] = $this->db2->get('rma_reportt1t2sum_v2')->result();
			// $data['fabric']= $this->db2->get('rma_reportt1t2fabric_v4')->result(); ganti v2 12jun2020 as ridwan info
			header("Content-Type:application/vnd.ms-excel");
			header('Content-Disposition:attachment; filename="Report T1 & T2 ' . date('d-m-Y') . '.xls"');
			$this->load->view('document/down_t1t2fabric', $data);
		} else {
			$this->db2->where('c_order_id', json_decode(base64_decode($_GET['order'])));
			$bp = $this->db2->get('c_order')->row_array()['c_bpartner_id'];
			if ($bp == '1001120') { //jika supplier acc ykk
				$this->db2->where('c_order_id', json_decode(base64_decode($_GET['order'])));
				$data['acc'] = $this->db2->get('rma_reportt1t2detail_v1_ykk')->result();
				header("Content-Type:application/vnd.ms-excel");
				header('Content-Disposition:attachment; filename="Report T1 & T2 ' . date('d-m-Y') . '.xls"');
				$this->load->view('document/down_t1t2acc', $data);
			} else { // supplier acc selain ykk
				$this->db2->where('c_order_id', json_decode(base64_decode($_GET['order'])));
				$data['acc'] = $this->db2->get('rma_reportt1t2detail_v1')->result();
				header("Content-Type:application/vnd.ms-excel");
				header('Content-Disposition:attachment; filename="Report T1 & T2 ' . date('d-m-Y') . '.xls"');
				$this->load->view('document/down_t1t2acc', $data);
			}
		}
	}

	// xml
	function up_t1t2_xml()
	{
		// ini_set('display_errors', 'On');

		$order = $this->input->post('c_order_id');

		$this->db2->distinct();
		$this->db2->select('a.documentno, a.c_order_id, d.m_product_category_id');
		$this->db2->from('c_order a');
		$this->db2->join('c_orderline b', 'b.c_order_id = a.c_order_id', 'left');
		$this->db2->join('m_product c', 'c.m_product_id = b.m_product_id', 'left');
		$this->db2->join('m_product_category d', 'd.m_product_category_id  = c.m_product_category_id', 'left');
		$this->db2->where('a.c_order_id', $order);
		$query = $this->db2->get()->row();

		$m_product = $query->m_product_category_id;

		if ($m_product == '1000003') { //view fabric
			$this->db2->where('c_order_id', $order);
			$this->db2->limit(1);
			$data = $this->db2->get('rma_reportt1t2sum_v2')->result_array();

			$this->db2->where('c_order_id', $order);
			$this->db2->order_by('line');
			$data_line = $this->db2->get('rma_reportt1t2sum_v2')->result_array();
		} else { //view acc
			$this->db2->where('c_order_id', $order);
			$this->db2->limit(1);
			$data = $this->db2->get('rma_reportt1t2v3')->result_array();

			$this->db2->where('c_order_id', $order);
			$this->db2->order_by('line');
			$data_line = $this->db2->get('rma_reportt1t2v3')->result_array();
		}


		$t1_code = $data[0]['t1_6digit_factorycode'];
		$name2 = $data[0]['name2']; //code adidas
		$b = str_replace('/', '-', $data[0]['documentno']);
		$c = $data[0]['version'];
		$d = $data[0]['version'];
		$e = $data[0]['version'];

		$version = sprintf('%02s', $e);
		//
		$purpose = !is_null($data[0]['purpose']) ? $data[0]['purpose'] : 'PO Creation';

		$uri = $_SERVER["DOCUMENT_ROOT"] . "/file/testzzz.xml";


		$x = new XMLWriter();

		$x->openURI($uri);
		// $x->openMemory();

		$x->startDocument('1.0', 'UTF-8');

		if (trim($purpose) == 'PO Creation' || trim($purpose) == 'PO Confirmation') {

			$x->setIndent(TRUE);
			$x->startElement('PurchaseOrder');
			$x->writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			$x->writeAttribute("xsi:noNamespaceSchemaLocation", "PurchaseOrderv1_0.xsd");

			foreach ($data as $row) {

				$x->startElement('POPurpose');
				$x->text($purpose);
				$x->endElement();

				$x->startElement('PONumber');
				$x->text($row['documentno']);
				$x->endElement();

				$x->startElement('POVersionNumber');
				$x->text($row['version']);
				$x->endElement();

				$x->startElement('POResponseVersionNumber');
				$x->text($row['version']);
				$x->endElement();

				$x->startElement('messageGenerationDateTime');
				$x->text(date('c', strtotime($row['latest_update'])));
				$x->endElement();

				$x->startElement('latestUpdateDateTime');
				$x->text(date('c', strtotime($row['latest_update'])));
				$x->endElement();

				$x->startElement('originalPOIssueDate');
				$x->text(date('Y-m-d', strtotime($row['issue_date'])));
				$x->endElement();

				$x->startElement('adidasPODownloadDate');
				$x->text(date('Y-m-d', strtotime($row['latest_update'])));
				$x->endElement();

				$x->startElement('currency');
				$x->text($row['currency']);
				$x->endElement();

				$x->startElement('paymentTerms');
				$x->text($row['payment_terms']);
				$x->endElement();

				$x->startElement('headerRemarks');
				$x->text($row['payment_terms']);
				$x->endElement();

				$x->startElement('sender');
				$x->startElement('senderName');
				$x->text($row['contact_personbuyer']);
				$x->endElement();
				$x->startElement('senderPerson');
				$x->text($row['contact_nobuyer']);
				$x->endElement();
				$x->endElement();

				$x->startElement('recipient');
				$x->text($row['contact_personbuyer']);
				$x->endElement();

				$x->startElement('buyer');
				$x->startElement('buyerName');
				$x->text($row['contact_personbuyer']);
				$x->endElement();
				$x->startElement('buyerAddressLine1');
				$x->text($row['t1_address1buy']);
				$x->endElement();
				$x->startElement('buyerAddressLine2');
				$x->text($row['t1_address2buy']);
				$x->endElement();
				$x->startElement('buyerAddressLine3');
				$x->text($row['t1_address3buy']);
				$x->endElement();
				$x->startElement('buyerAddressLine4');
				$x->text($row['t1_address4buy']);
				$x->endElement();
				$x->startElement('buyerCity');
				$x->text($row['t1_addresscitybuy']);
				$x->endElement();
				$x->startElement('buyerProvince');
				$x->text($row['buyer']);
				$x->endElement();
				$x->startElement('buyerCountryCode');
				$x->text($row['t1_addresscountrybuy']);
				$x->endElement();
				$x->startElement('buyerPostalCode');
				$x->text($row['t1_addressstatebuy']);
				$x->endElement();
				$x->startElement('buyerContactPerson');
				$x->text($row['contact_personbuyer']);
				$x->endElement();
				$x->startElement('buyerContactTelNumber');
				$x->text($row['contact_nobuyer']);
				$x->endElement();
				$x->startElement('buyerContactEmail');
				$x->text($row['emailbuyer']);
				$x->endElement();
				$x->endElement();

				$x->startElement('sellerName');
				$x->text($row['seller']);
				$x->endElement();

				$x->startElement('actualManufacturer');
				$x->startElement('actualManufacturerCode');
				$x->text($row['t1_6digit_factorycode']);
				$x->endElement();
				$x->startElement('actualManufacturerName');
				$x->text($row['t1_customer']);
				$x->endElement();
				$x->startElement('actualManufacturerAddressLine1');
				$x->text($row['t1_address1buy']);
				$x->endElement();
				$x->startElement('actualManufacturerAddressLine2');
				$x->text($row['t1_address2buy']);
				$x->endElement();
				$x->startElement('actualManufacturerAddressLine3');
				$x->text($row['t1_address3buy']);
				$x->endElement();
				$x->startElement('actualManufacturerAddressLine4');
				$x->text($row['t1_address4buy']);
				$x->endElement();
				$x->startElement('actualManufacturerCity');
				$x->text($row['t1_addresscitybuy']);
				$x->endElement();
				$x->startElement('actualManufacturerProvince');
				$x->text($row['buyer']);
				$x->endElement();
				$x->startElement('actualManufacturerCountryCode');
				$x->text($row['t1_addresscountrybuy']);
				$x->endElement();
				$x->startElement('actualManufacturerPostalCode');
				$x->text($row['t1_addressstatebuy']);
				$x->endElement();
				$x->endElement();

				$x->startElement('shipTo');
				$x->startElement('shipToCode');
				$x->text($row['t1_6digit_factorycode']);
				$x->endElement();
				$x->startElement('shipToName');
				// $x->text($row['t1_customer']);
				$x->text($row['ship_to']);
				$x->endElement();
				$x->startElement('shipToAddressLine1');
				$x->text($row['t1_address1buy']);
				$x->endElement();
				$x->startElement('shipToAddressLine2');
				$x->text($row['t1_address2buy']);
				$x->endElement();
				$x->startElement('shipToAddressLine3');
				$x->text($row['t1_address3buy']);
				$x->endElement();
				$x->startElement('shipToAddressLine4');
				$x->text($row['t1_address4buy']);
				$x->endElement();
				$x->startElement('shipToCity');
				$x->text($row['t1_addresscitybuy']);
				$x->endElement();
				$x->startElement('shipToProvince');
				// $x->text($row['buyer']);
				$x->text($row['t1_addressprovince']);
				$x->endElement();
				$x->startElement('shipToCountryCode');
				$x->text($row['t1_addresscountrybuy']);
				$x->endElement();
				$x->startElement('shipToPostalCode');
				// $x->text($row['t1_addressstatebuy']);
				$x->text($row['t1_addresspostal']);
				$x->endElement();
				$x->endElement();

				$x->startElement('T1Customer');
				$x->text($row['t1_customer']);
				$x->endElement();

				$x->startElement('ShippingInformation');
				$x->startElement('forwarder');
				$x->text($row['forwarder']);
				$x->endElement();
				$x->startElement('incoterm');
				$x->text($row['incoterm']);
				$x->endElement();
				$x->startElement('shipMode');
				$x->text($row['ship_mode']);
				$x->endElement();
				$x->startElement('countryOfOrigin');
				$x->text($row['country_origin']);
				$x->endElement();
				$x->startElement('packingInstruction');
				$x->text($row['country_origin']);
				$x->endElement();
				$x->startElement('shippingInstruction');
				$x->text($row['country_origin']);
				$x->endElement();
				$x->endElement();

				$x->startElement('headerExtension');
				$x->text($row['additional1']);
				$x->endElement();

				foreach ($data_line as $rows) {
					$x->startElement('POLineItem');
					$x->startElement('lineItemNumber');
					$x->text($rows['line']);
					$x->endElement();
					$x->startElement('changePurposeLineStatus');
					$x->text(isset($rows['line_status']) ? $rows['line_status'] : $rows['line_status2']);
					$x->endElement();
					$x->startElement('adidasOrderNumber');
					$x->text($rows['adidas_ordernumber']);
					$x->endElement();
					$x->startElement('adidasArticleNumber');
					$x->text($rows['adidas_articlenumber']);
					$x->endElement();
					$x->startElement('adidasWorkingNumber');
					$x->text($rows['string_agg']);
					$x->endElement();
					$x->startElement('requestDate');
					$x->text(isset($rows['buyer_requestdate']) ? $rows['buyer_requestdate'] : $rows['buyer_requestdate2']);
					$x->endElement();
					$x->startElement('adidasRequestDate');
					$x->text(isset($rows['buyer_requestdate']) ? $rows['buyer_requestdate'] : $rows['buyer_requestdate2']);
					$x->endElement();
					$x->startElement('adidasPlanDate');
					$x->text(isset($rows['buyer_requestdate']) ? $rows['buyer_requestdate'] : $rows['buyer_requestdate2']);
					$x->endElement();
					$x->startElement('priorityAndOrderType');
					$x->text(isset($rows['buyer_requestdate']) ? $rows['buyer_requestdate'] : $rows['buyer_requestdate2']);
					$x->endElement();
					$x->startElement('orderQty');
					$x->text((float) $rows['order_quantity']);
					$x->endElement();
					$x->startElement('orderUOM');
					$x->text($rows['uom']);
					$x->endElement();
					$x->startElement('season');
					$x->text($rows['season']);
					$x->endElement();
					$x->startElement('lineRemarks');
					$x->text($rows['line']);
					$x->endElement();
					$x->startElement('materialReferenceNumber');
					$x->text($rows['ref']);
					$x->endElement();
					$x->startElement('materialDescriptor');
					$x->startElement('materialDescription');
					$x->text($rows['material_name']);
					$x->endElement();
					$x->startElement('materialColor');
					$x->text($rows['material_colour']);
					$x->endElement();
					$x->startElement('sustainableMaterialComposition');
					$x->text($rows['material_name']);
					$x->endElement();
					// $x->startElement('sustainableMaterialComposition');
					// $x->text($rows['material_name']);
					// $x->endElement();
					$x->startElement('colorMatchingNote');
					$x->text($rows['material_colour']);
					$x->endElement();
					$x->startElement('materialSectionName');
					$x->text($rows['material_colour']);
					$x->endElement();
					$x->startElement('weightValue');
					$x->text(isset($rows['weight']) ? $rows['weight'] : $rows['weight1']);
					$x->endElement();
					$x->startElement('weightUOM');
					$x->text($rows['uom_weight']);
					$x->endElement();
					$x->startElement('widthValue');
					$x->text($rows['width']);
					$x->endElement();
					$x->startElement('widthUOM');
					$x->text($rows['uom_width']);
					$x->endElement();
					$x->startElement('thicknessValue');
					$x->text($rows['uom_width']);
					$x->endElement();
					$x->startElement('thicknessUOM');
					$x->text($rows['uom_width']);
					$x->endElement();
					$x->startElement('lengthValue');
					$x->text($rows['length']);
					$x->endElement();
					$x->startElement('lengthUOM');
					$x->text($rows['uom_length']);
					$x->endElement();
					$x->startElement('heightValue');
					$x->text($rows['height']);
					$x->endElement();
					$x->startElement('heightUOM');
					$x->text($rows['uom_height']);
					$x->endElement();
					$x->startElement('size');
					$x->text($rows['size']);
					$x->endElement();
					$x->endElement();
					$x->startElement('unit_price');
					$x->text($rows['unit_price']);
					$x->endElement();
					$x->startElement('linkExtension1');
					$x->text($rows['additional1']);
					$x->endElement();
					$x->startElement('linkExtension2');
					$x->text($rows['additional1']);
					$x->endElement();
					$x->startElement('linkExtension3');
					$x->text($rows['additional1']);
					$x->endElement();
					$x->startElement('linkExtension4');
					$x->text($rows['additional1']);
					$x->endElement();
					$x->startElement('linkExtension5');
					$x->text($rows['additional1']);
					$x->endElement();
					$x->endElement();
				}
			}
		} elseif (trim($purpose) == 'PO Amendment' || trim($purpose) == 'Amendment Confirmation') {

			$x->setIndent(TRUE);
			$x->startElement('PurchaseOrder');
			$x->writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			$x->writeAttribute("xsi:noNamespaceSchemaLocation", "PurchaseOrderv1_0.xsd");

			foreach ($data as $row) {

				$x->startElement('POPurpose');
				$x->text($purpose);
				$x->endElement();

				$x->startElement('PONumber');
				$x->text($row['documentno']);
				$x->endElement();

				$x->startElement('POVersionNumber');
				$x->text($row['version']);
				$x->endElement();

				$x->startElement('POResponseVersionNumber');
				$x->text($row['version']);
				$x->endElement();

				$x->startElement('messageGenerationDateTime');
				$x->text(date('c', strtotime($row['latest_update'])));
				$x->endElement();

				$x->startElement('latestUpdateDateTime');
				$x->text(date('c', strtotime($row['latest_update'])));
				$x->endElement();

				$x->startElement('originalPOIssueDate');
				$x->text(date('Y-m-d', strtotime($row['issue_date'])));
				$x->endElement();

				$x->startElement('adidasPODownloadDate');
				$x->text(date('Y-m-d', strtotime($row['latest_update'])));
				$x->endElement();

				$x->startElement('currency');
				$x->text($row['currency']);
				$x->endElement();

				$x->startElement('paymentTerms');
				$x->text($row['payment_terms']);
				$x->endElement();

				$x->startElement('headerRemarks');
				$x->text($row['payment_terms']);
				$x->endElement();

				$x->startElement('sender');
				$x->startElement('senderName');
				$x->text($row['contact_personbuyer']);
				$x->endElement();
				$x->startElement('senderPerson');
				$x->text($row['contact_nobuyer']);
				$x->endElement();
				$x->endElement();

				$x->startElement('recipient');
				$x->text($row['contact_personbuyer']);
				$x->endElement();

				$x->startElement('buyer');
				$x->startElement('buyerName');
				$x->text($row['contact_personbuyer']);
				$x->endElement();
				$x->startElement('buyerAddressLine1');
				$x->text($row['t1_address1buy']);
				$x->endElement();
				$x->startElement('buyerAddressLine2');
				$x->text($row['t1_address2buy']);
				$x->endElement();
				$x->startElement('buyerAddressLine3');
				$x->text($row['t1_address3buy']);
				$x->endElement();
				$x->startElement('buyerAddressLine4');
				$x->text($row['t1_address4buy']);
				$x->endElement();
				$x->startElement('buyerCity');
				$x->text($row['t1_addresscitybuy']);
				$x->endElement();
				$x->startElement('buyerProvince');
				$x->text($row['buyer']);
				$x->endElement();
				$x->startElement('buyerCountryCode');
				$x->text($row['t1_addresscountrybuy']);
				$x->endElement();
				$x->startElement('buyerPostalCode');
				$x->text($row['t1_addressstatebuy']);
				$x->endElement();
				$x->startElement('buyerContactPerson');
				$x->text($row['contact_personbuyer']);
				$x->endElement();
				$x->startElement('buyerContactTelNumber');
				$x->text($row['contact_nobuyer']);
				$x->endElement();
				$x->startElement('buyerContactEmail');
				$x->text($row['emailbuyer']);
				$x->endElement();
				$x->endElement();

				$x->startElement('sellerName');
				$x->text($row['seller']);
				$x->endElement();

				$x->startElement('actualManufacturer');
				$x->startElement('actualManufacturerCode');
				$x->text($row['t1_6digit_factorycode']);
				$x->endElement();
				$x->startElement('actualManufacturerName');
				$x->text($row['t1_customer']);
				$x->endElement();
				$x->startElement('actualManufacturerAddressLine1');
				$x->text($row['t1_address1buy']);
				$x->endElement();
				$x->startElement('actualManufacturerAddressLine2');
				$x->text($row['t1_address2buy']);
				$x->endElement();
				$x->startElement('actualManufacturerAddressLine3');
				$x->text($row['t1_address3buy']);
				$x->endElement();
				$x->startElement('actualManufacturerAddressLine4');
				$x->text($row['t1_address4buy']);
				$x->endElement();
				$x->startElement('actualManufacturerCity');
				$x->text($row['t1_addresscitybuy']);
				$x->endElement();
				$x->startElement('actualManufacturerProvince');
				$x->text($row['buyer']);
				$x->endElement();
				$x->startElement('actualManufacturerCountryCode');
				$x->text($row['t1_addresscountrybuy']);
				$x->endElement();
				$x->startElement('actualManufacturerPostalCode');
				$x->text($row['t1_addressstatebuy']);
				$x->endElement();
				$x->endElement();

				$x->startElement('shipTo');
				$x->startElement('shipToCode');
				$x->text($row['t1_6digit_factorycode']);
				$x->endElement();
				$x->startElement('shipToName');
				$x->text($row['t1_customer']);
				$x->endElement();
				$x->startElement('shipToAddressLine1');
				$x->text($row['t1_address1buy']);
				$x->endElement();
				$x->startElement('shipToAddressLine2');
				$x->text($row['t1_address2buy']);
				$x->endElement();
				$x->startElement('shipToAddressLine3');
				$x->text($row['t1_address3buy']);
				$x->endElement();
				$x->startElement('shipToAddressLine4');
				$x->text($row['t1_address4buy']);
				$x->endElement();
				$x->startElement('shipToCity');
				$x->text($row['t1_addresscitybuy']);
				$x->endElement();
				$x->startElement('shipToProvince');
				$x->text($row['buyer']);
				$x->endElement();
				$x->startElement('shipToCountryCode');
				$x->text($row['t1_addresscountrybuy']);
				$x->endElement();
				$x->startElement('shipToPostalCode');
				$x->text($row['t1_addressstatebuy']);
				$x->endElement();
				$x->endElement();

				$x->startElement('T1Customer');
				$x->text($row['t1_customer']);
				$x->endElement();

				$x->startElement('ShippingInformation');
				$x->startElement('forwarder');
				$x->text($row['forwarder']);
				$x->endElement();
				$x->startElement('incoterm');
				$x->text($row['incoterm']);
				$x->endElement();
				$x->startElement('shipMode');
				$x->text($row['ship_mode']);
				$x->endElement();
				$x->startElement('countryOfOrigin');
				$x->text($row['country_origin']);
				$x->endElement();
				$x->startElement('packingInstruction');
				$x->text($row['country_origin']);
				$x->endElement();
				$x->startElement('shippingInstruction');
				$x->text($row['country_origin']);
				$x->endElement();
				$x->endElement();

				$x->startElement('headerExtension');
				$x->text($row['additional1']);
				$x->endElement();

				foreach ($data_line as $rows) {
					$x->startElement('POLineItem');
					$x->startElement('lineItemNumber');
					$x->text($rows['line']);
					$x->endElement();
					$x->startElement('changePurposeLineStatus');
					$x->text(isset($rows['line_status']) ? $rows['line_status'] : $rows['line_status2']);
					$x->endElement();
					$x->startElement('adidasOrderNumber');
					$x->text($rows['adidas_ordernumber']);
					$x->endElement();
					$x->startElement('adidasArticleNumber');
					$x->text($rows['adidas_articlenumber']);
					$x->endElement();
					$x->startElement('adidasWorkingNumber');
					$x->text($rows['adidas_articlenumber']);
					$x->endElement();
					$x->startElement('requestDate');
					$x->text(isset($rows['buyer_requestdate']) ? $rows['buyer_requestdate'] : $rows['buyer_requestdate2']);
					$x->endElement();
					$x->startElement('adidasRequestDate');
					$x->text(isset($rows['buyer_requestdate']) ? $rows['buyer_requestdate'] : $rows['buyer_requestdate2']);
					$x->endElement();
					$x->startElement('adidasPlanDate');
					$x->text(isset($rows['buyer_requestdate']) ? $rows['buyer_requestdate'] : $rows['buyer_requestdate2']);
					$x->endElement();
					$x->startElement('priorityAndOrderType');
					$x->text(isset($rows['buyer_requestdate']) ? $rows['buyer_requestdate'] : $rows['buyer_requestdate2']);
					$x->endElement();
					$x->startElement('orderQty');
					$x->text((float) $rows['order_quantity']);
					$x->endElement();
					$x->startElement('orderUOM');
					$x->text($rows['uom']);
					$x->endElement();
					$x->startElement('season');
					$x->text($rows['season']);
					$x->endElement();
					$x->startElement('lineRemarks');
					$x->text($rows['line']);
					$x->endElement();
					$x->startElement('materialReferenceNumber');
					$x->text($rows['ref']);
					$x->endElement();
					$x->startElement('materialDescriptor');
					$x->startElement('materialDescription');
					$x->text($rows['material_name']);
					$x->endElement();
					$x->startElement('materialColor');
					$x->text($rows['material_colour']);
					$x->endElement();
					$x->startElement('sustainableMaterialComposition');
					$x->text($rows['material_name']);
					$x->endElement();
					// $x->startElement('sustainableMaterialComposition');
					// $x->text($rows['material_name']);
					// $x->endElement();
					$x->startElement('colorMatchingNote');
					$x->text($rows['material_colour']);
					$x->endElement();
					$x->startElement('materialSectionName');
					$x->text($rows['material_colour']);
					$x->endElement();
					$x->startElement('weightValue');
					$x->text(isset($rows['weight']) ? $rows['weight'] : $rows['weight1']);
					$x->endElement();
					$x->startElement('weightUOM');
					$x->text($rows['uom_weight']);
					$x->endElement();
					$x->startElement('widthValue');
					$x->text($rows['width']);
					$x->endElement();
					$x->startElement('widthUOM');
					$x->text($rows['uom_width']);
					$x->endElement();
					$x->startElement('thicknessValue');
					$x->text($rows['uom_width']);
					$x->endElement();
					$x->startElement('thicknessUOM');
					$x->text($rows['uom_width']);
					$x->endElement();
					$x->startElement('lengthValue');
					$x->text($rows['length']);
					$x->endElement();
					$x->startElement('lengthUOM');
					$x->text($rows['uom_length']);
					$x->endElement();
					$x->startElement('heightValue');
					$x->text($rows['height']);
					$x->endElement();
					$x->startElement('heightUOM');
					$x->text($rows['uom_height']);
					$x->endElement();
					$x->startElement('size');
					$x->text($rows['size']);
					$x->endElement();
					$x->endElement();
					$x->startElement('unit_price');
					$x->text($rows['unit_price']);
					$x->endElement();
					$x->startElement('linkExtension1');
					$x->text($rows['additional1']);
					$x->endElement();
					$x->startElement('linkExtension2');
					$x->text($rows['additional1']);
					$x->endElement();
					$x->startElement('linkExtension3');
					$x->text($rows['additional1']);
					$x->endElement();
					$x->startElement('linkExtension4');
					$x->text($rows['additional1']);
					$x->endElement();
					$x->startElement('linkExtension5');
					$x->text($rows['additional1']);
					$x->endElement();
					$x->endElement();
				}
			}
		} elseif (trim($purpose) == 'PO Cancellation' || trim($purpose) == 'Cancellation Confirmation') {

			$x->setIndent(TRUE);
			$x->startElement('POCancellation');
			$x->writeAttribute("xmlns:xsi", "http://www.w3.org/2001/XMLSchema-instance");
			$x->writeAttribute("xsi:noNamespaceSchemaLocation", "POCancellationv1_0.xsd");

			foreach ($data as $row) {

				$x->startElement('POCancellationPurpose');
				$x->text($purpose);
				$x->endElement();

				$x->startElement('PONumber');
				$x->text($row['documentno']);
				$x->endElement();

				$x->startElement('POVersionNumber');
				$x->text($row['version']);
				$x->endElement();

				$x->startElement('POResponseVersionNumber');
				$x->text($row['version']);
				$x->endElement();

				$x->startElement('messageGenerationDateTime');
				$x->text(date('c', strtotime($row['latest_update'])));
				$x->endElement();

				$x->startElement('latestUpdateDateTime');
				$x->text(date('c', strtotime($row['latest_update'])));
				$x->endElement();

				$x->startElement('originalPOIssueDate');
				$x->text(date('Y-m-d', strtotime($row['issue_date'])));
				$x->endElement();

				$x->startElement('headerRemarks');
				$x->text($row['payment_terms']);
				$x->endElement();

				$x->startElement('sender');
				$x->startElement('senderName');
				$x->text($row['contact_personbuyer']);
				$x->endElement();
				$x->startElement('senderPerson');
				$x->text($row['contact_nobuyer']);
				$x->endElement();
				$x->endElement();

				$x->startElement('recipient');
				$x->text($row['contact_personbuyer']);
				$x->endElement();

				$x->startElement('buyerName');
				$x->text($row['contact_personbuyer']);
				$x->endElement();

				$x->startElement('sellerName');
				$x->text($row['seller']);
				$x->endElement();
			}
		}
		$x->endElement(); //
		$x->endDocument();
		$x->flush();
		// $xml = $x->outputMemory();

		// template code
		if (trim($purpose) == 'PO Creation') {
			$template_code = 'M1';
		} elseif (trim($purpose) == 'PO Confirmation') {
			$template_code = 'M2';
		} elseif (trim($purpose) == 'PO Amendment') {
			$template_code = 'M3';
		} elseif (trim($purpose) == 'Amendment Confirmation') {
			$template_code = 'M4';
		} elseif (trim($purpose) == 'PO Cancellation') {
			$template_code = 'M7';
		} elseif (trim($purpose) == 'Cancellation Confirmation') {
			$template_code = 'M8';
		}

		$nama = $name2 . $t1_code . '_' . $b . '_V' . $version . $version . '_' . $template_code . '.xml';
		$this->load->helper('file');
		// $path_file = $_SERVER["DOCUMENT_ROOT"]."/testzzz.xml";

		// set up basic connection
		// $conn_id = ftp_connect('ftp.tong-siang.com');
		$conn_id = ftp_connect('po.aoi.co.id', 2121) or die('gagal');
		// login with username and password
		// $login_result = ftp_login($conn_id, 'aoi', 'aoi@245');
		$login_result = ftp_login($conn_id, 'T1', 'Pass@123#T1');

		// turn passive mode on
		ftp_pasv($conn_id, true);

		// upload a file
		// if (ftp_put($conn_id, 'CustPOLoad/AOI/CustUpload/'.$nama, $uri, FTP_ASCII)) {
		if (ftp_put($conn_id, 'T1/1001061/' . $nama, $uri, FTP_ASCII)) {
			$messages = "successfully uploaded $nama";
		} else {
			$messages = "There was a problem while uploading $nama";
		}

		// unlink($path_file);

		// close the connection
		ftp_close($conn_id);

		$data['messages'] = $messages;
		echo json_encode($messages);
		exit;
		// $this->load->helper('download');
		// force_download($nama, $xml);

		// view in browser
		// $this->output->set_content_type('text/xml');
		// $this->output->set_output($xml);

	}

	function down_t1t2_xml()
	{
		// ini_set('display_errors', 'On');
		error_reporting(E_ERROR | E_PARSE);

		$order = $this->input->post('c_order_id');

		$this->db2->distinct();
		$this->db2->select('a.documentno, a.c_order_id, d.m_product_category_id');
		$this->db2->from('c_order a');
		$this->db2->join('c_orderline b', 'b.c_order_id = a.c_order_id', 'left');
		$this->db2->join('m_product c', 'c.m_product_id = b.m_product_id', 'left');
		$this->db2->join('m_product_category d', 'd.m_product_category_id  = c.m_product_category_id', 'left');
		$this->db2->where('a.c_order_id', $order);
		$query = $this->db2->get()->row();

		$m_product = $query->m_product_category_id;

		if ($m_product == '1000003') { //view fabric
			$this->db2->where('c_order_id', $order);
			$this->db2->limit(1);
			$data = $this->db2->get('rma_reportt1t2sum_v2')->result_array();

			$this->db2->where('c_order_id', $order);
			$this->db2->order_by('line');
			$data_line = $this->db2->get('rma_reportt1t2sum_v2')->result_array();
		} else { //view acc
			$this->db2->where('c_order_id', $order);
			$this->db2->limit(1);
			$data = $this->db2->get('rma_reportt1t2v3')->result_array();

			$this->db2->where('c_order_id', $order);
			$this->db2->order_by('line');
			$data_line = $this->db2->get('rma_reportt1t2v3')->result_array();
		}


		$t1_code = $data[0]['t1_6digit_factorycode'];
		$name2 = $data[0]['name2']; //code adidas
		$b = str_replace('/', '-', $data[0]['documentno']);
		$c = $data[0]['version'];
		$d = $data[0]['version'];
		$e = $data[0]['version'];

		$version = sprintf('%02s', $e);
		//
		$purpose = !is_null($data[0]['purpose']) ? $data[0]['purpose'] : 'PO Creation';


		// template code
		if (trim($purpose) == 'PO Creation') {
			$template_code = 'M1';
			$template_code_respone = 'M2';
		} elseif (trim($purpose) == 'PO Confirmation') {
			$template_code = 'M2';
			$template_code_respone = 'M2';
		} elseif (trim($purpose) == 'PO Amendment') {
			$template_code = 'M3';
			$template_code_respone = 'M4';
		} elseif (trim($purpose) == 'Amendment Confirmation') {
			$template_code = 'M4';
			$template_code_respone = 'M4';
		} elseif (trim($purpose) == 'PO Cancellation') {
			$template_code = 'M7';
			$template_code_respone = 'M8';
		} elseif (trim($purpose) == 'Cancellation Confirmation') {
			$template_code = 'M8';
			$template_code_respone = 'M8';
		}

		$nama = $name2 . $t1_code . '_' . $b . '_V' . $version . $version . '_' . $template_code_respone . '.xml';
		$this->load->helper('file');
		$path_file = $_SERVER["DOCUMENT_ROOT"] . "/file/xml/" . $nama;


		// set up basic connection
		// $conn_id = ftp_connect('ftp.tong-siang.com');
		$conn_id = ftp_connect('po.aoi.co.id', 2121) or die('gagal');
		// login with username and password
		// $login_result = ftp_login($conn_id, 'aoi', 'aoi@245');
		$login_result = ftp_login($conn_id, 'T1', 'Pass@123#T1');

		// turn passive mode on
		ftp_pasv($conn_id, true);

		// download a file
		if (ftp_get($conn_id, $path_file, 'T2/1001061/' . $nama, FTP_BINARY)) {
			$messages = "successfully download $nama";
		} else {
			$messages = "There was a problem while downloading $nama";
		}

		// close the connection
		ftp_close($conn_id);


		$data['messages'] = $messages;
		echo json_encode($messages);
		exit;
	}

	// xml with libarary xml writer
	/*function down_t1t2_xml(){
    	ini_set('display_errors', 'On');

    	$this->db2->where('c_order_id', json_decode(base64_decode($_GET['order'])));
	    $data = $this->db2->get('rma_reportt1t2v3')->result_array();
    	// Load XML writer library
        $this->load->library('xml_writer');

        $a = $data[0]['t1_6digit_factorycode'];
		$b = str_replace('/', '-', $data[0]['documentno']);
		$c = $data[0]['version'];
		$d = $data[0]['version'];
		$e = $data[0]['version'];
		//
		$purpose = $data[0]['purpose'];

        // Initiate class
        $xml = new Xml_writer();
        $xml->setRootName('PurchaseOrder');
        $xml->initiate();

        foreach($data as $deff){
        $xml->addNode('POPurpose', $deff['purpose']);
        $xml->addNode('PONumber', $deff['documentno']);
        $xml->addNode('POVersionNumber', $deff['version']);
        $xml->addNode('POResponseVersionNumber', $deff['version']);
        $xml->addNode('messageGenerationDateTime', $deff['version']);
        $xml->addNode('latestUpdateDateTime', $deff['latest_update']);
        $xml->addNode('originalPOIssueDate', $deff['issue_date']);
        $xml->addNode('adidasPODownloadDate', $deff['latest_update']);
        $xml->addNode('currency', $deff['currency']);
        $xml->addNode('paymentTerms', $deff['payment_terms']);
        $xml->addNode('headerRemarks', $deff['payment_terms']);

        $xml->startBranch('sender');
            $xml->addNode('senderName', $deff['contact_personbuyer']);
            $xml->addNode('senderPerson', $deff['contact_nobuyer']);
        $xml->endBranch();

        $xml->addNode('recipient', $deff['payment_terms']);

        $xml->startBranch('buyer');
            $xml->addNode('buyerName', $deff['contact_personbuyer']);
            $xml->addNode('buyerAddressLine1', $deff['t1_address1buy']);
            $xml->addNode('buyerAddressLine2', $deff['t1_address2buy']);
            $xml->addNode('buyerAddressLine3', $deff['t1_address3buy']);
            $xml->addNode('buyerAddressLine4', $deff['t1_address4buy']);
            $xml->addNode('buyerCity', $deff['t1_addresscitybuy']);
            $xml->addNode('buyerProvince', $deff['buyer']);
            $xml->addNode('buyerCountryCode', $deff['t1_addresscountrybuy']);
            $xml->addNode('buyerPostalCode', $deff['t1_addressstatebuy']);
            $xml->addNode('buyerContactPerson', $deff['contact_personbuyer']);
            $xml->addNode('buyerContactTelNumber', $deff['contact_nobuyer']);
            $xml->addNode('buyerContactEmail', $deff['emailbuyer']);
        $xml->endBranch();

        $xml->addNode('sellerName', $deff['seller']);

        $xml->startBranch('actualManufacturer');
            $xml->addNode('actualManufacturerCode', $deff['t1_6digit_factorycode']);
            $xml->addNode('actualManufacturerName', $deff['t1_customer']);
            $xml->addNode('actualManufacturerAddressLine1', $deff['t1_address1buy']);
            $xml->addNode('actualManufacturerAddressLine2', $deff['t1_address2buy']);
            $xml->addNode('actualManufacturerAddressLine3', $deff['t1_address3buy']);
            $xml->addNode('actualManufacturerAddressLine4', $deff['t1_address4buy']);
            $xml->addNode('actualManufacturerCity', $deff['t1_addresscitybuy']);
            $xml->addNode('actualManufacturerProvince', $deff['buyer']);
            $xml->addNode('actualManufacturerCountryCode', $deff['t1_addresscountrybuy']);
            $xml->addNode('actualManufacturerPostalCode', $deff['t1_addressstatebuy']);
        $xml->endBranch();

        $xml->startBranch('shipTo');
            $xml->addNode('shipToCode', $deff['t1_6digit_factorycode']);
            $xml->addNode('shipToName', $deff['t1_customer']);
            $xml->addNode('shipToAddressLine1', $deff['t1_address1buy']);
            $xml->addNode('shipToAddressLine2', $deff['t1_address2buy']);
            $xml->addNode('shipToAddressLine3', $deff['t1_address3buy']);
            $xml->addNode('shipToAddressLine4', $deff['t1_address4buy']);
            $xml->addNode('shipToCity', $deff['t1_addresscitybuy']);
            $xml->addNode('shipToProvince', $deff['buyer']);
            $xml->addNode('shipToCountryCode', $deff['t1_addresscountrybuy']);
            $xml->addNode('shipToPostalCode', $deff['t1_addressstatebuy']);
        $xml->endBranch();

        $xml->addNode('T1Customer', $deff['t1_customer']);

        $xml->startBranch('ShippingInformation');
            $xml->addNode('forwarder', $deff['forwarder']);
            $xml->addNode('incoterm', $deff['incoterm']);
            $xml->addNode('shipMode', $deff['ship_mode']);
            $xml->addNode('countryOfOrigin', $deff['country_origin']);
            $xml->addNode('packingInstruction', $deff['country_origin']);
            $xml->addNode('shippingInstruction', $deff['country_origin']);
        $xml->endBranch();

        $xml->addNode('headerExtension', $deff['additional1']);

        $xml->startBranch('POLineItem');
            $xml->addNode('lineItemNumber', $deff['line']);
            $xml->addNode('changePurposeLineStatus', $deff['line_status']);
            $xml->addNode('adidasOrderNumber', $deff['adidas_ordernumber']);
            $xml->addNode('adidasArticleNumber', $deff['adidas_articlenumber']);
            $xml->addNode('adidasWorkingNumber', $deff['adidas_articlenumber']);
            $xml->addNode('requestDate', $deff['buyer_requestdate']);
            $xml->addNode('adidasRequestDate', $deff['buyer_requestdate']);
            $xml->addNode('adidasPlanDate', $deff['buyer_requestdate']);
            $xml->addNode('priorityAndOrderType', $deff['buyer_requestdate']);
            $xml->addNode('orderQty', $deff['order_quantity']);
            $xml->addNode('orderUOM', $deff['uom']);
            $xml->addNode('season', $deff['season']);
            $xml->addNode('lineRemarks', $deff['line']);
            $xml->addNode('materialReferenceNumber', $deff['ref']);
            $xml->startBranch('materialDescriptor');
                $xml->addNode('materialDescription', $deff['material_name']);
                $xml->addNode('materialColor', $deff['material_colour']);
                $xml->addNode('sustainableMaterialComposition', $deff['material_name']);
                $xml->addNode('sustainableMaterialComposition', $deff['material_name']);
                $xml->addNode('colorMatchingNote', $deff['material_colour']);
                $xml->addNode('materialSectionName', $deff['material_colour']);
                $xml->addNode('weightValue', $deff['weight']);
                $xml->addNode('weightUOM', $deff['uom_weight']);
                $xml->addNode('widthValue', $deff['width']);
                $xml->addNode('widthUOM', $deff['uom_width']);
                $xml->addNode('thicknessValue', $deff['uom_width']);
                $xml->addNode('thicknessUOM', $deff['uom_width']);
                $xml->addNode('lengthValue', $deff['length']);
                $xml->addNode('lengthUOM', $deff['uom_length']);
                $xml->addNode('heightValue', $deff['height']);
                $xml->addNode('heightUOM', $deff['uom_height']);
                $xml->addNode('size', $deff['size']);
            $xml->endBranch();
            $xml->addNode('unit_price', $deff['unit_price']);
            $xml->addNode('linkExtension1', $deff['additional1']);
            $xml->addNode('linkExtension2', $deff['additional1']);
            $xml->addNode('linkExtension3', $deff['additional1']);
            $xml->addNode('linkExtension4', $deff['additional1']);
            $xml->addNode('linkExtension5', $deff['additional1']);
        $xml->endBranch();
        }
        // // Start branch 'cars'
        $xml->endBranch();

        // Pass the XML to the view
        $data = array();
        $data['xml'] = $xml->getXml(FALSE);


        $nama = $purpose.'_'.$b.'_'. date('d-m-Y') .'.xml';
  		header('Content-Type: text/xml');
		header('Content-Disposition: attachment; filename='.$nama);
        $this->load->view('xml', $data);
    }*/

	public function report_t1t2()
	{
		$data['title_page'] = "APPAREL ONE INDONESIA";
		$data['desc_page']  = "REPORT T1 & T2";
		$data['content']    = "report/report_t1t2";

		$this->load->view('layout', $data);
	}
	function xls_pilih_t1t2()
	{
		$this->load->view('supplier/report/xls_pilih_t1t2');
	}
	function new_pl_ajax()
	{
		//$user = $this->session->userdata('user_id');
		$user = $this->input->post('user'); //for testing postman purpose
		$m_warehouse_id = $this->input->post('m_warehouse_id');
		$users = $this->m_new_pl->get_datatables($user, $m_warehouse_id);
		$count = $this->m_new_pl->count_all($user, $m_warehouse_id);
		$count_filtered = $this->m_new_pl->count_filtered($user, $m_warehouse_id);
		$draw  = $this->input->post('draw');

		//
		$data  = array();
		foreach ($users as $po) {
			$warna = $this->m_new_pl->get_warna($po->c_order_id);
			$wh = $po->m_warehouse_id;
			if ($wh == 1000013) {
				$whs = "Acc AOI 2";
			} elseif ($wh == 1000011) {
				$whs = "Fbr AOI 2";
			} elseif ($wh == 1000001) {
				$whs = "Fbr AOI 1";
			} else {
				$whs = "Acc AOI 1";
			}

			//
			$nestedData['checkbox']  = "<input type='checkbox' class='$po->c_order_id' onclick='return add($po->c_order_id)' id='id" . $po->documentno . "'>";
			$nestedData['po_number'] = "<label for='id" . $po->documentno . "'>" . $po->documentno . "</label>";
			$nestedData['warehouse'] = $whs;
			$nestedData['warna'] = $warna;
			$data[] = $nestedData;
		}

		//
		$output = array(
			"draw" => intval($draw),
			"recordsTotal" => intval($count),
			"recordsFiltered" => intval($count_filtered),
			"data" => $data
		);
		echo json_encode($output);
	}
	function upload_pi()
	{
		if ($_GET['token'] == $this->session->userdata('session_id')) {
			$data['c_bpartner_id'] = $this->session->userdata('user_id');
			$name = str_replace(array('#'), '_', $_FILES['po']['name']);
			$data['c_order_id'] = $_GET['c_order_id'];
			$data['file_upload_supplier']  = substr(md5(date('Y-m-d H:i:s') . $data['c_order_id']), 0, 10) . '-' . $name;
			//$data['user_id']    = $this->session->userdata('user_id');
			$upl = move_uploaded_file($_FILES['po']['tmp_name'], $_SERVER['DOCUMENT_ROOT'] . '/file/pi_supplier/' . $data['file_upload_supplier']);
			if ($upl) {
				$a = $this->db->insert('m_pi_file', $data);
				echo 1;
			} else {
				echo 0;
			}
		} else {
			redirect(base_url('user/login'));
		}
	}
	function listpi_aoi()
	{
		//print_r($_GET);exit();
		if ($_GET['token'] == $this->session->userdata('session_id')) {
			$this->load->view('supplier/data/detail/list_pi_aoi');
		} else {
			redirect(base_url('user/login'));
		}
	}
	function download_pi_file()
	{
		if ($_GET['token'] == $this->session->userdata('session_id')) {
			$file = $this->db->where('m_pi_file_id', $_GET['id'])->get('m_pi_file')->result()[0]->file_upload_admin;
			header('location:' . base_url('file/pi_purchase/' . $file));
		} else {
			redirect(base_url('user/login'));
		}
	}
	function listpi_file()
	{
		$this->db->where('c_order_id', $_GET['id']);
		$this->db->where('c_bpartner_id', $this->session->userdata('user_id'));
		$pi_file = $this->db->get('m_pi_file')->result();
		$draw = "<table class='table table-striped table-bordered table-responsive'>
					<thead class='bg-green'>
						<tr>
							<th width='10px'>No.</th>
							<th>Upload Date</th>
							<th>file Name</td>
						</tr>
					</thead>
					<tbody>";
		$no = 1;
		foreach ($pi_file as $pi) {
			$draw .= '<tr>' .
				'<td class="text-center">' . $no++ . '</td>' .
				'<td>' . date("d-m-Y H:i:s", strtotime($pi->create_date)) . '</td>' .
				'<td>' . str_replace(explode('-', $pi->file_upload_supplier)[0] . '-', '', $pi->file_upload_supplier) . '</td>' .
				'</tr>';
		}

		$draw .= '</tbody></table>';
		echo $draw;
	}

	function update_awb()
	{
		$data['title_page'] = "PURCHASE ORDER";
		$data['desc_page']	= "Update AWB";
		$data['content']	= "data/update_awb";
		$this->load->view('layout', $data);
	}
	function xls_update_awb()
	{
		$this->load->view('supplier/report/xls_update_awb');
	}
	function edit_awb()
	{
		$data = $_FILES['file'];
		include $_SERVER["DOCUMENT_ROOT"] . '/library/Classes/PHPExcel/IOFactory.php';
		$path = $_SERVER["DOCUMENT_ROOT"] . "/file/awb/";
		if (
			$data["type"] != 'application/kset' &&
			$data["type"] != 'application/vnd.ms-excel' &&
			$data["type"] != 'application/xls' &&
			$data["type"] != 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet' &&
			$data["type"] != 'application/ms-excel'
		) {
			echo "<br><div class='alert alert-danger text-center'>File extention is not support, make sure that the type is <b>.xls</b></div>";
		} else {
			$datas          = move_uploaded_file($data["tmp_name"], $path . $data['name']);
			$inputFileName  = $path . $data['name'];
			$objPHPExcel    = PHPExcel_IOFactory::load($inputFileName);
			$sheetCount     = $objPHPExcel->getSheetCount();
			$sheet          = $objPHPExcel->getSheetNames();
			$sheetData      = $objPHPExcel->getActiveSheet()->toArray(null, true, true, true);
			foreach ($sheetData as $a => $b) {
				if ($a > 1) {
					//            $where = array(
					// 'no_packinglist'    => $b['A'],
					// 'kst_invoicevendor' => $b['B'],
					// 'c_bpartner_id'     => $b['D']
					//             );
					$no_pl = $b['A'];
					$inv   = $b['B'];
					$bp    = $b['D'];
					$sql = "select * from po_detail where no_packinglist = '$no_pl' and kst_invoicevendor = '$inv' and c_bpartner_id = $bp ";
					$cekpodetail =  $this->db->query($sql);
					// $this->db->where($where);
					//     $cekpodetail = $this->db->get('po_detail');
					if ($cekpodetail->num_rows() > 0) {
						$x = $cekpodetail->result()[0];
						$x->no_packinglist    = $b['A'];
						$x->kst_invoicevendor = $b['B'];
						$x->kst_resi          = $b['C'];
						$x->c_bpartner_id     = $b['D'];
						$excel[]              = $x;
					} else {
						$excel[] = array();
					}
				}
			}
			echo "<br><table class='table table-bordered'>";
			echo "<tr class='bg-green'><th class='text-center'>No Packinglist</th><th class='text-center'>AWB<th class='text-center'>Status</th></tr>";

			foreach ($excel as $a => $b) {

				if (!empty($b->no_packinglist) && !empty($b->kst_invoicevendor) && !empty($b->kst_resi)) {
					if (sizeof($excel) != 0) {
						$insert = $this->update_awb_action($b);
					} else {
						$insert = 'There is no record found!';
					}
					$this->table_status_update_awb($sheetData[$a + 2]['A'], $b, $insert);
				}
			}
			echo "</table>";
		}
	}

	private function update_awb_action($data)
	{
		$input['kst_resi']          = $data->kst_resi;

		$where['no_packinglist']    = $data->no_packinglist;
		$where['kst_invoicevendor'] = $data->kst_invoicevendor;
		$where['c_bpartner_id']     = $data->c_bpartner_id;

		$cek = $this->db->where($where)->get('po_detail');

		if ($cek->num_rows() != 0) {

			$this->db->where($where);
			$this->db->update('po_detail', $input);

			$where_erp = array(
				'kst_packinglist'   => $data->no_packinglist,
				'kst_invoicevendor' => $data->kst_invoicevendor,
				'c_bpartner_id'     => $data->c_bpartner_id
			);

			$this->db2->where($where_erp);
			$cekerp = $this->db2->get('kst_invoicevendor');
			if ($cekerp->num_rows() != 0) {
				$updated = date('Y-m-d H:i:s');
				$detail = array(
					'kst_resi'  => $data->kst_resi,
					'updated'   => $updated,
					'updatedby' => '1002062'
				);
				$this->db2->where($where_erp);
				$this->db2->update('kst_invoicevendor', $detail);
			}
			return "Success!";
		} else {
			return "No data found!";
		}
	}
	private function table_status_update_awb($id, $data = NULL, $status = 0)
	{

		if ($data != NULL) {
			echo "<tr>";
			echo "<td class='text-center'>" . $data->no_packinglist . "</td>";
			echo "<td class='text-center'>" . $data->kst_resi . "</td>";
			echo "<td class='text-center'><b>" . $status . "</b></td>";
			echo "</tr>";
		}
	}
}

//6 Maret 2018 - Adit
//update seluruh redirect yang awalnya ke pl_list, diarahkan ke pl_list_today
//06 April 2018 - Edi
//update seluruh redirect yang awalnya ke pl_list, diarahkan ke pl_list_ori
//9 April 2018 - Adit
//Penambahan function download t1t2 report
//27 Desember 2018 - Budi
//Penambahan Function Download XML
//12/6/2020 REQ20065A005 request arina nama supplier tanpa di preg replace
