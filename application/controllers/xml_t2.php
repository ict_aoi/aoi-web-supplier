<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Xml extends CI_Controller {

	function __construct(){
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
		$CI = &get_instance();
  		$this->db2 = $CI->load->database('db2',TRUE);
    }

    public function index(){
		$data['title_page'] = "DATA";
		$data['desc_page']	= "UPLOAD XML T2 CONFIRMATION";
		$data['content']	= "data/temp_xml_t2";
		$this->load->view('layout',$data);
	}

	public function upload_xml()
	{

		$data = isset($_FILES['file']) ? $_FILES['file'] : array('type' =>  'asd');
		// $path = $_SERVER["DOCUMENT_ROOT"]."/file/xml/";
		$path = $_SERVER["DOCUMENT_ROOT"]."/file/xml/M2/"; //for T2 Confirmation template
		if ($data['type'] != 'text/xml') {

			$data['title_page'] = "DATA";
			$data['desc_page']	= "UPLOAD XML";
			$data['content']	= "data/temp_xml";

			$data['success'] 	= 'File XML Not Found';
			// $data['data_xmls'] 	= $data_xmls;

		}else{
			error_reporting(0);
            ini_set('display_errors', 0);
			$move_file = move_uploaded_file($data['tmp_name'], $path.$data['name']);

			if ($move_file) {
				// $path_file = $_SERVER["DOCUMENT_ROOT"]."/file/xml/".$data['name'];
				$path_file = $_SERVER["DOCUMENT_ROOT"]."/file/xml/M2/".$data['name']; //for T2 Confirmation template

				$PurchaseOrders = simplexml_load_file($path_file);

				if ($PurchaseOrders === false) {
					unlink($path_file);
					// $status = 'Error, Cannot create object';
					redirect($_SERVER['HTTP_REFERER']);
				}

				$PONumber = $PurchaseOrders->PONumber;
				$POPurpose = $PurchaseOrders->POPurpose;
				$POVersionNumber = $PurchaseOrders->POVersionNumber;
				$POResponseVersionNumber = $PurchaseOrders->POResponseVersionNumber;
				$messageGenerationDateTime = $PurchaseOrders->messageGenerationDateTime;
				$latestUpdateDateTime = $PurchaseOrders->latestUpdateDateTime;
				$originalPOIssueDate = $PurchaseOrders->originalPOIssueDate;
				$adidasPODownloadDate = $PurchaseOrders->adidasPODownloadDate;
				$currency = $PurchaseOrders->currency;
				$paymentTerms = $PurchaseOrders->paymentTerms;
				$headerRemarks = $PurchaseOrders->headerRemarks;
				$recipient = $PurchaseOrders->recipient;
				$senderName = $PurchaseOrders->sender->senderName;
				$senderPerson = $PurchaseOrders->sender->senderPerson;
				$buyerName = $PurchaseOrders->buyer->buyerName;
				$buyerAddressLine1 = $PurchaseOrders->buyer->buyerAddressLine1;
				$buyerAddressLine2 = $PurchaseOrders->buyer->buyerAddressLine2;
				$buyerAddressLine3 = $PurchaseOrders->buyer->buyerAddressLine3;
				$buyerAddressLine4 = $PurchaseOrders->buyer->buyerAddressLine4;
				$buyerCity = $PurchaseOrders->buyer->buyerCity;
				$buyerProvince = $PurchaseOrders->buyer->buyerProvince;
				$buyerCountryCode = $PurchaseOrders->buyer->buyerCountryCode;
				$buyerPostalCode = $PurchaseOrders->buyer->buyerPostalCode;
				$buyerContactPerson = $PurchaseOrders->buyer->buyerContactPerson;
				$buyerContactTelNumber = $PurchaseOrders->buyer->buyerContactTelNumber;
				$buyerContactEmail = $PurchaseOrders->buyer->buyerContactEmail;
				$sellerName = $PurchaseOrders->sellerName;
				$actualManufacturerCode = $PurchaseOrders->actualManufacturer->actualManufacturerCode;
				$actualManufacturerName = $PurchaseOrders->actualManufacturer->actualManufacturerName;
				$actualManufacturerAddressLine1 = $PurchaseOrders->actualManufacturer->actualManufacturerAddressLine1;
				$actualManufacturerAddressLine2 = $PurchaseOrders->actualManufacturer->actualManufacturerAddressLine2;
				$actualManufacturerAddressLine3 = $PurchaseOrders->actualManufacturer->actualManufacturerAddressLine3;
				$actualManufacturerAddressLine4 = $PurchaseOrders->actualManufacturer->actualManufacturerAddressLine4;
				$actualManufacturerCity = $PurchaseOrders->actualManufacturer->actualManufacturerCity;
				$actualManufacturerProvince = $PurchaseOrders->actualManufacturer->actualManufacturerProvince;
				$actualManufacturerCountryCode = $PurchaseOrders->actualManufacturer->actualManufacturerCountryCode;
				$actualManufacturerPostalCode = $PurchaseOrders->actualManufacturer->actualManufacturerPostalCode;
				$shipToCode = $PurchaseOrders->shipTo->shipToCode;
				$shipToName = $PurchaseOrders->shipTo->shipToName;
				$shipToAddressLine1 = $PurchaseOrders->shipTo->shipToAddressLine1;
				$shipToAddressLine2 = $PurchaseOrders->shipTo->shipToAddressLine2;
				$shipToAddressLine3 = $PurchaseOrders->shipTo->shipToAddressLine3;
				$shipToAddressLine4 = $PurchaseOrders->shipTo->shipToAddressLine4;
				$shipToCity = $PurchaseOrders->shipTo->shipToCity;
				$shipToProvince = $PurchaseOrders->shipTo->shipToProvince;
				$shipToCountryCode = $PurchaseOrders->shipTo->shipToCountryCode;
				$shipToPostalCode = $PurchaseOrders->shipTo->shipToPostalCode;
				$T1Customer = $PurchaseOrders->T1Customer;
				$forwarder = $PurchaseOrders->ShippingInformation->forwarder;
				$incoterm = $PurchaseOrders->ShippingInformation->incoterm;
				$shipMode = $PurchaseOrders->ShippingInformation->shipMode;
				$countryOfOrigin = $PurchaseOrders->ShippingInformation->countryOfOrigin;
				$packingInstruction = $PurchaseOrders->ShippingInformation->packingInstruction;
				$shippingInstruction = $PurchaseOrders->ShippingInformation->shippingInstruction;
				$headerExtension = $PurchaseOrders->headerExtension;

				if (!isset($PONumber) || $PONumber == '') {
					$status = 'Ups, Po Number Not Found..!';
				}

				$data_header = array(
								'PONumber' => trim($PONumber),
								'POPurpose' => trim($POPurpose),
								'POVersionNumber' => trim($POVersionNumber),
								'POResponseVersionNumber' => trim($POResponseVersionNumber),
								'messageGenerationDateTime' => trim($messageGenerationDateTime) != '' ? trim($messageGenerationDateTime) : NULL,
								'latestUpdateDateTime' => trim($latestUpdateDateTime) != '' ? trim($latestUpdateDateTime) : NULL,
								'originalPOIssueDate' => trim($originalPOIssueDate) != '' ? trim($originalPOIssueDate) : NULL,
								'adidasPODownloadDate' => trim($adidasPODownloadDate) != '' ? trim($adidasPODownloadDate) : NULL,
								'currency' => trim($currency),
								'paymentTerms' => trim($paymentTerms),
								'headerRemarks' => trim($headerRemarks),
								'recipient' => trim($recipient),
								'senderName' => trim($senderName),
								'senderPerson' => trim($senderPerson),
								'buyerName' => trim($buyerName),
								'buyerAddressLine1' => trim($buyerAddressLine1),
								'buyerAddressLine2' => trim($buyerAddressLine2),
								'buyerAddressLine3' => trim($buyerAddressLine3),
								'buyerAddressLine4' => trim($buyerAddressLine4),
								'buyerCity' => trim($buyerCity),
								'buyerProvince' => trim($buyerProvince),
								'buyerCountryCode' => trim($buyerCountryCode),
								'buyerPostalCode' => trim($buyerPostalCode),
								'buyerContactPerson' => trim($buyerContactPerson),
								'buyerContactTelNumber' => trim($buyerContactTelNumber),
								'buyerContactEmail' => trim($buyerContactEmail),
								'sellerName' => trim($sellerName),
								'actualManufacturerCode' => trim($actualManufacturerCode),
								'actualManufacturerName' => trim($actualManufacturerName),
								'actualManufacturerAddressLine1' => trim($actualManufacturerAddressLine1),
								'actualManufacturerAddressLine2' => trim($actualManufacturerAddressLine2),
								'actualManufacturerAddressLine3' => trim($actualManufacturerAddressLine3),
								'actualManufacturerAddressLine4' => trim($actualManufacturerAddressLine4),
								'actualManufacturerCity' => trim($actualManufacturerCity),
								'actualManufacturerProvince' => trim($actualManufacturerProvince),
								'actualManufacturerCountryCode' => trim($actualManufacturerCountryCode),
								'actualManufacturerPostalCode' => trim($actualManufacturerPostalCode),
								'shipToCode' => trim($shipToCode),
								'shipToName' => trim($shipToName),
								'shipToAddressLine1' => trim($shipToAddressLine1),
								'shipToAddressLine2' => trim($shipToAddressLine2),
								'shipToAddressLine3' => trim($shipToAddressLine3),
								'shipToAddressLine4' => trim($shipToAddressLine4),
								'shipToCity' => trim($shipToCity),
								'shipToProvince' => trim($shipToProvince),
								'shipToCountryCode' => trim($shipToCountryCode),
								'shipToPostalCode' => trim($shipToPostalCode),
								'T1Customer' => trim($T1Customer),
								'forwarder' => trim($forwarder),
								'incoterm' => trim($incoterm),
								'shipMode' => trim($shipMode),
								'countryOfOrigin' => trim($countryOfOrigin),
								'packingInstruction' => trim($packingInstruction),
								'shippingInstruction' => trim($shippingInstruction),
								'headerExtension' => trim($headerExtension)
								);

				$this->db->trans_start();

					$this->db->where('PONumber',trim($PONumber))->delete('xml_t1t2_header');
					$this->db->where('PONumber',trim($PONumber))->delete('xml_t1t2_detail');

					// insert header
					$this->db->insert('xml_t1t2_header', $data_header);

					// insert detail
					foreach ($PurchaseOrders->POLineItem as $POLineItems) {
						$lineItemNumber = $POLineItems->lineItemNumber;
						$changePurposeLineStatus = $POLineItems->changePurposeLineStatus;
						$adidasOrderNumber = $POLineItems->adidasOrderNumber;
						$adidasArticleNumber = $POLineItems->adidasArticleNumber;
						$adidasWorkingNumber = $POLineItems->adidasWorkingNumber;
						$requestDate = $POLineItems->requestDate;
						$adidasRequestDate = $POLineItems->adidasRequestDate;
						$adidasPlanDate = $POLineItems->adidasPlanDate;
						$priorityAndOrderType = $POLineItems->priorityAndOrderType;
						$orderQty = $POLineItems->orderQty;
						$orderUOM = $POLineItems->orderUOM;
						$season = $POLineItems->season;
						$lineRemarks = $POLineItems->lineRemarks;
						$materialReferenceNumber = $POLineItems->materialReferenceNumber;
						$materialDescription = $POLineItems->materialDescriptor->materialDescription;
						$materialColor = $POLineItems->materialDescriptor->materialColor;
						$sustainableMaterialComposition = $POLineItems->materialDescriptor->sustainableMaterialComposition;
						$sustainableMaterialComposition = $POLineItems->materialDescriptor->sustainableMaterialComposition;
						$colorMatchingNote = $POLineItems->materialDescriptor->colorMatchingNote;
						$materialSectionName = $POLineItems->materialDescriptor->materialSectionName;
						$weightValue = $POLineItems->materialDescriptor->weightValue;
						$weightUOM = $POLineItems->materialDescriptor->weightUOM;
						$widthValue = $POLineItems->materialDescriptor->widthValue;
						$widthUOM = $POLineItems->materialDescriptor->widthUOM;
						$thicknessValue = $POLineItems->materialDescriptor->thicknessValue;
						$thicknessUOM = $POLineItems->materialDescriptor->thicknessUOM;
						$lengthValue = $POLineItems->materialDescriptor->lengthValue;
						$lengthUOM = $POLineItems->materialDescriptor->lengthUOM;
						$heightValue = $POLineItems->materialDescriptor->heightValue;
						$heightUOM = $POLineItems->materialDescriptor->heightUOM;
						$size = $POLineItems->materialDescriptor->size;
						$unit_price = $POLineItems->unit_price;
						$linkExtension1 = $POLineItems->linkExtension1;
						$linkExtension2 = $POLineItems->linkExtension2;
						$linkExtension3 = $POLineItems->linkExtension3;
						$linkExtension4 = $POLineItems->linkExtension4;
						$linkExtension5 = $POLineItems->linkExtension5;


						$data_xmls[] = array(
									'POPurpose' => trim($POPurpose),
									'PONumber' => trim($PONumber),
									'currency' => trim($currency),
									'senderName' => trim($senderName),
									'buyerName' => trim($buyerName),
									'lineItemNumber' => trim($lineItemNumber),
									'adidasOrderNumber' => trim($adidasOrderNumber),
									'orderQty' => trim($orderQty)
						);


						$data_detail = array(
									'PONumber' => trim($PONumber),
									'lineItemNumber' => trim($lineItemNumber),
									'changePurposeLineStatus' => trim($changePurposeLineStatus),
									'adidasOrderNumber' => trim($adidasOrderNumber),
									'adidasArticleNumber' => trim($adidasArticleNumber),
									'adidasWorkingNumber' => trim($adidasWorkingNumber),
									'requestDate' => trim($requestDate) != '' ? trim($requestDate) : NULL,
									'adidasRequestDate' => trim($adidasRequestDate) != '' ? trim($adidasRequestDate) : NULL,
									'adidasPlanDate' => trim($adidasPlanDate) != '' ? trim($adidasPlanDate) : NULL,
									'priorityAndOrderType' => trim($priorityAndOrderType),
									'orderQty' => trim($orderQty),
									'orderUOM' => trim($orderUOM),
									'season' => trim($season),
									'lineRemarks' => trim($lineRemarks),
									'materialReferenceNumber' => trim($materialReferenceNumber),
									'materialDescription' => trim($materialDescription),
									'materialColor' => trim($materialColor),
									'sustainableMaterialComposition' => trim($sustainableMaterialComposition),
									'sustainableMaterialComposition' => trim($sustainableMaterialComposition),
									'colorMatchingNote' => trim($colorMatchingNote),
									'materialSectionName' => trim($materialSectionName),
									'weightValue' => trim($weightValue),
									'weightUOM' => trim($weightUOM),
									'widthValue' => trim($widthValue),
									'widthUOM' => trim($widthUOM),
									'thicknessValue' => trim($thicknessValue),
									'thicknessUOM' => trim($thicknessUOM),
									'lengthValue' => trim($lengthValue),
									'lengthUOM' => trim($lengthUOM),
									'heightValue' => trim($heightValue),
									'heightUOM' => trim($heightUOM),
									'size' => trim($size),
									'unit_price' => trim($unit_price),
									'linkExtension1' => trim($linkExtension1),
									'linkExtension2' => trim($linkExtension2),
									'linkExtension3' => trim($linkExtension3),
									'linkExtension4' => trim($linkExtension4),
									'linkExtension5' => trim($linkExtension5)
							);

						$this->db->insert('xml_t1t2_detail', $data_detail);
				}


				if ($this->db->trans_status() === FALSE)
				{
			        $status = 'Ups, Something Wrong..!';
				}else{
					$status = 'File XML has been sent successfully';
				}

				$this->db->trans_complete();

				$data['title_page'] = "DATA";
				$data['desc_page']	= "UPLOAD XML";
				$data['content']	= "data/temp_xml";

				$data['success'] 	= $status;
				$data['data_xmls'] 	= $data_xmls;



			}
		}

		$this->load->view('layout', $data);
	}

	public function index2(){
		$data['title_page'] = "DATA";
		$data['desc_page']	= "UPLOAD XML T2 CONFIRMATION";
		$data['content']	= "data/temp_xml_t2";
		$this->load->view('layout',$data);
	}

}