<div class="row">
	<div class="col-sm-12">
	
        <?php 
            if($status=='200'){
        ?>
        <!-- <input type="hidden" id="id" value="<?//=$id;?>"> -->
        <table class="table table-striped table-bordered" id="table">
            <tr>
                <td  class="text-center bg-green">
                    AWB Number
                </td>
                <td  class="text-center">
                    <?php echo $hasil->CompletedTrackDetails->TrackDetails->TrackingNumber;?>
                </td>
            </tr>
        </table>
        
		<table class="table table-bordered" id="table">
			<thead>
                <tr>
                    <td colspan="4" class="text-center bg-blue">                
                        SHIPMENT INFO
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2" width="500px" class="text-center bg-green">Shipper Address</td>
                    <td colspan="2" width="500px" class="text-center bg-green">Destination Address</td>
                </tr>
                <tr>
                    <td bgcolor="#bfbaba">City</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->ShipperAddress->City;?></td>
                    <td bgcolor="#bfbaba">City</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->DestinationAddress->City;?></td>
                </tr>
                <tr>
                    <td bgcolor="#bfbaba">State/Province Code</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->ShipperAddress->StateOrProvinceCode;?></td>
                    <td bgcolor="#bfbaba">State/Province Code</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->DestinationAddress->StateOrProvinceCode;?></td>
                </tr>

                <tr>
                    <td bgcolor="#bfbaba">Country Code</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->ShipperAddress->CountryCode;?></td>
                    <td bgcolor="#bfbaba">Country Code</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->DestinationAddress->CountryCode;?></td>
                </tr>

                <tr>
                    <td bgcolor="#bfbaba">Country Name</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->ShipperAddress->CountryName;?></td>
                    <td bgcolor="#bfbaba">Country Name</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->DestinationAddress->CountryName;?></td>
                </tr>
                <tr>
                    <td colspan="4" class="text-center bg-green">Package Weight</td>
                </tr>
                <tr>
                    <td bgcolor="#bfbaba">Units</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->PackageWeight->Units;?></td>
                    <td bgcolor="#bfbaba">Value</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->PackageWeight->Value;?></td>
                </tr>
                <tr>
                    <td colspan="4" class="text-center bg-green">Package Dimensions</td>
                </tr>
                <tr>
                    <td bgcolor="#bfbaba">Length</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->PackageDimensions->Length;?></td>
                    <td bgcolor="#bfbaba">Width</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->PackageDimensions->Width;?></td>
                </tr>
                <tr>
                    <td bgcolor="#bfbaba">Height</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->PackageDimensions->Height;?></td>
                    <td bgcolor="#bfbaba">Units</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->PackageDimensions->Units;?></td>
                </tr>
                <tr>
                    <td colspan="4" class="text-center bg-green">Shipment Weight</td>
                </tr>
                <tr>
                    <td bgcolor="#bfbaba">Units</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->ShipmentWeight->Units;?></td>
                    <td bgcolor="#bfbaba">Value</font></td>
                    <td><?php echo $hasil->CompletedTrackDetails->TrackDetails->ShipmentWeight->Value;?></td>
                </tr>
                <tr>
                    <td colspan="4" class="text-center bg-green">Shipment Event</td>
                </tr>
                <tr>
                    <td bgcolor="#bfbaba">Date & Time</font></td>
                    <td bgcolor="#bfbaba">Event Type</font></td>
                    <td bgcolor="#bfbaba">Event Description</font></td>
                    <td bgcolor="#bfbaba">Country</font></td>
                </tr>
                <?php 
                $hitung = count( $hasil->CompletedTrackDetails->TrackDetails->Events);
                // var_dump(($hitung));
                if($hitung==1){
                    echo "<tr>";
                        echo "<td>".$hasil->CompletedTrackDetails->TrackDetails->Events->Timestamp."</td>";
                        echo "<td>".$hasil->CompletedTrackDetails->TrackDetails->Events->EventType."</td>";
                        echo "<td>".$hasil->CompletedTrackDetails->TrackDetails->Events->EventDescription ."</td>";
                        echo "<td>".$hasil->CompletedTrackDetails->TrackDetails->Events->Address->CountryName."</td>";
                    echo "</tr>";
                }
                else{
                    foreach ($hasil->CompletedTrackDetails->TrackDetails->Events as $se){
                        echo "<tr>";
                            echo "<td>".$se->Timestamp."</td>";
                            echo "<td>".$se->EventType."</td>";
                            echo "<td>".$se->EventDescription."</td>";
                            echo "<td>".$se->Description."</td>";
                        echo "</tr>";
                    }    
                }
                ?>			
			</tbody>
		</table>
        <?php
            }
            else{
        ?>
            <div class="alert alert-danger" role="alert">
                FEDEX IS NOT FOUND!, PLEASE CHECK AGAIN. THANK YOU
            </div>
        <?php
            }
        ?>
	</div>
	<div class="col-sm-12 view">
	
	</div>
</div>

<script type="text/javascript">
	var table;
    $(document).ready(function() {

        //datatables
        table = $('#table').DataTable();

    });
</script>



