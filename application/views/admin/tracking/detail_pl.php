<div class="row">
	<div class="col-sm-12">
	
		<input type="hidden" id="id" value="<?=$id;?>">
		<table class="table table-striped table-bordered" id="table">
			<thead>
				<tr class="bg-green">
				<th class="text-center">#</th>
				<th>PL NUMBER</th>
				<!--th>SJ / DN </th-->
				<th>INVOICE</th>
				<!--th>RESI / AWB</th-->
				<!-- <th class="text-center">QC-CHECK REPORT</th>
				<th class="text-center">QC REPORT</th> -->
				<th class="text-center">DETAIL</th>
				<th class="text-center">AWB</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>
	<div class="col-sm-12 view">
	
	</div>
</div>

<script type="text/javascript">
	var table;
    $(document).ready(function() {

        //datatables
        table = $('#table').DataTable({ 

            "processing": true, 
            "serverSide": true, 
            "order": [],
            "orderable" : true, 
            
            "ajax": {
                "url": "<?php echo site_url('admin/packinglist')?>",
                "type": "POST",
                "datatype":"json",
                 "data":function(data) {
					 	data.id    = $('#id').val();
                        data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                      },
				 
				//  {  '<?php //echo $this->security->get_csrf_token_name(); ?>' : '<?php// echo $this->security->get_csrf_hash(); ?>' }
            },

            
            "columns": [
		          { "data": "no" },
		          { "data": "no_packinglist" },
		          { "data": "kst_invoicevendor" },
		          {
				    render: function(data, type, full, meta)
				    {
				    	
				     var no_packinglist = full.no_packinglist;
				     var kst_suratjalanvendor = full.kst_suratjalanvendor;
				     var kst_invoicevendor = full.kst_invoicevendor;
				     var kst_resi = full.kst_resi;
				     var url_detail = full.url_detail;
				     var data 	= full.data;
				     
				     return '<center><a href="javascript:void(0)" class="link" id="link" onClick=detail(\''+url_detail+'\',\''+data+'\') data="'+no_packinglist+'&sj='+kst_suratjalanvendor+'&inv='+kst_invoicevendor+'&awb='+kst_resi+'">Detail</a></center>';
				    }
				   },
				   { 
					render: function(data, type, full, meta)
				    {
				    	
				     var no_packinglist = full.no_packinglist;
				     var kst_suratjalanvendor = full.kst_suratjalanvendor;
				     var kst_invoicevendor = full.kst_invoicevendor;
				     var kst_resi = full.kst_resi;
				     var url_resi = full.url_resi;
				     var data 	= full.data;
				     
					 if(kst_resi=='-'||kst_resi==null){
						 return '<center>'+kst_resi+'</center>';
					 }
					 else{
						return '<center><a href="javascript:void(0)" class="link" id="link" onClick=detail(\''+url_resi+'\',\''+data+'\') data="'+no_packinglist+'&sj='+kst_suratjalanvendor+'&inv='+kst_invoicevendor+'&awb='+kst_resi+'">'+kst_resi+'</a></center>';
					 }
				    }
					//    "data": "kst_resi" 
				   },
		       ]

        });

    });

	function upload_file(id,url){
		var form_data = new FormData($('#form_'+id)[0]);
		var x = confirm('Do you really want to upload a file?');
		if(x==true){
			$.ajax({
	        url: url,
	        type: "POST",
	        data: form_data,
	        enctype: 'multipart/form-data',
	        processData: false,
	        contentType: false,
	        success: function( values )
	        {
	        	if (values == 1) {
					$('#table').DataTable().ajax.reload();
	            } 
	        }
	      })
		  .fail(function(jqXHR, textStatus) {
	          
	          $.unblockUI();
	          alert('File upload failed ...');
	      });
		}else{
			$('#table').DataTable().ajax.reload();
		}
	}	
 	
     function detail(url,data) {
     	$.ajax({
				url:url,
				data:data,
				type:"POST",
				success:function(data){
					var modal = $('#myModal_ > div > div');
					$('#myModal_').modal('show');
					modal.children('.modal-header').children('.modal-title').html('DETAIL DATA');
					modal.parent('.modal-dialog').addClass('modal-lg');
					modal.children('.modal-body').html(data);

				}
			})
			return false;
     }
    function edit_header(url){    	
			$.ajax({
				url:url,
				success:function(data){
					$('#myModal').modal('show');
					$('.modal-dialog').removeClass('modal-lg');
					$('.modal-title').html('EDIT DATA');
					$('.modal-body').html(data);
					$('#header').submit(function(){
						$.ajax({
						url:$('#header').attr('action'),
						type: "POST",
						data: $('#header').serialize(),
						success:function(values){
							if(values==1){
								$('#myModal').modal('hide');
								$('#table').DataTable().ajax.reload();
							}else{
								alert("Packinglist Already locked, Refresh Your Page");
								$('#table').DataTable().ajax.reload(); 
							}
						}
					});
						return false;
					})
				}
			})
			return false;
    }
    function isactive(url,data){
    	event.preventDefault();
    	var x = confirm('Do you really want to Delete a Packinglist?');    	
			if(x==true)
				$.ajax({
				url:url,
				type:"POST",
				data:data,
				success:function(value){
					if (value==1) {
						$('#table').DataTable().ajax.reload();
					}else{
						alert('Packinglist Already locked, Refresh Your Page');
						$('#table').DataTable().ajax.reload();
					}
				}
			})
    }
    function lock(url,data){
    	//event.preventDefault();
    	var x = confirm('Do you really want to Lock a Packinglist?');    	
			if(x==true){
				$.ajax({
					url:url,
					type:"POST",
					data:data,
					success:function(value){
						$('#table').DataTable().ajax.reload();
					}
				});
			}
			
    }
    
</script>



