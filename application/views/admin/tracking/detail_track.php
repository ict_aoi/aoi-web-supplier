<div class="row">
	<div class="col-sm-12">
	
        <?php 
            if($status=='200'){
        ?>
        <!-- <input type="hidden" id="id" value="<?//=$id;?>"> -->
        <table class="table table-striped table-bordered" id="table">
            <tr>
                <td  class="text-center bg-green">
                    AWB Number
                </td>
                <td  class="text-center">
                    <?php echo $hasil['AWBInfo']['AWBNumber'];?>
                </td>
            </tr>
        </table>
        
		<table class="table table-bordered" id="table">
			<thead>
                <tr>
                    <td colspan="4" class="text-center bg-blue">                
                        SHIPMENT INFO
                    </td>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td colspan="2" width="500px" class="text-center bg-green">Origin Services</td>
                    <td colspan="2" width="500px" class="text-center bg-green">Destination Services</td>
                </tr>
                <tr>
                    <td bgcolor="#bfbaba">Service Area Code</font></td>
                    <td><?php echo $hasil['AWBInfo']['ShipmentInfo']['OriginServiceArea']['ServiceAreaCode'];?></td>
                    <td bgcolor="#bfbaba">Service Area Code</font></td>
                    <td><?php echo $hasil['AWBInfo']['ShipmentInfo']['DestinationServiceArea']['ServiceAreaCode'];?></td>
                </tr>
                <tr>
                    <td bgcolor="#bfbaba">Description</font></td>
                    <td><?php echo $hasil['AWBInfo']['ShipmentInfo']['OriginServiceArea']['Description'];?></td>
                    <td bgcolor="#bfbaba">Description</font></td>
                    <td><?php echo $hasil['AWBInfo']['ShipmentInfo']['DestinationServiceArea']['Description'];?></td>
                </tr>
               
                <tr>
                    <td bgcolor="#bfbaba">Pieces</font></td>
                    <td><?php echo $hasil['AWBInfo']['ShipmentInfo']['Pieces'];?></td>
                    <td bgcolor="#bfbaba">Weight</font></td>
                    <td><?php echo $hasil['AWBInfo']['ShipmentInfo']['Weight']; echo $hasil['AWBInfo']['ShipmentInfo']['WeightUnit'];?></td>
                </tr>
                <tr>
                    <td bgcolor="#bfbaba">Shipment Date</font></td>
                    <td><?php echo $hasil['AWBInfo']['ShipmentInfo']['ShipmentDate'];?></td>
                    <td bgcolor="#bfbaba">Shipment Desc</font></td>
                    <td><?php echo $hasil['AWBInfo']['ShipmentInfo']['ShipmentDesc'];?></td>
                </tr>
                <tr>
                    <td colspan="4" class="text-center bg-green">Shipper</td>
                </tr>
                <tr>
                    <td bgcolor="#bfbaba">City</font></td>
                    <td><?php echo $hasil['AWBInfo']['ShipmentInfo']['Shipper']['City'];?></td>
                    <td bgcolor="#bfbaba">Country Code</font></td>
                    <td><?php echo $hasil['AWBInfo']['ShipmentInfo']['Shipper']['CountryCode'];?></td>
                </tr>
                <tr>
                    <td colspan="4" class="text-center bg-green">Shipment Event</td>
                </tr>
                <tr>
                    <td bgcolor="#bfbaba">Date</font></td>
                    <td bgcolor="#bfbaba">Time</font></td>
                    <td bgcolor="#bfbaba">Service Event</font></td>
                    <td bgcolor="#bfbaba">Service Area</font></td>
                </tr>
                <?php 
                $hitung = count($hasil['AWBInfo']['ShipmentInfo']['ShipmentEvent']);
                // var_dump(($hitung==5));
                if($hitung==5){
                    echo "<tr>";
                        echo "<td>".$hasil['AWBInfo']['ShipmentInfo']['ShipmentEvent']['Date']."</td>";
                        echo "<td>".$hasil['AWBInfo']['ShipmentInfo']['ShipmentEvent']['Time']."</td>";
                        echo "<td>".$hasil['AWBInfo']['ShipmentInfo']['ShipmentEvent']['ServiceEvent']['Description']."</td>";
                        echo "<td>".$hasil['AWBInfo']['ShipmentInfo']['ShipmentEvent']['ServiceArea']['Description']."</td>";
                    echo "</tr>";
                }
                else{
                    foreach ($hasil['AWBInfo']['ShipmentInfo']['ShipmentEvent'] as $se){
                        echo "<tr>";
                            echo "<td>".$se['Date']."</td>";
                            echo "<td>".$se['Time']."</td>";
                            echo "<td>".$se['ServiceEvent']['Description']."</td>";
                            echo "<td>".$se['ServiceArea']['Description']."</td>";
                        echo "</tr>";
                    }    
                }
                ?>				
			</tbody>
		</table>
        <?php
            }
            else{
        ?>
            <div class="alert alert-danger" role="alert">
                AWB IS NOT FOUND!, PLEASE CHECK AGAIN. THANK YOU
            </div>
        <?php
            }
        ?>
	</div>
	<div class="col-sm-12 view">
	
	</div>
</div>

<script type="text/javascript">
	var table;
    $(document).ready(function() {

        //datatables
        table = $('#table').DataTable();

    });
</script>



