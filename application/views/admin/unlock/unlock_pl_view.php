<div class="row">
	<div class="col-sm-12 view">
		<input type="hidden" value="<?php echo $c_bpartner_id;?>">
		<table class="table table-striped table-bordered" id="tables">
			<thead>
				<tr class="bg-green">
				<th class="text-center" width="20px">#</th>
				<th>PL NUMBER</th>
				<th>SJ / DN </th>
				<th>INVOICE</th>
				<th>RESI / AWB</th>
				<th class="text-center" width="20px">ACTION</th>
				</tr>
			</thead>
			<tbody>
				
			</tbody>
		</table>
	</div>		
</div>

<script type="text/javascript">
var table = $("#tables").DataTable({
		  processing  : true,
		  serverSide  : true,
		  order: [],
		  orderMulti  : true,
          ajax: {
			url: '<?php echo base_url('admin/list_sup_ajax')?>',
			type: 'POST',
			datatype: 'JSON',
			data: { 
                 	'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
                 	'c_bpartner_id': $('#c_bpartner_id').val(),
			}
		  },
		  columns: [
			  {"data" : null, 'sortable' : false},
			  {'data' : 'no_packinglist'},
			  {'data' : 'kst_suratjalanvendor'},
			  {'data' : 'kst_invoice'},
			  {'data' : 'kst_resi'},
			  {'data' : 'lock'}
		  ],
		  language: {
			  processing: "L o a d i n g...<div id='process'></div>",
			  searchPlaceholder: "Masukkan PO Number"
		  },
		  fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
      });
	function lock(nopl,inv,awb,token){
		swal({
			  title: "Are you sure?",
			  text: "Packinglist Will Be Unlocked?",
			  icon: "warning",
			  buttons: true,
			  dangerMode: true,
			})
			.then((willunlock) => {
			  if (willunlock) {
			  	$.ajax({
			  		url:'<?php echo base_url('admin/is_unlock?nopl=');?>'+nopl+'&inv='+inv+'&awb='+awb+'&token='+token,
			  		success:function(value){
			  			if(value==1){
			  				swal("Packinglist Already Unlocked!", {
						      icon: "success",
						      timer: 1600,
			              	  buttons: false,
						    });
						    $('#tables').DataTable().ajax.reload();
			  			}else{
			  				swal("Packinglist Can't Unlocked!");
			  			}
			  		}

			  	});
			    
			  }
			});
	}
</script>