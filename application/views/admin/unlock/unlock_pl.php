<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
    <!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> not stable-->
    <script src="http://code.jquery.com/jquery-migrate-1.1.0.js"></script>
 <script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
<div class="row">
 <div class="col-md-4">
  <div class="alert alert-warning alert-dismissable">
    <strong>Warning!</strong> Please choose one Supplier!
  </div>
 </div>
</div>
<div class="row">
 <div class="col-md-4">
  <form method="POST" action="<?php echo base_url('admin/list_sup_unlock') ?>" id="select_supplier">
   <label>Supplier</label>
    <select class="livesearch" id="c_bpartner_id" name="c_bpartner_id">
    <option value="" id="user_id">Pilih Nama Supplier</option>
    <?php
     foreach ($supplier->result() as $dt) {
      echo "<option value='".$dt->user_id."'>".$dt->nama."</option>";
     }
    ?>
   </select>
   <br>
   <label></label>
   <button type="submit" class="btn btn-flat btn-primary btn-block">Search</button>
   <br>
  </form>
 </div>
</div>
<div id="result">
  
</div>
<script type="text/javascript">
 $(function(){
    $('#tabless').DataTable();
 })
    $(".livesearch").chosen();
$(document).ready(function(){
    $('#select_supplier').submit(function(e){
      e.preventDefault();      
      var _c_bpartner_id = $('#c_bpartner_id').val();
      if(!_c_bpartner_id){
        swal({
              title: "Warning!!",
              text: "Please Select Supplier First!",
              icon: "warning",
              dangerMode: true,
              timer: 1600,
              buttons: false
          });
        $('#result').html(' ');
        return false;
      }

      $.ajax({
        url:$('#select_supplier').attr('action'),
        type: "POST",
        data: $('#select_supplier').serialize(),
        beforeSend:function (){
          progress_bar();
        },
        success:function(values){
          $('#result').html(values);
          dieYou_progress_bar();
        }
      });
    });
  });
</script>