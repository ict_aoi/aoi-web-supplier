
<div class="row">
<?php
  if(isset($_POST['value']) && $_POST['value'] != NULL){
    $value = $_POST['value'];
    $this->db2->where('value',$value);
    $value = $_POST['value'];
  }else{
    $value = '';
  }
?>        
        <div class="col-md-4">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Add New Business Partner</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="" method="POST">
              <div class="box-body">
                <div class="form-group">
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="value" placeholder="Input SP-DM code from ERP" required>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info btn-block"> SEARCH</button>
                <?php
                  if(!empty($_POST['value'])){
                    ?>
                        <div class="col-sm-12">
                              <?php
                                $this->db2->where('value',$value);
                                $name_bp = $this->db2->get('c_bpartner');
                                // $name_sup = $name_bp->row_array()['name'];
                                $c_bpartner_id = $name_bp->row_array()['c_bpartner_id'];

                                        $this->db->where('user_id', $c_bpartner_id);
                            $user_web = $this->db->get('m_user');
                                        if ($user_web->num_rows() > 0) { ?>
                                            <code>Business Partner already registered on the system!</code>
                                        <?php }else{
                                            if ($name_bp->num_rows() > 0) { ?>
                                                <br>
                                                <a href="<?=base_url('admin/add_bp?value='.$value);?>" class="btn btn-block btn-flat btn-success"> Add <?=$name_bp->row_array()['name'];?> to AOI SCM</a>
                                            <?php }else{ ?>
                                                <code>SP DM Code not found in ERP, please input with correct code!</code>
                                         <?php   }
                                        }
                              ?>
                            
                        </div>                        
                    <?php
                  }
                ?>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
    <div class="col-md-8">
		<div class="box box-success">
            <div class="box-header">
                <h3 class="box-title"><i class="fa fa-list"></i> List of Business Partner</h3>
            </div>
            <div class="box-body">
            <table class="table table-striped table-bordered table-responsive" id="bp">
                    <thead class="bg-green">
                        <tr>
                            <th style="width: 20px">#</th>
                            <th class="text-center">Business Partner Name</th>
                            <th class="text-center">C_BPartner_ID</th>
                            <th class="text-center">Email</th>
                            <th class="text-center">Status</th>
                            <th class="text-center">Action</th>				
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                            $no = 1;
                                    $this->db->order_by('nama', 'asc');                                                                                
                            $data = $this->db->get('m_user')->result();
                            foreach ($data as $bp) { ?>
                                <tr>
                                    <td><?=$no++;?></td>
                                    <td><?=$bp->nama;?></td>
                                    <td><?=$bp->user_id;?></td>
                                    <td><?=$bp->email;?></td>
                                    <td><?php
                                        $status = $bp->isactive;
                                        if ($status == 't') { ?>
                                            <a href="<?=base_url('admin/bp_inactive?user_id='.$bp->user_id);?>" class="btn btn-success btn-sm"> Active</a>
                                        <?php }else{ ?>
                                            <a href="<?=base_url('admin/bp_active?user_id='.$bp->user_id);?>" class="btn btn-warning btn-sm"> Inactive</a>
                                        <?php }
                                    ?></td>
                                    <td>
                                        <a href="<?=base_url('admin/edit_bp?user_id='.$bp->user_id);?>" title="Edit BP" style="margin-right:10px"><i class="fa fa-edit"></i></a>
                                        <a href="<?=base_url('admin/delete_bp?user_id='.$bp->user_id);?>" title="Delete BP" style="margin-right:10px"><i class="fa fa-trash"></i></a>
                                    </td>
                                </tr>
                            <?php }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
	</div>
</div>
