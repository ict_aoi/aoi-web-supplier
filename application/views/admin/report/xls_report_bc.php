<?php
  if(isset($_GET['from']) && $_GET['from'] != NULL){
    $dari   = date("Y-m-d 00:00:00",strtotime($_GET['from']));
    $this->db->where('issue_date >=',$dari);
    }
    if(isset($_GET['until']) && $_GET['until'] != NULL){
      $sampai = date("Y-m-d 23:59:59",strtotime($_GET['until']));
      $this->db->where('issue_date <=',$sampai);
    }
  $data_all= $this->db->get('adt_rep_bc_exim')->result();

  header("Content-Type:application/vnd.ms-excel");
  header('Content-Disposition:attachment; filename="Report BC '.date('d-m-Y').'.xls"');
?>

<table border="1px">
  <thead>
    <th style="width: 20px">#</th>
      <th>Supplier</th>
      <th>No Packinglist</th>
      <th>Invoice Vendor</th>
      <th>AWB / Resi</th>
      <th>Documentno PO</th>
      <th>Issued Date</th>
      <th>Warehouse</th>
      <th>Item Code</th>
      <th>Item Name</th>
      <th>Category</th>
      <th>Qty Ordered</th>
      <th>Qty Packinglist</th>
      <th>UoM</th>
      <th>FoC</th>
      <th>ExFactory Date</th>
      <th>ETA</th>
      <th>Created Packing List</th>
      <th>Price</th>
      <th>Total Price</th>
      <th>Currency</th>
  </thead>
  <tbody>
    <?php
      $nomor=1;
      foreach($data_all as $po){ 
    ?>
      <tr>        
      <td><?=$nomor++;?></td>
      <td><?=$po->supplier;?></td>
      <td><?=$po->no_packinglist;?></td>
      <td><?=$po->kst_invoicevendor;?></td>
      <td><?=$po->kst_resi;?></td>
      <td><?=$po->documentno;?></td>
      <td style="mso-number-format:'d-mmm-yyyy'"><?=$po->issue_date;?></td>
      <td><?=$po->warehouse;?></td>
      <td><?=$po->item;?></td>
      <td><?=$po->desc_product;?></td>
      <td><?=$po->category;?></td>
      <td><?=number_format($po->qtyentered,2);?></td>
      <td><?=number_format($po->qty_packinglist,2);?></td>
      <td><?=$po->uomsymbol;?></td>
      <td><?=$po->foc;?></td>
      <td style="mso-number-format:'d-mmm-yyyy'"><?=$po->kst_etddate;?></td>
      <td style="mso-number-format:'d-mmm-yyyy'"><?=$po->kst_etadate;?></td>
      <td style="mso-number-format:'d-mmm-yyyy'"><?=$po->date_input_pl;?></td>
      <td><?=$po->priceactual;?></td>
      <td><?=number_format($po->total_price,4);?></td>
      <td><?=$po->iso_code;?></td>
      </tr>
    <?php }?>
  </tbody>
</table>