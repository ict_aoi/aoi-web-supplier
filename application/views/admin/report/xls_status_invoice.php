<?php
  if(isset($_GET['from']) && $_GET['from'] != NULL){
    // $dari   = date("Y-m-d 00:00:00",strtotime($_GET['from']));
    $dari = $_GET['from'];
    $this->db->where('created_date >=',$dari);
    }
    if(isset($_GET['until']) && $_GET['until'] != NULL){
      // $sampai = date("Y-m-d 23:59:59",strtotime($_GET['until']));
      $sampai = $_GET['until'];
      $this->db->where('created_date <=',$sampai);
    }
    if (isset($_GET['no_invoice']) && $_GET['no_invoice'] != NULL){
      $this->db->where('kst_invoicevendor',$_GET['no_invoice']);      
    }

    $data_all= $this->db->get('status_awb');
        
  header("Content-Type:application/vnd.ms-excel");
  header('Content-Disposition:attachment; filename="Status Invoice.xls"');
?>

<table border="1px">
  <thead>
    <th>Packing List</th>
    <th>Invoice Number</th>
    <th>AWB/Resi Number</th>
    <th>Supplier Name</th>
    <th>Warehouse</th>
    <th>PL Created at</th>
    <th>Nomor Pend.</th>
    <th>Nomor AJU</th>
    <th>Kurs</th>
    <th>BC Date</th>
    <th>No KPBC</th>
    <th>HS Code</th>
    <th>Jenis BC</th>
    <th>Gross Weight</th>
    <th>CBM</th>
    <th>FCL</th>
    <th>Port of Discharge</th>
    <th>Remarks WMS</th>
    <th>Status Update Awb</th>
    <th>Updated by</th>
    <th>Updated at</th>
    <th>BM</th>
    <th>PPN</th>
    <th>PPH</th>
    <th>Country</th>
  </thead>
  <tbody>
    <?php foreach($data_all->result() as $data){ ?>
      <tr>
        <td style="mso-number-format:'\@'"><?=$data->no_packinglist;?></td>
        <td style="mso-number-format:'\@'"><?=$data->kst_invoicevendor;?></td>
        <td style="mso-number-format:'\@'"><?=$data->kst_resi;?></td>
        <td style="background-color: GreenYellow"><?=$data->supplier;?></td>
        <td><?=$data->warehouse;?></td>
        <td style="mso-number-format:'Short Date'"><?=$data->created_date;?></td>
        <td style="mso-number-format:'\@'"><?=$data->bc_no;?></td>
        <td style="mso-number-format:'\@'"><?=$data->car_no;?></td>
        <td><?=$data->kurs;?></td>
        <td style="mso-number-format:'Short Date'"><?=$data->bc_date;?></td>
        <td><?=$data->no_kpbc;?></td>      
        <td><?=$data->hs_code;?></td>
        <td><?=$data->jenis_bc;?></td>
        <td><?=$data->aoi_gross_weight;?></td>
        <td><?=$data->aoi_cbm;?></td>
        <td><?=$data->aoi_fcl_type;?></td>
        <td><?=$data->aoi_pod_type;?></td>
        <td><?=$data->remark_wms;?></td>
        <td><?=$data->status_update_awb;?></td>
        <td><?=$data->ic_updatedby;?></td>
        <td><?=$data->ic_updated;?></td>
        <td><?=$data->aoi_bm;?></td>
        <td><?=$data->aoi_ppn;?></td>
        <td><?=$data->aoi_pph;?></td>
        <td><?=$data->country;?></td>
      </tr>
    <?php }?>
  </tbody>
</table> 
