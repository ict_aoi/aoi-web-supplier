<?php    
    if(isset($_GET['from']) && $_GET['from'] != NULL){
    $dari   = date("Y-m-d 00:00:00",strtotime($_GET['from']));
    $this->db->where('create_date >=',$dari);
    }
    if(isset($_GET['until']) && $_GET['until'] != NULL){
      $sampai = date("Y-m-d 23:59:59",strtotime($_GET['until']));
      $this->db->where('create_date <=',$sampai);
    }
    if (isset($_GET['type_po']) && $_GET['type_po'] != NULL){
      $this->db->where('type_po',$_GET['type_po']);      
    }
             
  $data_all= $this->db->get('adt_unlocked_pl')->result();

  header("Content-Type:application/vnd.ms-excel");
  header('Content-Disposition:attachment; filename="Report Unlocked Packing List.xls"');
?>

<table border="1px">
  <thead>
    <th>No Packing List</th>
    <th>Invoice</th>
    <th>Resi</th>
    <th>Supplier</th>
    <th>Documentno</th>
    <th>Status PO</th>
    <th>Item</th>
    <th>Qty Ordered</th>
    <th>Qty Entered</th>
    <th>Qty Uploaded</th>
    <th>FoC</th>
    <th>UoM</th>
    <th>Product Category</th>
    <th>Created Date</th>
    <th>Created By</th>
  </thead>
  <tbody>
    <?php
      $nomor=1;
      foreach($data_all as $po){ 
    ?>
      <tr>        
      <td><?=$po->no_packinglist;?></td>
      <td><?=$po->kst_invoicevendor;?></td>
      <td><?=$po->kst_resi;?></td>
      <td><?=$po->supplier;?></td>
      <td><?=$po->documentno;?></td>
      <td><?=$po->status_po;?></td>
      <td><?=$po->item;?></td>
      <td><?=number_format($po->qtyordered,2);?></td>
      <td><?=number_format($po->qtyentered,2);?></td>
      <td><?=number_format($po->qty_upload,2);?></td>
      <td><?=number_format($po->foc,2);?></td>
      <td><?=$po->uomsymbol;?></td>
      <td><?=$po->category;?></td>
      <td style="mso-number-format:'Short Date'"><?=$po->create_date;?></td>
      <td><?=$po->createdby;?></td>
      </tr>
    <?php }?>
  </tbody>
</table>