        <div class="col-md-6">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Click this button to download the file!</h3>
                </div>
                    <div class="box-footer">
                            <br>
                            <a href="<?= base_url('admin/xls_dp_detail'); ?>" class="btn btn-block btn-success"><i class='fa fa-download'></i> DOWNLOAD</a>
                            <br>
                    </div>
            </div>
        </div>