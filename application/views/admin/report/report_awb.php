    <?php
  if(isset($_POST['from']) && $_POST['from'] != NULL){
    $dari   = date("Y-m-d 00:00:00",strtotime($_POST['from']));
    $this->db2->where('created >=',$dari);
    $from = $_POST['from'];
  }else{
    $from = '';
  }
  if(isset($_POST['until']) && $_POST['until'] != NULL){
    $sampai = date("Y-m-d 23:59:59",strtotime($_POST['until']));
    $this->db2->where('created <=',$sampai);
    $until = $_POST['until'];
  }else{
    $until = '';
  }
  if(isset($_POST['c_bpartner_id']) && $_POST['c_bpartner_id'] != NULL){
    $this->db->where('user_id',$_POST['c_bpartner_id']);
    $c_bpartner_id = $_POST['c_bpartner_id'];
  }else{
    $c_bpartner_id = '';
  }
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="http://code.jquery.com/jquery-migrate-1.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>

        
        <div class="col-md-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Choose the range of Create Date Packinglist!</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="" method="POST">
              <div class="box-body">
                <div class="form-group">
                  <label for="from" class="col-sm-3 control-label">Created From</label>

                  <div class="col-sm-7">
                    <input type="date" class="form-control" name="from" placeholder="Start Date">
                  </div>
                </div>
                <div class="form-group">
                  <label for="until" class="col-sm-3 control-label">Until</label>

                  <div class="col-sm-7">
                    <input type="date" class="form-control" name="until" placeholder="End Date">
                  </div>
                </div>
                <div class="form-group">
                  <label for="c_bpartner_id" class="col-sm-3 control-label">Supplier</label>

                  <div class="col-sm-7">
                    <select class="livesearch" name="c_bpartner_id">
                      <option value="">Choose Supplier Name</option>
                      <?php
                        $supplier = $this->db->get('m_user');
                        foreach ($supplier->result() as $dt) {
                          echo "<option value='".$dt->user_id."'>".$dt->nama."</option>";
                        }
                      ?>
                    </select>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info btn-block"> SEARCH</button>
                <?php
                  if(isset($_POST['from']) && isset($_POST['until'])){
                    ?>
                        <br>
                        <a href="<?=base_url('admin/xls_pilih_awb?from='.$from.'&until='.$until.'&c_bpartner_id='.$c_bpartner_id);?>" class="btn btn-block btn-success"><i class='fa fa-download'></i> DOWNLOAD</a>
                        <br>
                    <?php
                  }
                ?>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
<script type="text/javascript">
  $(".livesearch").chosen();
</script>