<?php   
  $data_all= $this->db->get('adt_dp_detail')->result();

  header("Content-Type:application/vnd.ms-excel");
  header('Content-Disposition:attachment; filename="Report Date Promised Detail.xls"');
?>

<table border="1px">
  <thead>
      <th>C_Order_ID</th>
      <th>C_OrderLine_ID</th>
      <th>Date Required</th>
      <th>Date Promised</th>
      <th>Max Created Date</th>
      <th>Lock Status</th>
      <th>Flag ERP</th>
      <th>C_BPartner_ID</th>
      <th>Confirm Date</th>
      <th>PO Number</th>
      <th>Business Partner</th>
      <th>Item</th>
      <th>Product Category</th>
      <th>PO Created By</th>
  </thead>
  <tbody>
    <?php
      $nomor=1;
      foreach($data_all as $po){ 
    ?>
      <tr>        
      <td><?=$po->c_order_id;?></td>
      <td><?=$po->c_orderline_id;?></td>
      <td><?=$po->date_required;?></td>
      <td><?=$po->date_promised;?></td>
      <td><?=date('Y-m-d',strtotime($po->max_created_date));?></td>
      <td><?=$po->lock_status;?></td>
      <td><?=$po->flag_erp;?></td>
      <td><?=$po->c_bpartner_id;?></td>
      <td><?=$po->confirm_date;?></td>
      <td><?=$po->documentno;?></td>
      <td><?=$po->supplier;?></td>
      <td><?=$po->item;?></td>
      <td><?=$po->category;?></td>
      <td><?=$po->createdby;?></td>
      </tr>
    <?php }?>
  </tbody>
</table>