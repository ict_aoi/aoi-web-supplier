<?php
  if(isset($_POST['from']) && $_POST['from'] != NULL){
    $dari   = date("Y-m-d 00:00:00",strtotime($_POST['from']));
    $this->db2->where('issue_date >=',$dari);
    $from = $_POST['from'];
  }else{
    $from = '';
  }
  if(isset($_POST['until']) && $_POST['until'] != NULL){
    $sampai = date("Y-m-d 23:59:59",strtotime($_POST['until']));
    $this->db2->where('issue_date <=',$sampai);
    $until = $_POST['until'];
  }else{
    $until = '';
  }
  if(isset($_POST['type_po']) && $_POST['type_po'] != NULL){
    $type_po = $_POST['type_po'];
    $this->db2->where('type_po',$type_po);
    $type_po = $_POST['type_po'];
  }else{
    $type_po = '';
  }
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="http://code.jquery.com/jquery-migrate-1.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>

        
        <div class="col-md-6">
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Choose Range of Created Date Packinglist!</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="" method="POST">
              <div class="box-body">
                <div class="form-group">
                  <label for="from" class="col-sm-3 control-label">Created From</label>

                  <div class="col-sm-7">
                    <input type="date" class="form-control" name="from" placeholder="Start Date">
                  </div>
                </div>
                <div class="form-group">
                  <label for="until" class="col-sm-3 control-label">Until</label>

                  <div class="col-sm-7">
                    <input type="date" class="form-control" name="until" placeholder="End Date">
                  </div>
                </div>
                <div class="form-group">
                  <label for="type_po" class="col-sm-3 control-label">Warehouse</label>

                  <div class="col-sm-7">
                    <select class="livesearch" name="type_po">
                      <option value="">Choose Warehouse</option>
                      <?php
                              $this->db->distinct();
                              $this->db->select('type_po');
                        $wh = $this->db->get('po_detail');
                        foreach ($wh->result() as $dt) {
                          echo "<option value='".$dt->type_po."'>".$dt->type_po."</option>";
                        }
                      ?>
                    </select>
                  </div>
                </div>
                <div class="col-sm-12">
                  <label><code> Choose 1 for Warehouse Fabric, 2 for Warehouse Accessories</code></label>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-success btn-block"> SEARCH</button>
                <?php
                  if(isset($_POST['from']) && isset($_POST['until'])){
                    ?>
                        <br>
                        <a href="<?=base_url('admin/xls_pilih_unlocked_pl?from='.$from.'&until='.$until.'&type_po='.$type_po);?>" class="btn btn-block btn-warning"><i class='fa fa-download'></i> DOWNLOAD</a>
                        <br>
                    <?php
                  }
                ?>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
<script type="text/javascript">
  $(".livesearch").chosen();
</script>