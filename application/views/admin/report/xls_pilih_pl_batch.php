<?php
  if(isset($_GET['from']) && $_GET['from'] != NULL){
    $dari   = date("Y-m-d 00:00:00",strtotime($_GET['from']));
    $this->db->where('create_date >=',$dari);
    }
    if(isset($_GET['until']) && $_GET['until'] != NULL){
      $sampai = date("Y-m-d 23:59:59",strtotime($_GET['until']));
      $this->db->where('create_date <=',$sampai);
    }

    $data_all= $this->db->get('adt_mrc_batch');
        
  header("Content-Type:application/vnd.ms-excel");
  header('Content-Disposition:attachment; filename="Report Packinglist Batch.xls"');
?>

<table border="1px">
  <thead>
    <th>No Packinglist</th>
    <th>Invoice</th>
    <th>Resi</th>
    <th>Documentno</th>
    <th>Supplier</th>
    <th>Item</th>
    <th>Desc</th>
    <th>Batch Number</th>
    <th>ETA</th>
    <th>Created</th>
    <th>Status ERP</th>
    <th>Status WMS</th>
    <th>Received Date</th>
  </thead>
  <tbody>
    <?php foreach($data_all->result() as $data){ ?>
      <tr>
        <td><?=$data->no_packinglist;?></td>
        <td><?=$data->kst_invoicevendor;?></td>
        <td><?=$data->kst_resi;?></td>
        <td><?=$data->documentno;?></td>
        <td><?=$data->supplier;?></td>
        <td><?=$data->item;?></td>
        <td><?=$data->desc_product;?></td>
        <td style="mso-number-format:'\@'"><?=$data->batch_number;?></td>
        <td style="mso-number-format:'Short Date'"><?=$data->kst_etadate;?></td>
        <td style="mso-number-format:'Short Date'"><?=$data->create_date;?></td>
        <td><?=$data->status_erp;?></td>
        <td><?=$data->status_wms;?></td> 
        <td><?=$data->received_date;?></td> 
      </tr>
    <?php }?>
  </tbody>
</table> 