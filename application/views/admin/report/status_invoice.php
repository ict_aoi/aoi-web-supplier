<?php
if (isset($_POST['from']) && $_POST['from'] != NULL) {
  $dari   = date("Y-m-d 00:00:00", strtotime($_POST['from']));
  $this->db->where('created_date >=', $dari);
  $from = $_POST['from'];
} else {
  $from = '';
}
if (isset($_POST['until']) && $_POST['until'] != NULL) {
  $sampai = date("Y-m-d 23:59:59", strtotime($_POST['until']));
  $this->db->where('created_date <=', $sampai);
  $until = $_POST['until'];
} else {
  $until = '';
}
if (isset($_POST['no_invoice']) && $_POST['no_invoice'] != NULL) {
  $no_invoice = $_POST['no_invoice'];
  // $this->db2->where('no_invoice',$no_invoice);
  $no_invoice = $_POST['no_invoice'];
} else {
  $no_invoice = '';
}
?>

<div class="col-md-6">
  <div class="box box-info">
    <div class="box-header with-border">
      <h3 class="box-title">Choose the range of Created Invoice Date!</h3>
    </div>
    <!-- /.box-header -->
    <!-- form start -->
    <form class="form-horizontal" action="" method="POST">
      <div class="box-body">
        <div class="form-group">
          <label for="from" class="col-sm-3 control-label">Created From</label>

          <div class="col-sm-7">
            <input type="date" class="form-control" name="from" placeholder="Start Date">
          </div>
        </div>
        <div class="form-group">
          <label for="until" class="col-sm-3 control-label">Until</label>

          <div class="col-sm-7">
            <input type="date" class="form-control" name="until" placeholder="End Date">
          </div>
        </div>
        <div class="form-group">
          <label for="no_invo" class="col-sm-3 control-label">No Invoice</label>

          <div class="col-sm-7">
            <input type="text" class="form-control" name="no_invoice" placeholder="No Invoice">
          </div>
        </div>
      </div>
      <!-- /.box-body -->
      <div class="box-footer">
        <button type="submit" class="btn btn-info btn-block"> SEARCH</button>
        <?php
        if (!empty($_POST['from']) && !empty($_POST['until']) && empty($_POST['no_invoice'])) {
          ?>
          <div class="col-sm-12">
            <code>You're Searching Status Invoice from <?= $from; ?> until <?= $until; ?> </code>
          </div>
          <br><br>
          <a href="<?= base_url('admin/xls_status_invoice?from=' . $from . '&until=' . $until . '&no_invoice=' . $no_invoice); ?>" class="btn btn-block btn-success"><i class='fa fa-download'></i> DOWNLOAD</a>
        <?php
        } elseif (!empty($_POST['from']) && !empty($_POST['until']) && !empty($_POST['no_invoice'])) {
          ?>
          <div class="col-sm-12">
            <code>You're Searching Status Invoice from <?= $from; ?> until <?= $until; ?> with Invoice No <?= $no_invoice; ?></code>
          </div>
          <br><br>
          <a href="<?= base_url('admin/xls_status_invoice?from=' . $from . '&until=' . $until . '&no_invoice=' . $no_invoice); ?>" class="btn btn-block btn-success"><i class='fa fa-download'></i> DOWNLOAD</a>
        <?php
        } elseif (empty($_POST['from']) && empty($_POST['until']) && !empty($_POST['no_invoice'])) {
          ?>
          <div class="col-sm-12">
            <code>You're Searching Status Invoice for Invoice <?= $no_invoice; ?></code>
          </div>
          <br><br>
          <a href="<?= base_url('admin/xls_status_invoice?from=' . $from . '&until=' . $until . '&no_invoice=' . $no_invoice); ?>" class="btn btn-block btn-success"><i class='fa fa-download'></i> DOWNLOAD</a>
        <?php
        }
        ?>
      </div>
      <!-- /.box-footer -->
    </form>
  </div>
</div>