<?php
  if(isset($_POST['from']) && $_POST['from'] != NULL){
    $dari   = date("Y-m-d 00:00:00",strtotime($_POST['from']));
    $this->db->where('issue_date >=',$dari);
    $from = $_POST['from'];
  }else{
    $from = '';
  }
  if(isset($_POST['until']) && $_POST['until'] != NULL){
    $sampai = date("Y-m-d 23:59:59",strtotime($_POST['until']));
    $this->db->where('issue_date <=',$sampai);
    $until = $_POST['until'];
  }else{
    $until = '';
  }
?>
        <div class="col-md-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Choose the range of Issued Date!</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="" method="POST">
              <div class="box-body">
                <div class="form-group">
                  <label for="from" class="col-sm-3 control-label">Issued From</label>

                  <div class="col-sm-7">
                    <input type="date" class="form-control" name="from" placeholder="Start Date">
                  </div>
                </div>
                <div class="form-group">
                  <label for="until" class="col-sm-3 control-label">Until</label>

                  <div class="col-sm-7">
                    <input type="date" class="form-control" name="until" placeholder="End Date">
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info btn-block"> SEARCH</button>
                <?php
                  if(isset($_POST['from']) && isset($_POST['until'])){
                    ?>
                        <br>
                        <a href="<?=base_url('admin/xls_report_bc?from='.$from.'&until='.$until);?>" class="btn btn-block btn-success"><i class='fa fa-download'></i> DOWNLOAD</a>
                        <br>
                    <?php
                  }
                ?>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>