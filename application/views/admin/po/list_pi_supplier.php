<?php
$this->db->where('c_bpartner_id',$this->session->userdata('user_id'))->update('m_pi_file',array('read'=>'t'));
$nomor=1;
$this->db->order_by('create_date','DESC');
$this->db->join('m_user','m_user.user_id = m_pi_file.c_bpartner_id');
$rev = $this->db->where('c_order_id',$_GET['id'])->get('m_pi_file');

?>
<table class="table table-bordered small">
	<thead class="bg-green">
		<tr>
			<th class="text-center" style="width: 20px">NO</th>
			<th>REV TIME</th>
			<th>UPLOAD BY</th>
			<th>FILE NAME</th>
			<th class="text-center"><i class='fa fa-download'></i></th>
		</tr>
	</thead>
	<tbody>
	<?php foreach($rev->result() as $rev){ ?>
		
		<tr>
			<td class="text-center"><?=$nomor++;?></td>
			<td><?=date("d-m-Y H:i:s",strtotime($rev->create_date));?></td>
			<td><?=$rev->nama;?></td>
			<td><?=str_replace(explode('-',$rev->file_upload_supplier)[0].'-','',$rev->file_upload_supplier);?></td>
			<td class="text-center">
				<a href='<?=base_url('admin/download_pi_supplier?token='.$this->session->userdata("session_id").'&id='.$rev->m_pi_file_id);?>'><i class='fa fa-download'></i></a>
			</td>
		</tr>
	<?php } ?>
	</tbody>
</table>