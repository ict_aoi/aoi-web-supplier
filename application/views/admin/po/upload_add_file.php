<?php
$this->db2->where('c_bpartner_id',$_GET['id']);
// $po = $this->db2->get('f_web_po_header');
$po = $this->db2->get('adt_web_po_header_status');
$nomor = 1;
?>

<input type="hidden" id="c_bpartner_id" value="<?php echo $_GET['id'] ?>">
<table class="table table-bordered table-hovered" id="detail">
	<thead class="bg-blue">
		<tr>
			<th>#</th>
			<th>PO NAME</th>
			<th>DOC STATUS</th>
			<th class="text-center">STATUS</th>
			<th class="text-center">REVISION STATUS</th>
			<th class="text-center">UPLOAD</th>
			<th class="text-center"	width="100px">PI AOI</th>
			<th class="text-center" width="100px">SET ETA& ETD</th>
			<th class="text-center">ETA</th>
			<th class="text-center">ETD</th>
			<th class="text-center">SHOW/HIDE</th>
			<th class="text-center">LAST CONFIRMED</th>
		</tr>
	</thead>
	<tbody>
	</tbody>
</table>
<div class="set_eta hidden">
	<form class="set_eta"  method="POST">
		<label>Set ETA</label>
		<input type="date" name="eta" class="form-control" id="datepicker" value="<?=date('Y-m-d');?>">
		<label>Set ETD</label>
		<input type="date" name="etd" class="form-control" id="datepicker2" value="<?=date('Y-m-d');?>">
		<label></label>
		<button class="btn btn-flat btn-block btn-primary">Simpan</button>
	</form>
</div>
<div class="upload_formpi hidden">
	<form class="upload_pi" enctype="multipart/form-data" method="POST">
		<label>Filetest</label>
		<input type="file" name="po" class="form-control">
		<label></label>
		<button class="btn btn-flat btn-block btn-primary submit">Upload</button>
	</form>
	<br>
	<div id="draw_table"></div>
</div>
<script type="text/javascript">
	$('#datepicker').datepicker({
		format: "yyyy-mm-dd"
	});

	$('#datepicker2').datepicker({
		format: "yyyy-mm-dd"
	});
	var table = $("#detail").DataTable({
		  processing  : true,
		  serverSide  : true,
		  order: [],
		  orderMulti  : true,
          ajax: {
			url: '<?php echo base_url('admin/upload_add_file')?>',
			type: 'POST',
			datatype: 'JSON',
			data: {
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
			'c_bpartner_id':$('#c_bpartner_id').val(),

			}
		  },
		  columns: [
		  	  {"data" : null, 'sortable' : false},
			  {'data' : 'documentno', "className": "text-center"},
			  {'data' : 'docstatus', "className": "text-center", 'orderable' : false},
			  {'data' : 'status', "className": "text-center", 'orderable' : false},
			  {'data' : 'revisi', "className": "text-center", 'orderable' : false},
			  {'data' : 'upload', "className": "text-center", 'orderable' : false},
			  {'data' : 'pi', "className": "text-center", 'orderable' : false},
			  {'data' : 'seteta', "className": "text-center", 'orderable' : false},
			  {'data' : 'date_eta', "className": "text-center", 'orderable' : false},
			  {'data' : 'date_etd', "className": "text-center", 'orderable' : false},
			  {'data' : 'show', "className": "text-center", 'orderable' : false},
			  {'data' : 'date_last', "className": "text-center", 'orderable' : false},
		  ],
		  language: {
			  processing: "L O A D I N G  D A T A...<div id='process'></div>",
			  searchPlaceholder: "Insert PO Supplier"
		  },
		  fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
		  ,"fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ) 
            {
            	if(aData.docstatus == "DR"){
            		  $(nRow).css('background-color', '#f9ad2a');
            		  $(nRow).css('color', 'white');
            	}
            }
      });
	function show(id,st){
		if(st == 1){
			var x = confirm('Apakah anda yakin akan menampilkan Purchase Order ini untuk supplier?');
			var y = '<?=base_url('admin/show_po');?>';
		}else{
			var x = confirm('Apakah anda yakin akan mennyembunyikan Purchase Order dari untuk supplier?');
			var y = '<?=base_url('admin/hide_po');?>';
		}
		if(x == true){
			$.ajax({
				url:y,
				data:'c_order_id='+id,
				type:'POST',
				success:function(data){
					if(data != 0){
						$('#detail').DataTable().ajax.reload();
					}else{
						alert('Gagal!');
					}
				}
			})
		}
		return false;
	}
	
	function set_eta(id,name,user_id){
		var modal = $('#myModal_ > div > div');
		$('#myModal_').modal('show');
		modal.children('.modal-header').children('.modal-title').html(name);
		modal.parent('.modal-dialog').addClass('modal-md');
		modal.children('.modal-body').html($('.set_eta').html());
		$('.set_eta').submit(function(){
			var data = new FormData(this);
			$.ajax({
				url:'<?=base_url('admin/set_eta?c_order_id=');?>'+id,
				type: "POST",
				data: data,
				contentType: false,       
				cache: false,          
				processData:false, 
				success: function(response){
					if(response == 1){
						$('#myModal_').modal('hide');
						$('#detail').DataTable().ajax.reload();
					}
				}
			})
			return false;
		})
		return false;
	}
	function upload_pi(id,name){
		$.ajax({
			url:'<?=base_url('admin/listpi_file?token='.$this->session->userdata("session_id").'&id=');?>'+id,
			type:'POST',
			success:function(data){

			}
		})
		.done(function(response){
			var modal = $('#myModal_ > div > div');
			$('#myModal_').modal('show');
			modal.children('.modal-header').children('.modal-title').html(name);
			modal.parent('.modal-dialog').addClass('modal-md');
			modal.children('.modal-body').html($('.upload_formpi').html());
			$('.modal-body #draw_table').html(response);
			$('.upload_pi').submit(function(){
				var data = new FormData(this);
				$('.submit').attr('disabled',true);
				$.ajax({
					url:'<?=base_url('admin/upload_pi?token='.$this->session->userdata("session_id").'&c_order_id=');?>'+id,
					type: "POST",
					data: data,
					contentType: false,       
					cache: false,          
					processData:false, 
					success: function(response){
						if(response == 1){
							$('.submit').attr('disabled',false);
							$('#myModal_').modal('hide');
							swal("Upload Success!", {
							      icon: "success",
							      timer: 1600,
				              	  buttons: false,
							    });
						}else{
							$('.submit').attr('disabled',false);
							$('#myModal_').modal('hide');
							swal("Failed!", {
							      icon: "warning",
							      timer: 1600,
				              	  buttons: false,
							    });
						}
					}
				});
				return false;
			});
			return false;
		});
		
	}
	function download_pi(id){
		var modal = $('#myModal_ > div > div');
		$('#myModal_').modal('show');
		modal.children('.modal-title').text('All file attachment');
		modal.parent('.modal-dialog').addClass('modal-md');
		modal.children('.modal-body').load('<?=base_url('admin/listpi_supplier?token='.$this->session->userdata("session_id").'&id=');?>'+id);		
	}
	
</script>