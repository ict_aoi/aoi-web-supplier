<style type="text/css">
 .sorting, .sorting_asc, .sorting_desc {
    background : none;
}
.modal-xxl {
     width: 1200px;
    }
</style>
<div class="row">
	<div class="col-sm-12">
		<table class="table table-striped table-bordered" id="tables2">
			<thead class="bg-green">
				<tr>
					<th class="text-center" style="width: 30px">#</th>
					<th>Nama</th>
					<th class="text-center">PO DETAIL</th>
					<th class="text-center">OPTION</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>
	</div>
</div>
<div class="upload_form hidden">
<form class="upload_po" enctype="multipart/form-data" method="POST">
	<label>File</label>
	<input type="file" name="po" class="form-control">
	<label></label>
	<button class="btn btn-flat btn-block btn-primary">Upload</button>
</form>
</div>
<script type="text/javascript">
	var table = $("#tables2").DataTable({
		  processing  : true,
		  serverSide  : true,
		  order: [],
		  orderMulti  : true,
          ajax: {
			url: '<?php echo base_url('admin/list_po')?>',
			type: 'POST',
			datatype: 'JSON',
			data: {
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>'
			}
		  },
		  columns: [
			  {'data' : null, 'sortable' : false},
			  {'data' : 'nama'},
			  {'data' : 'detail', "className": "text-center", 'orderable' : false},
			  {'data' : 'option', "className": "text-center", 'orderable' : false}
		  ],
		  language: {
			  processing: "L O A D I N G...<div id='process'></div>",
			  // searchPlaceholder: "Insert Supplier Name"
			  searchPlaceholder: "Minimal 2 characters"
		  },
		  fnCreatedRow: function (row, data, index) {
		  	var info = table.page.info();
		  	var value = index+1+info.start;
		  	$('td', row).eq(0).html(value);
		  }
      });

	$('#tables2_filter input').unbind();
    $('#tables2_filter input').bind('keyup', function(e) {
        // if (e.keyCode == 13) {
        //     table.search($(this).val()).draw();
        // }
        if ($(this).val().length == 0 || $(this).val().length >= 2) {
            table.search($(this).val()).draw();
        }
    });

	function delete_file(id){
		alert(id);
		return false;
	}
	function detail(id,name){
		var modal = $('#myModal > div > div');
		$('#myModal').modal('show');
		modal.children('.modal-header').children('.modal-title').html('Purchase Order <b>'+name+'</b>');
		modal.parent('.modal-dialog').addClass('modal-lg');
		modal.children('.modal-body').html('<center>Loading..</center>');
		modal.children('.modal-body').load('<?=base_url('admin/po_detail?id=');?>'+id);
		return false;
	}
	function upload_file(id,name,user_id){
		var modal = $('#myModal_ > div > div');
		$('#myModal_').modal('show');
		modal.children('.modal-header').children('.modal-title').html(name);
		modal.parent('.modal-dialog').addClass('modal-sm');
		modal.children('.modal-body').html('<center>Loading..</center>');
		modal.children('.modal-body').html($('.upload_form').html());
		$('.upload_po').submit(function(){
			var data = new FormData(this);
			$.ajax({
				url:'<?=base_url('admin/po_upload_file_submit?c_order_id=');?>'+id,
				type: "POST",
				data: data,
				contentType: false,
				cache: false,
				processData:false,
				success: function(response){
					if(response == 1){
						$('#myModal_').modal('hide');
						$('#detail').DataTable().ajax.reload();
					}
				}
			})
			return false;
		})
		return false;
	}
	function upload(id,name){
		var modal = $('#myModal > div > div');
		$('#myModal').modal('show');
		modal.children('.modal-header').children('.modal-title').html('Upload PO <b>'+name+'</b>');
		modal.parent('.modal-dialog').addClass('modal-lg');
		modal.children('.modal-body').html('<center>Loading..</center>');
		modal.children('.modal-body').load('<?=base_url('admin/po_upload?id=');?>'+id);
		return false;
	}
	function upload__(id,name){
		var modal = $('#myModal > div > div');
		$('#myModal').modal('show');
		modal.children('.modal-header').children('.modal-title').html('ADDITIONAL FILE FOR <b>'+name+'</b>');
		modal.parent('.modal-dialog').addClass('modal-xxl');
		modal.children('.modal-body').load('<?=base_url('admin/po_add_upload?id=');?>'+id);
		return false;
	}
	function revisi(id,name){
		var modal = $('#myModal_ > div > div');
		$('#myModal_').modal('show');
		modal.children('.modal-header').children('.modal-title').html('REVISION STATUS : '+name);
		modal.parent('.modal-dialog').removeClass('modal-sm');
		modal.children('.modal-body').html('<center>Loading..</center>');
		modal.children('.modal-body').load("<?=base_url('admin/po_upload_revision?id=');?>"+id);
		return false;
	}
	function revisi_file(id,name){
		var modal = $('#myModal_ > div > div');
		$('#myModal_').modal('show');
		modal.children('.modal-header').children('.modal-title').html('REVISION STATUS : '+name);
		modal.parent('.modal-dialog').removeClass('modal-sm');
		modal.children('.modal-body').html('<center>Loading..</center>');
		modal.children('.modal-body').load("<?=base_url('admin/file_upload_revision?id=');?>"+id);
		return false;
	}

</script>