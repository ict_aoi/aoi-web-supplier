<?php
    if (json_decode(base64_decode($_GET['type_po']))==1) {
        $this->db2->where('c_order_id',json_decode(base64_decode($_GET['order'])));
        $data_all= $this->db2->get('rma_reportt1t2fabric_v')->result();
    }else{
         $this->db2->where('c_order_id',json_decode(base64_decode($_GET['order'])));
        $data_all= $this->db2->get('rma_reportt1t2v2acc')->result();
    }   

  header("Content-Type:application/vnd.ms-excel");
  header('Content-Disposition:attachment; filename="Report T1 & T2 '.date('d-m-Y').'.xls"');
?>

<table border="1px">
    <tr></tr>
    <tr></tr>
  <thead>
    <th>Issue Date:</th>
    <th>Order Number:</th>
    <th>Latest Update (GMT):</th>
    <th>Version:</th>
    <th>Purpose:</th>
    <th>Buyer (T1 Supplier Name)</th>
    <th>T1 address line 1</th>
    <th>T1 address line 2</th>
    <th>T1 address line 3</th>
    <th>T1 address line 4</th>
    <th>T1 address city</th>
    <th>T1 address province/ state</th>
    <th>T1 address postal code</th>
    <th>T1 address Country (refer to Pick List_Country)</th>
    <th>Contact Person (Buyer)</th>
    <th>Contact No. (Buyer)</th>
    <th>Email (Buyer)</th>
    <th>Ship To (T1 Factory Name)</th>
    <th>T1 address line 1</th>
    <th>T1 address line 2</th>
    <th>T1 address line 3</th>
    <th>T1 address line 4</th>
    <th>T1 address city</th>
    <th>T1 address province/ state</th>
    <th>T1 address postal code</th>
    <th>T1 address Country (refer to Pick List_Country)</th>
    <th>T1 6 digit adidas Factory Code</th>
    <th>Seller (T2 Supplier Name)</th>
    <th>Actual Manufacturer (T2 Factory Name)</th>
    <th>T2 address line 1</th>
    <th>T2 address line 2</th>
    <th>T2 address line 3</th>
    <th>T2 address line 4</th>
    <th>T2 address city</th>
    <th>T2 address province/ state</th>
    <th>T2 address postal code</th>
    <th>T2 address Country (refer to Pick List_Country)</th>
    <th>T2 6 digit adidas Factory Code</th>
    <th>T1 Customer</th>
    <th>PO Download Date</th>
    <th>Currency</th>
    <th>Payment Terms</th>
    <th>Incoterm</th>
    <th>Ship Mode</th>
    <th>Country of Origin</th>
    <th>Forwarder</th>
    <th>Remarks</th>
    <th>Packing Instruction:</th>
    <th>Shipping Instruction:</th>
    <th>Line Status</th>
    <th>Line #</th>
    <th>Ref#</th>
    <th>Description / Supplier Material Name</th>
    <th>Width</th>
    <th>UOM (for width)</th>
    <th>Weight</th>
    <th>UOM (for weight)</th>
    <th>Length</th>
    <th>UOM (for length)</th>
    <th>Height</th>
    <th>UOM (for Height)</th>
    <th>Thickness</th>
    <th>UOM (for thickness)</th>
    <th>Size</th>
    <th>Material Color</th>
    <th>Unit Price</th>
    <th>Order Quantity</th>
    <th>UOM (for order quantity)</th>
    <th>Color Matching</th>
    <th>Section Name</th>
    <th>Sustainable material</th>
    <th>Buyer Request Date</th>
    <th>adidas CRD</th>
    <th>adidas Plan Date</th>
    <th>Seller Confirm Delivery Date</th>
    <th>Seller Updated Delivery Date</th>
    <th>Confirmed Delivery Quantity</th>
    <th>Updated Delivery Quantity</th>
    <th>Seller Confirm Delivery Date  (Last Shipment)</th>
    <th>Seller Updated Delivery Date  (Last Shipment)</th>
    <th>Confirmed Delivery Quantity  (Last Shipment)</th>
    <th>Updated Delivery Quantity (Last Shipment)</th>
    <th>Season</th>
    <th>Priority & Order Type</th>
    <th>adidas Order Number</th>
    <th>adidas Article Number</th>
    <th>adidas Working Number / Model Name</th>
    <th>Remarks</th>
    <th>Additional Optional 1</th>
    <th>Additional Optional 2</th>
    <th>Additional Optional 3</th>
    <th>Additional Optional 4</th>
    <th>Additional Optional 5</th>
</thead>
  <tbody>
    <?php foreach($data_all as $data){ ?>
      <tr>
        <td style="mso-number-format:'Short Date'"><?=$data->issue_date2;?></td>
        <td><?=$data->documentno;?></td>
        <td><?=$data->latest_update;?></td>
        <td><?=$data->version;?></td>
        <td><?=$data->purpose;?></td>
        <td><?=$data->buyer;?></td>
        <td><?=$data->t1_address1buy;?></td>
        <td><?=$data->t1_address2buy;?></td>
        <td><?=$data->t1_address3buy;?></td>
        <td><?=$data->t1_address4buy;?></td>
        <td><?=$data->t1_addresscitybuy;?></td>
        <td><?=$data->t1_addressstatebuy;?></td>
        <td><?=$data->t1_addresspostalcodebuy;?></td>
        <td><?=$data->t1_addresscountrybuy;?></td>
        <td><?=$data->contact_personbuyer;?></td>
        <td style="mso-number-format:'\@'"><?=$data->contact_nobuyer;?></td>
        <td><?=$data->emailbuyer;?></td>
        <td><?=$data->ship_to;?></td>
        <td><?=$data->t1_address1;?></td>
        <td></td>
        <td></td>
        <td></td>
        <td><?=$data->t1_addresscity;?></td>
        <td><?=$data->t1_addressprovince;?></td>
        <td><?=$data->t1_addresspostal;?></td>
        <td><?=$data->t1_addresscountry;?></td>
        <td><?=$data->t1_6digit_factorycode;?></td>
        <td><?=$data->seller;?></td>
        <td><?=$data->actual_manufacturer;?></td>
        <td><?=$data->t2_address_line_1;?></td>
        <td><?=$data->t2_address_line_2;?></td>
        <td><?=$data->t2_address_line_3;?></td>
        <td><?=$data->t2_address_line_4;?></td>
        <td><?=$data->t2_address_city;?></td>
        <td></td>
        <td><?=$data->t2_address_postal;?></td>
        <td><?=$data->t2_address_country;?></td>
        <td style="mso-number-format:'\@'"><?=$data->name2;?></td>
        <td><?=$data->t1_customer;?></td>
        <td style="mso-number-format:'Short Date'"><?=$data->po_download_date;?></td>
        <td><?=$data->currency;?></td>
        <td><?=$data->payment_terms;?></td>
        <td><?=$data->incoterm2;?></td>
        <td><?=$data->ship_mode2;?></td>
        <td><?=$data->country_origin;?></td>
        <td><?=$data->forwarder;?></td>
        <td></td>
        <td></td>
        <td></td>
        <td><?=$data->line_status;?></td>
        <td><?=$data->line;?></td>
        <td><?=$data->ref;?></td>
        <td><?=$data->material_name;?></td>
        <td><?=$data->width;?></td>
        <td><?=$data->uom_width;?></td>
        <td></td>
        <td><?=$data->uom_weight;?></td>
        <td><?=$data->length;?></td>
        <td><?=$data->uom_length;?></td>
        <td><?=$data->height;?></td>
        <td><?=$data->uom_height;?></td>
        <td></td>
        <td></td>
        <td><?=$data->size;?></td>
        <td><?=$data->material_colour;?></td>
        <td><?=$data->unit_price;?></td>
        <td><?=$data->order_quantity;?></td>
        <td></td>
        <td><?=$data->color_matching;?></td>
        <td></td>
        <td></td>
        <td style="mso-number-format:'Short Date'"><?=$data->buyer_requestdate2;?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td><?=$data->season;?></td>
        <td><?=$data->ordertype;?></td>
        <td><?=$data->adidas_ordernumber;?></td>
        <td><?=$data->adidas_articlenumber;?></td>
        <td><?=$data->string_agg;?></td>
        <td></td>
        <td><?=$data->additional1;?></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>

      </tr>
    <?php }?>
  </tbody>
</table> 