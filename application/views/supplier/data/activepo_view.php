<style>
#process {
	position: fixed;
	width: 100%;
	height: 100%;
	top: 0px;
	left: 0px;
	background: transparent;
	z-index: 999;
}
</style>
<div class="row">
	<div class="col-md-12">
		<table class="table table-striped table-bordered table-responsive" id="tables2">
			<thead class="bg-green">
				<tr>
					<th style="width: 20px">#</th>
					<th>PO Number</th>
					<th class="text-center">Release Date</th>
					<th class="text-center">Category</th>
					<th class="text-center">Item</th>
					<th class="text-center">Detail</th>
					<th class="text-center">Purchase Order</th>
					<th class="text-center">Additional File</th>
					<th class="text-center">PI Supplier</th>
					<th class="text-center">Orderform</th>
					<th class="text-center">Report T1 T2</th>
					<th class="text-center">XML</th>
					<th class="text-center">Unconfirm</th>
					<th class="text-center">Remark Confirmation</th>
				</tr>
			</thead>
			<tbody>

			</tbody>
		</table>

	</div>
</div>
<div class="row">
	<div class="col-sm-2">
		 <a href="<?=base_url('data/activepo');?>" class="btn btn-block btn-danger"><i class='fa fa-arrow-left'></i> Back</a>
	</div>
</div>
<div class="upload_form hidden">
<form class="upload_pi" enctype="multipart/form-data" method="POST">
	<label>File</label>
	<input type="file" name="po" class="form-control">
	<label></label>
	<button class="btn btn-flat btn-block btn-primary submit" id="upload">Upload</button>
</form>
<br>
	<div id="draw_table"></div>
</div>
<script type="text/javascript">

	  var table = $("#tables2").DataTable({
		  processing  : true,
		  serverSide  : true,
		  order: [],
		  orderMulti  : true,
          ajax: {
			url: '<?php echo base_url('data/activepo_ajax')?>',
			type: 'POST',
			datatype: 'JSON',
			data: {
				'<?php echo $this->security->get_csrf_token_name(); ?>' : '<?php echo $this->security->get_csrf_hash(); ?>',
				dari: '<?php echo $dari;?>', sampai: '<?php echo $sampai;?>'
			}
		  },
		  columns: [
			  {"data" : null, 'sortable' : false},
			  {'data' : 'po_number'},
			  {'data' : 'release_date'},
			  {'data' : 'category', 'orderable' : false},
			  {'data' : 'item', "className": "text-center", 'orderable' : false},
			  {'data' : 'detail', "className": "text-center", 'orderable' : false},
			  {'data' : 'purchase_order', "className": "text-center", 'orderable' : false},
			  {'data' : 'additional_file', "className": "text-center", 'orderable' : false},
			  {'data' : 'pi_supplier', "className": "text-center", 'orderable' : false},
			  {'data' : 'orderform', "className": "text-center", 'orderable' : false},
			  {'data' : 'down_t1t2', "className": "text-center", 'orderable' : false},
			  {'data' : 'down_t1t2_xml', "className": "text-center", 'orderable' : false},
			  {'data' : 'unconfirm', "className": "text-center", 'orderable' : false},
			  {'data' : 'remark_confirmation', "className": "text-center", 'orderable' : false}
		  ],
		  language: {
			  processing: "L o a d i n g...<div id='process'></div>",
			  searchPlaceholder: "Masukkan PO Number"
		  },
		  fnCreatedRow: function (row, data, index) { var info = table.page.info(); var value = index+1+info.start; $('td', row).eq(0).html(value); }
      });

	function view_po(id){
		$('#myModal').modal('show');
		$('.modal-title').text('Preview Purchase Order');
		$('.modal-dialog').addClass('modal-lg');
		$('.modal-body').load('<?=base_url('data/po_view?id=');?>'+id);
		return false;
	};
	function unduh(id){
		$('#myModal').modal('show');
		$('.modal-title').text('All file attachment');
		$('.modal-dialog').removeClass('modal-lg');
		$('.modal-body').load('<?=base_url('data/list_file?id=');?>'+id);
		return false;
	}
	function upload_pi(id,name){
		$.ajax({
			url:'<?=base_url('data/listpi_file?id=');?>'+id,
			type:'POST',
			success:function(data){

			}
		})
		.done(function(response){

			var html = $('.upload_form').html();
			$('#myModal_').modal('show');
			$('.modal-title').html(name);
			$('.modal-body').html(html);
			$('.modal-body #draw_table').html(response);
			$('.upload_pi').submit(function(){
				$('.submit').attr('disabled',true);
				var data = new FormData(this);
				$.ajax({
					url:'<?=base_url('data/upload_pi?token='.$this->session->userdata("session_id").'&c_order_id=');?>'+id,
					type: "POST",
					data: data,
					contentType: false,
					cache: false,
					processData:false,
					success: function(response){
						if(response == 1){
							$('.submit').attr('disabled',false);
							$('#myModal_').modal('hide');
							swal("Upload Success!", {
							      icon: "success",
							      timer: 1600,
				              	  buttons: false,
							    });
						}else{
							$('.submit').attr('disabled',false);
							$('#myModal_').modal('hide');
							swal("Failed!", {
							      icon: "warning",
							      timer: 1600,
				              	  buttons: false,
							    });
					}
					}
				})
				return false;
			});
		});

	};
	function download_pi(id,name){
			$('#myModal').modal('show');
			$('.modal-title').text('All file attachment '+name);
			$('.modal-dialog').removeClass('modal-lg');
			$('.modal-body').load('<?=base_url('data/listpi_aoi?token='.$this->session->userdata("session_id").'&id=');?>'+id);
			return false;
	}
	function up_t1t2_xml(c_order_id){

		$.ajax({
			url: '<?php echo base_url('data/up_t1t2_xml')?>',
			type: 'POST',
			datatype: 'JSON',
			data: {c_order_id: c_order_id},
			beforeSend:function (){
	          progress_bar();
	        },
			success: function(response){
				swal(response,
					{
				      icon: "info",
				      timer: 2000,
	              	  buttons: false,
				    });

				dieYou_progress_bar();
			},
			error: function(jqXHR, textStatus, errorThrown) {
              swal("Failed!", (JSON.parse(jqXHR.responseText)).error.message, "error");
              console.log(jqXHR, textStatus, errorThrown);
            }
		});

	}

	function down_t1t2_xml(c_order_id){

		$.ajax({
			url: '<?php echo base_url('data/down_t1t2_xml')?>',
			type: 'POST',
			datatype: 'JSON',
			data: {c_order_id: c_order_id},
			beforeSend:function (){
	          progress_bar();
	        },
			success: function(response){
				swal(response,
					{
				      icon: "info",
				      timer: 2000,
	              	  buttons: false,
				    });

				dieYou_progress_bar();
			},
			error: function(jqXHR, textStatus, errorThrown) {
              swal("Failed!", (JSON.parse(jqXHR.responseText)).error.message, "error");
              console.log(jqXHR, textStatus, errorThrown);
            }
		});

	}
    </script>
