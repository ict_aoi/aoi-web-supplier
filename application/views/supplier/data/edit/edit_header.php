<?php
if($_GET['token'] == $this->session->userdata('session_id')){ ?>
<!DOCTYPE html>
<html>
<head>
	<title>EDIT DATA</title>
	<link rel="stylesheet" type="text/css" href="http://po.aoi.co.id/assets/bootstrap/css/bootstrap-datepicker.min.css">
    <script type="text/javascript" src="http://po.aoi.co.id/assets/bootstrap/js/bootstrap-datepicker.min.js"></script>
    <script type="text/javascript">
      $(function(){
          $('#datepicker').datepicker({
              format: "yyyy-mm-dd"
          });
      })
    </script>
    <script type="text/javascript">
      $(function(){
          $('#datepicker2').datepicker({
              format: "yyyy-mm-dd"
          });
      })
    </script>
</head>
<body>
	<form method="POST" action="<?=base_url('data/edit_header_submit');?>" id="header" class="form_daily_data">
		<input type="hidden" name="no_packinglist_before" value="<?=$_GET['no_packinglist'];?>">			
		<input type="hidden" name="invoice_before" value="<?=$_GET['kst_invoicevendor'];?>">	
		<input type="hidden" name="awb" value="<?=$_GET['awb'];?>">	
		<label>Packing List Number</label>
		<input type="text" value="<?=$_GET['no_packinglist'];?>" class='form-control' disabled>
		<!--label>Surat Jalan or DN</label-->
		<label>Invoice (INV)</label>
		<input type="text" class="form-control" name="kst_invoicevendor" value="<?=$_GET['kst_invoicevendor'];?>">
		<!--label>Resi Number (AWB)</label-->
		<?php
			$this->db->limit(1);
			$this->db->where('no_packinglist',$_GET['no_packinglist']);
			$this->db->where('kst_invoicevendor',$_GET['kst_invoicevendor']);
			$po = $this->db->get('po_detail');
			
		?>
		<label>Ex Factory Date</label>
		<input type="text" id="datepicker" name="kst_etddate" value="<?=$po->result()[0]->kst_etddate;?>" class="form-control" >
		<label>Estimated Time Arrival (ETA)</label>
		<input type="text" id="datepicker2" name="kst_etadate" value="<?php foreach($po->result() as $po){ if($po->kst_etadate==NULL){ echo ""; }else{ echo "$po->kst_etadate";
				} } ?>" class="form-control">
		<label></label>
		<button class="btn btn-block btn-primary btn-flat"> Update</button>
	</form>	
</body>
</html>
<?php }else{
	$this->load->view('user/login');
}
?>

