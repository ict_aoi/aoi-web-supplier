<!-- START CUSTOM TABS -->
<div class="row">
    <div class="col-md-6">
        <div class="upload_xml">
          <form enctype="multipart/form-data" onsubmit="return verify()" method="POST" class="" action='<?=base_url('xml_t2/upload_xml_t2');?>'>
              <label>File</label>
              <input type="file" name="file" id="inputFile" class="form-control">
              <label></label>
              <button class="btn btn-success btn-flat btn-block">Verify File</button>
          </form>
        </div>
    </div>
    <?php if (isset($success)) {
    ?>
        <div class="col-md-6">
            <div class="alert alert-warning fade in">
                <strong><?php echo $success; ?></strong>
            </div>
        </div>
    <?php
        }
    ?>

</div>
<!-- END CUSTOM TABS -->
<?php if (isset($data_xmls)) {

 ?>
<hr>
<div class="row">
    <div class="col-sm-12">
        <table class="table table-striped table-bordered" id="tabless">
            <thead class="bg-green">
                <tr>
                    <th>PO Purpose</th>
                    <th>PO Number</th>
                    <th>Currency</th>
                    <th>Sender Name</th>
                    <!-- <th>Buyer Name</th> -->
                    <th>Line Item</th>
                    <th>Adidas Order Number</th>
                    <th>Order Qty</th>
                </tr>
            </thead>
            <tbody>
                <?php
                    foreach ($data_xmls as $key => $value) {
                ?>
                <tr>
                    <td><?php echo $value['POPurpose'] ?></td>
                    <td><?php echo $value['PONumber'] ?></td>
                    <td><?php echo $value['currency'] ?></td>
                    <td><?php echo $value['senderName'] ?></td>
                    <!-- <td><?php echo $value['buyerName'] ?></td> -->
                    <td class="text-center"><?php echo $value['lineItemNumber'] ?></td>
                    <td><?php echo $value['adidasOrderNumber'] ?></td>
                    <td class="text-right"><?php echo $value['orderQty'] ?></td>
                </tr>
                <?php
                    }
                 ?>

            </tbody>
        </table>
    </div>
</div>

<?php } ?>

<script type="text/javascript">

function verify() {
    // validasi ekstensi file xml
    var inputFile = document.getElementById('inputFile');
    var pathFile = inputFile.value;
    var ekstensiOk = /(\.xml)$/i;

   if (!ekstensiOk.exec(pathFile)) {
    alert('File XML Not Found');
    return false;
   }

}



</script>
