<?php

  // print_r($_POST);
  // die();
  if(isset($_POST['from']) && $_POST['from'] != NULL){
    $dari   = date("Y-m-d 00:00:00",strtotime($_POST['from']));
    $this->db2->where('issue_date >=',$dari);
    $from = $_POST['from'];
  }else{
    $from = '';
  }
  if(isset($_POST['until']) && $_POST['until'] != NULL){
    $sampai = date("Y-m-d 23:59:59",strtotime($_POST['until']));
    $this->db2->where('issue_date <=',$sampai);
    $until = $_POST['until'];
  }else{
    $until = '';
  }
  if(isset($_POST['c_bpartner_id']) && $_POST['c_bpartner_id'] != NULL){
    $c_bpartner_id = $_POST['c_bpartner_id'];
    $this->db2->where('c_bpartner_id',$c_bpartner_id);
    $c_bpartner_id = $_POST['c_bpartner_id'];
  }else{
    $c_bpartner_id = '';
  }  
  if(isset($_POST['warehouse']) && $_POST['warehouse'] != NULL){
    $ware = $_POST['warehouse'];
    $this->db->where('m_warehouse_id',$ware);
    $warehouse = $_POST['warehouse'];
  }else{
    $warehouse = '';
  }
?>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<script src="http://code.jquery.com/jquery-migrate-1.1.0.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>

        
        <div class="col-md-6">
          <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Choose the range of Created Date Packing List!</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" action="" method="POST">
              <div class="box-body">
                <div class="form-group">
                  <label for="from" class="col-sm-3 control-label">Created From</label>

                  <div class="col-sm-7">
                    <input type="date" class="form-control" name="from" placeholder="Start Date" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="until" class="col-sm-3 control-label">Until</label>

                  <div class="col-sm-7">
                    <input type="date" class="form-control" name="until" placeholder="End Date" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="c_bpartner_id" class="col-sm-3 control-label">Supplier</label>

                  <div class="col-sm-7">
                    <select class="livesearch" name="c_bpartner_id">
                      <option value="">Choose Supplier Name</option>
                      <?php
                        $supplier = $this->db->get('m_user');
                        foreach ($supplier->result() as $dt) {
                          echo "<option value='".$dt->user_id."'>".$dt->nama."</option>";
                        }
                      ?>
                    </select>
                  </div>
                </div>
                
                <div class="form-group">
                  <label for="warehouse" class="col-sm-3 control-label">Warehouse</label>

                  <div class="col-sm-7">
                    <select class="form-control" name="warehouse" >
                      <option value="">Pilih Bagian</option>
                      <option value="1000001">Fabric AOI 1</option>
                      <option value="1000011">Fabric 2</option>
                      <option value="1000002">Accessories AOI 1</option>
                      <option value="1000013">Accessories AOI 2</option>
                    </select>
                    <!-- <input type="date" class="form-control" name="Factory" placeholder="End Date"> -->
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-info btn-block"> SEARCH</button>
                <?php
                  if(!empty($_POST['from']) && !empty($_POST['until'])){
                    ?>
                          <div class="col-sm-12">
                              <!--</?php
                                // if(!empty($_POST['c_bpartner_id'])){
                                //   $this->db->where('user_id',$c_bpartner_id);
                                // }
                                // $name_bp = $this->db->get('m_user')->row_array()['nama'];
                              ?> -->
                              
                              <!-- <code>You're Searching Packing List for <//$name_bp;?> from <//$from;?> until <//$until;?></code> -->

                              <?php
                                if(!empty($_POST['c_bpartner_id'])){
                                  $this->db->where('user_id',$c_bpartner_id);
                                  $name_bp = $this->db->get('m_user')->row_array()['nama'];
                              ?>
                                <code>You're Searching Packing List for <?=$name_bp;?> from <?=$from;?> until <?=$until;?></code>
                              <?php
                                }else{
                              ?>
                                <code>You're Searching Packing List for ALL SUPPLIER from <?=$from;?> until <?=$until;?></code>
                              <?php
                                }
                              ?>
                        </div>
                        <br><br>
                          <a href="<?=base_url('admin/xls_update_awb?from='.$from.'&until='.$until.'&warehouse='.$warehouse.'&c_bpartner_id='.$c_bpartner_id);?>" class="btn btn-block btn-success"><i class='fa fa-download'></i> DOWNLOAD</a>
                        <br>
                          <button type="button" class="btn btn-block btn-warning upload_awb"><i class="fa fa-upload"> UPLOAD</i></button>
                        <br>
                          <button type="button" class="btn btn-block btn-danger cara_upload"><i class="fa fa-clipboard"> CARA PENULISAN UPLOAD</i></button>
                        <br>
                    <?php
                  }
                ?>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
        </div>
        <div class="hidden upload_awb_pop">
          <form enctype="multipart/form-data" method="POST" class="upload_awb" action='<?=base_url('admin/edit_awb')?>' >
            <label>File</label>
            <input type="file" name="file" class="form-control">
            <code>Only Excel 97-2003 format can be uploaded!</code>
            <label></label>
            <button class="btn btn-success btn-flat btn-block submit">Verify File</button>
          </form>
          <div class="view_">

          </div>
        </div>

        <div class="hidden cara_upload_pop" >
          <table class="table table-striped table-bordered" id="table">
            <tr>
              <td>INPUTAN</td>
              <td>YANG DI INPUT DI EXCEL</td>
              <td>HASIL DI ERP</td>
            </tr>
            <tr>
              <td rowspan="2">FCL</td>
              <td>1</td>
              <td>FCL</td>
            </tr>
            <tr>
              <td>2</td>
              <td>LCL/CFS</td>
            </tr>
            <tr>
              <td rowspan="2">Port Of Discharge</td>
              <td>JKT</td>
              <td>SRG</td>
            </tr>
            <tr>
              <td>JAKARTA</td>
              <td>SEMARANG</td>
            </tr>
            <tr>
              <td rowspan="2">Kode Valuta</td>
              <td>100</td>
              <td>303</td>
            </tr>
            <tr>
              <td>USD</td>
              <td>IDR</td>
            </tr>

          </table>
        </div>

<script type="text/javascript">
  $(".livesearch").chosen();

  $(function(){
      $('.cara_upload').click(function(){
        var val = $('.cara_upload_pop').html();
        $('.view_').html(' ');
        $('#myModal').modal('show');
        $('.modal-dialog').addClass('modal-sm');
        $('.modal-title').text('Cara Upload WB');
        $('.modal-body').html(val);
      });
      $('.upload_awb').click(function(){
        var val = $('.upload_awb_pop').html();
        $('.view_').html(' ');
        $('#myModal').modal('show');
        $('.modal-dialog').addClass('modal-sm');
        $('.modal-title').text('Upload AWB');
        $('.modal-body').html(val);
        $('.upload_awb').submit(function(){
          var data = new FormData(this);
          $('.submit').attr('disabled',true);
          $.ajax({
            url:$(this).attr('action'),
            type: "POST",
            data: data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(response){
              $('.submit').attr('disabled',false);
              $('.modal-dialog').removeClass('modal-sm');
              $('.view_').html(response);
            }
            ,error:function(response){
                if(response.status==500){
                    $('.submit').attr('disabled',false);
                    swal({
                          title: "Warning!!",
                          text: "Upload Failed",
                          icon: "warning",
                          dangerMode: true,
                          timer: 1600,
                          buttons: false
                      });
                }
            }
          });
          return false;
        });
        
    })
  });
</script>