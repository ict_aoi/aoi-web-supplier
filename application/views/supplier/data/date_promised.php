<link rel="stylesheet" href="<?php echo base_url("assets/dist/daterangepicker.min.css");?>" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js" type="text/javascript"></script>
<script src="<?php echo base_url("assets/dist/jquery.daterangepicker.js");?>"></script>
<div class="col-sm-6">
	<div class="box">
		<div class="box-header">
			<h1>Choose Range Date </h1>			
		</div>
		<div class="box-body">
			<form action="<?=base_url('data/download_excel_range');?>" method="POST">
				<span id="two-inputs">
					<input class="form-control" id="date-range200" name="range1" style="width: 300px" value="" required> <br><input class="form-control" id="date-range201" name="range2" style="width: 300px" value="" required>
				</span>
			
		</div>
		<div class="box-header with border">
			<button class="btn btn-primary" type="submit"><i class='fa fa-download'></i>Download in Excel</button>
			</form>

			<a href="#" class="btn btn-warning btn-flat upload_dp"><i class='fa fa-upload'></i> Upload Date Promised</a>
		</div>
	</div>
</div>
<div class="hidden upload_dp_pop">
	<form enctype="multipart/form-data" method="POST" class="upload_dp_" action='<?=base_url('data/upload_dp_new');?>' >
		<label>File</label>
		<input type="file" name="file" class="form-control">
		<label></label>
		<button class="btn btn-success btn-flat btn-block">Verify File</button>
	</form>
	<div class="view_">
	</div>
</div>
<script type="text/javascript">
	// $('#daterange').daterangepicker();
	$(function(){
		$('#date-range200').datepicker({
		      format: "yyyy-mm-dd"
		  });
		$('#date-range201').datepicker({
		      format: "yyyy-mm-dd"
		  });
		if (!window['console'])
          {
            window.console = {};
            window.console.log = function(){};
          }
		$('#two-inputs').daterangepicker(
		{	
			separator : '  ',
			getValue: function()
			{
				if ($('#date-range200').val() && $('#date-range201').val() )
					return $('#date-range200').val() + ' to ' + $('#date-range201').val();
				else
					return '';
			},
			setValue: function(s,s1,s2)
			{
				$('#date-range200').val(s1);
				$('#date-range201').val(s2);
			}
		});		
	})
	$(function(){
		$('.upload_dp').click(function(){
			$('.view_').html('');
			var val = $('.upload_dp_pop').html();
			$('#myModal').modal('show');
			$('.modal-dialog').addClass('modal-lg');
			$('.modal-title').text('Upload Date Promised');
			$('.modal-body').html(val);
			$('.upload_dp_').submit(function(){
				var data = new FormData(this);
				$.ajax({
					url:$(this).attr('action'),
					type: "POST",
					data: data,
					contentType: false,       
					cache: false,          
					processData:false, 
					success: function(response){
						$('.modal-dialog').removeClass('modal-lg');
						$('.view_').html(response);
					}
				})
				return false;
			})
		})
	})
</script>