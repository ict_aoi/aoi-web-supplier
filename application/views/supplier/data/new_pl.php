<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">
<!-- <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script> not stable-->
<!-- <script src="http://code.jquery.com/jquery-migrate-1.1.0.js"></script> -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.jquery.min.js"></script>
<style type="text/css">
    #notifications {
    cursor: pointer;
    position: fixed;
    right: 0px;
    z-index: 9999;
    bottom: 0px;
    margin-bottom: 22px;
    margin-right: 15px;
    min-width: 300px; 
    max-width: 800px;  
    }
</style> 
<?php
if(count($detail) != 0){ ?>
	<div class="row">
        <div id="notifications"><?php echo $this->session->flashdata('lock');?></div>
	    <div class="col-md-4">
		    <form method="POST" action="" onsubmit="progress_bar();">
			    <label>Warehouse of Shipment</label>
				<select class="livesearch" name="m_warehouse_id">
				    <option value=""><strong>-- Select Warehouse --</strong></option>
				    <?php
					foreach ($warehouse as $dt) {
						echo "<option value='".$dt->m_warehouse_id."'>".$dt->name."</option>";
					}
				    ?>
			    </select>
				<input type="hidden" name="detail_list" value="<?php echo base64_encode(json_encode($detail));?>">
				<input type="hidden" name="warehouse_list" value="<?php echo base64_encode(json_encode($warehouse));?>">
			    <br><br>
			    <button class="btn btn-flat btn-primary btn-block">Search</button>
			    <br>
		    </form>
	    </div>
    </div>

    <?php
    if (isset($_POST['m_warehouse_id']) && $_POST['m_warehouse_id'] != NULL ) { ?>
        <style type="text/css">
        	input[type=number]::-webkit-inner-spin-button,
        	input[type=number]::-webkit-outer-spin-button {
        	    -webkit-appearance: none;
        	    -moz-appearance: none;
        	    appearance: none;
        	    margin: 0;
        	}
        	.inp{
        		width: 50px;
        		border: 0px;
        		background:inherit;
        		text-align: center;
        	}
        	.imp{
        		width: auto;
        	}
        	.actived{
        		border-bottom: 1px dotted #999;
        	}
        	.dataTables_filter{
        		margin-left: -100px;
        		border: 0px solid #000;
        	}
            
        </style>
        <div class="row">
	        <!-- <?php if($this->session->userdata('user_id') == 1001120){ ?>
	        <div class="col-sm-12">
		        <a href="#" class="btn pull-right btn-warning btn-flat upload_pl"><i class='fa fa-upload'></i> Upload Packing List</a>
	        </div>
	        <?php } ?> -->

            <!-- penambahan upload -->
    	        <div class="col-sm-12">
                    <input type="hidden" id="select_warehouse" value="<?php echo $select_warehouse ?>">
    	            <?php
    	            if($users==2){ ?>
    		            <a href='#' class="btn pull-right btn-flat btn-primary" onclick='download_template("<?=base_url('data/download_pl_acc?c_bpartner_id='.$this->session->userdata('user_id').'&m_warehouse_id='.$select_warehouse);?>")' id="dl_pl1"><i class='fa fa-excel-o'></i>Download Packing List</a>
    	            <?php
                    }else if($users==1){ ?>
    			        <a href='#' class="btn pull pull-right btn-flat btn-warning" onclick='download_template("<?=base_url('data/download_excel_pl_fbr?c_bpartner_id='.$this->session->userdata('user_id').'&m_warehouse_id='.$select_warehouse);?>")' id="dl_pl2"><i class='fa fa-excel-o'></i>Download Packing List</a>
    	            <?php
                    }

					//
					if($this->session->userdata('user_id') == 1007796){ ?>
						<a href="#" class="btn pull-right btn-flat btn-primary upload_pl_fbr" data-toggle="modal" data-backdrop="static" data-keyboard="false"><i class='fa fa-upload'></i> Upload Packing List Trax</a>
					<?php
					}
    		        elseif($detail_type_po==2 && $this->session->userdata('user_id') != 1001120){ ?>
    			        <a href="#" class="btn pull-right btn-warning btn-flat upload_placc" data-toggle="modal" data-backdrop="static" data-keyboard="false"><i class='fa fa-upload'></i> Upload Packing List</a>
    		        <?php
                    }elseif ($this->session->userdata('user_id') == 1001120) { ?>
    			        <a href="#" class="btn pull-right btn-success btn-flat upload_plykk" data-toggle="modal" data-backdrop="static" data-keyboard="false"><i class='fa fa-upload'></i> Upload Packing List</a>
    		        <?php
                    }else{ ?>
    			        <a href="#" class="btn pull-right btn-flat btn-primary upload_pl_fbr" data-toggle="modal" data-backdrop="static" data-keyboard="false"><i class='fa fa-upload'></i> Upload Packing List</a>
    		        <?php
                    } ?>
    	        </div>
	        <!-- end penambahan -->

            <form method="POST" action="new_pl_submit" enctype="multipart/form-data">
	            <div class="col-sm-3" style="overflow: hidden;">
		            <label>Packing List Number</label>
		            <input type="text" name="no_packinglist" class="form-control" placeholder="Packing List Name" required>
		            <!--label>Surat Jalan or DN</label-->
            		<input type="hidden" name="kst_suratjalanvendor" value ="-" class="form-control" placeholder="Surat Jalan or DN">
            		<label>Invoice <code style="font-size:10px">If you don't have Invoice Number, please input same as Packing List Number</code> </label>
            		<input type="text" name="kst_invoicevendor" class="form-control" value="" placeholder="Invoice" required>
            		<label>Resi Number or AWB</label>
            		<input type="text" name="kst_resi" class="form-control" value="-" placeholder="Resi Number or AWB" required>
            		<label>Ex Factory Date</label>
            		<input type="text" id="datepicker" name="kst_etddate" class="form-control" placeholder="" required>
            		<label>Estimated Time Arrival (ETA)</label>
            		<input type="text" id="datepicker2" name="kst_etadate" class="form-control" placeholder="" required>

                    <!-- SERVER SIDE LIST PURCHASE ORDER -->
            		<label>List of Purchase Order</label>
            		<table class="table table-striped table-bordered" id="tables123">
            		<thead class="bg-green">
            			<tr>
            				<th style="width: 30px" class="text-center">#</th>
            				<th>PO Number</th>
            				<th>Warehouse</th>
            			</tr>
            		</thead>
            		<tbody>

            		</tbody>
            		</table>
                    <!-- end of SERVER SIDE LIST PURCHASE ORDER -->
	            </div>

	            <div class="col-sm-9">
	                <label>Item list</label>
	                <input type="hidden" name="type_po" value="<?php echo $users;?>">
	                    <?php
                        if($users == 2){ ?>
                            <table class="table table-striped table-bordered">
                                <thead class="bg-green">
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>PO NUMBER</th>
                                        <th>ITEM</th>
                                        <th>PRODUCT CODE</th>
                                        <th>QTY ORDER</th>
                                        <th class="text-center">UOM</th>
                                        <th class="text-center">DP</th>
                                        <th class="text-center">QTY DLV</th>
                                        <th class="text-center">FOC</th>
                                        <th class="text-center">UNIT PRICE</th>
                                        <th class="text-center">QTY PACKAGE</th>
                                    </tr>
                                </thead>
                                <tbody class="view">

                                </tbody>
                            </table>
                        <?php
                        }else{ ?>
                            <table class="table table-striped table-bordered">
                                <thead class="bg-green">
                                    <tr>
                                        <th class="text-center">#</th>
                                        <th>PO NUMBER</th>
                                        <th>ITEM</th>
                                        <th>PRODUCT CODE</th>
                                        <th>QTY ORDER</th>
                                        <th class="text-center">UOM</th>
                                        <th class="text-center">QTY DEL</th>
                                        <th class="text-center">FOC</th>
                                        <th class="text-center">UNIT PRICE</th>
                                        <th class="text-center">IMPORT</th>
                                    </tr>
                                </thead>
                                <tbody class="view">

                                </tbody>
                            </table> <?php
                        } ?>
                		DP : Date Promised
                	<button class="btn btn-success pull-right">Save Packing List</button>
	            </div>
	        </form>
        </div>

        <div class="hidden upload_pl_pop">
        	<form enctype="multipart/form-data" method="POST" class="upload_pl_" action='<?=base_url('data/upload_pl_new');?>' >
        		<label>File</label>
        		<input type="file" name="file" class="form-control">
        		<label></label>
        		<button class="btn btn-success btn-flat btn-block submit">Verify File</button>
        	</form>
        	<div class="view_">

        	</div>
        </div>

        <!-- modal upload Packinglist -->
        <div class="hidden upload_placc_pop">
        	<form enctype="multipart/form-data" method="POST" class="upload_pl_acc" action='<?=base_url('data/upload_pl_acc');?>' >
        		<label>File</label>
        		<input type="file" name="file" class="form-control">
        		<label></label>
        		<button class="btn btn-success btn-flat btn-block submit">Verify File</button>
        	</form>
        	<div class="view_">

        	</div>
        </div>

        <div class="hidden upload_pl_fbr_pop">
        	<form enctype="multipart/form-data" method="POST" class="upload_pl_fbr_" action='<?=base_url('data/upload_pl_fr');?>' >
        		<label>File</label>
        		<input type="file" name="file_pl_fbr" class="form-control">
        		<label></label>
        		<button class="btn btn-success btn-flat btn-block submit">Verify File</button>
        	</form>
        	<div class="view_">

        	</div>
        </div>
        <div class="hidden upload_plykk_pop">
        	<form enctype="multipart/form-data" method="POST" class="uploadykk" action='<?=base_url('data/upload_pl_new');?>' >
        		<label>File</label>
        		<input type="file" name="file" class="form-control">
        		<label></label>
        		<button class="btn btn-success btn-flat btn-block submit">Verify File</button>
        	</form>
        	<div class="view_">

        	</div>
        </div>
        <script type="text/javascript">
            $('#notifications').slideDown('slow').delay(3000).slideUp('slow');
        	$(".livesearch").chosen();
            
            if ( !$.fn.dataTable.isDataTable( '#tables123' )) {

                $("#tables123").DataTable({
                    processing  : true,
                    serverSide  : true,
                    order: [],
                    orderMulti  : true,
                    lengthChange : false,
                    sDom: '<"row view-filter"<"col-sm-12"<"pull-left"l><"pull-right"f><"clearfix">>>t<"row view-pager"<"col-sm-12"<"text-center"ip>>>',
                    ajax: {
                      url: '<?php echo base_url('data/new_pl_ajax')?>',
                      type: 'POST',
                      datatype: 'JSON',
                      data: {
                          user: '<?php echo $this->session->userdata('user_id');?>',
                          m_warehouse_id: '<?php echo isset($_POST['m_warehouse_id']) ? $_POST['m_warehouse_id'] : '';?>'
                      }
                    },
                    columns: [
                        {"data" : 'checkbox'},
                        {'data' : 'po_number'},
                        {'data' : 'warehouse'}
                    ],
                    language: {
                        processing: "Sedang mengambil data...<div id='process'></div>",
                        searchPlaceholder: "Masukkan PO Number"
                    },
                    createdRow: function( row, data, dataIndex ) {
                        if ( data["warna"] > 0 ) {
                          $(row).addClass( 'bg-yellow' );
                        }
                    }
                });
            };

        	$(function(){
        		$('.upload_pl').click(function(){
        			$('.view_').html('');
        			var val = $('.upload_pl_pop').html();
        			$('#myModal').modal('show');
        			$('.modal-dialog').addClass('modal-sm');
        			$('.modal-title').text('Upload Packing List');
        			$('.modal-body').html(val);
        			$('.upload_pl_').submit(function(){
        				var data = new FormData(this);
        				$('.submit').attr('disabled',true);
        				$.ajax({
        					url:$(this).attr('action'),
        					type: "POST",
        					data: data,
        					contentType: false,
        					cache: false,
        					processData:false,
        					success: function(response){
        						$('.submit').attr('disabled',false);
        						$('.modal-dialog').removeClass('modal-sm');
        						$('.view_').html(response);
        					}
                            ,error:function(response){
                                if(response.status==500){
                                    $('.submit').attr('disabled',false);
                                    swal({
                                          title: "Warning!!",
                                          text: "Upload Failed",
                                          icon: "warning",
                                          dangerMode: true,
                                          timer: 1600,
                                          buttons: false
                                      });
                                }
                            }
        				})
        				return false;
        			})
        		})
        	})
        	$(function(){
        		$('.upload_placc').click(function(){
        			$('.view_').html('');
        			var val = $('.upload_placc_pop').html();
        			$('#myModal').modal({backdrop: 'static', keyboard: false});
        			$('.modal-dialog').addClass('modal-md');
        			$('.modal-title').text('Upload Packing List');
        			$('.modal-body').html(val);
        			$('.upload_pl_acc').submit(function(){
        				var data = new FormData(this);
        				$('.submit').attr('disabled',true);
        				$.ajax({
        					url:$(this).attr('action'),
        					type: "POST",
        					data: data,
        					contentType: false,
        					cache: false,
        					processData:false,
        					success: function(response){
        						$('.submit').attr('disabled',false);
        						$('.modal-dialog').removeClass('modal-sm');
        						$('.view_').html(response);
        					}
                            ,error:function(response){
                                if(response.status==500){
                                    $('.submit').attr('disabled',false);
                                    swal({
                                          title: "Warning!!",
                                          text: "Upload Failed",
                                          icon: "warning",
                                          dangerMode: true,
                                          timer: 1600,
                                          buttons: false
                                      });
                                }
                            }
        				});
        				return false;
        			});
        		});
        	})
        	$(function(){
        		$('.upload_pl_fbr').click(function(){
        			$('.view_').html('');
        			var val = $('.upload_pl_fbr_pop').html();
        			$('#myModal').modal({backdrop: 'static', keyboard: false});
        			$('.modal-dialog').addClass('modal-md');
        			$('.modal-title').text('Upload Packing List');
        			$('.modal-body').html(val);
        			$('.upload_pl_fbr_').submit(function(){
        				var data = new FormData(this);
        				$('.submit').attr('disabled',true);
        				$.ajax({
        					url:$(this).attr('action'),
        					type: "POST",
        					data: data,
        					contentType: false,
        					cache: false,
        					processData:false,
        					success: function(response){
        						$('.submit').attr('disabled',false);
        						$('.modal-dialog').removeClass('modal-md');
        						$('.view_').html(response);
        					}
        				})
        				return false;
        			})
        		})
        	})

        	$(function(){
        		$('.upload_plykk').click(function(){
        			$('.view_').html('');
        			var val = $('.upload_plykk_pop').html();
        			$('#myModal').modal({backdrop: 'static', keyboard: false});
        			$('.modal-dialog').addClass('modal-md');
        			$('.modal-title').text('Upload Packing List');
        			$('.modal-body').html(val);
        			$('.uploadykk').submit(function(){
        				$('.submit').attr('disabled',true);
        				var data = new FormData(this);
        				$.ajax({
        					url:$(this).attr('action'),
        					type: "POST",
        					data: data,
        					contentType: false,
        					cache: false,
        					processData:false,
        					success: function(response){
        						$('.submit').attr('disabled',false);
        						$('.modal-dialog').removeClass('modal-sm');
        						$('.view_').html(response);
        					}
        				})
        				return false;
        			})
        		})
        	})

        	function add(id){
        		if($('.'+id).is(':checked')){
        			$.ajax({
        				url:'<?=base_url('data/show_line');?>',
        				type:'POST',
        				data:'id='+id,
        				success:function(data){
        					$('.view').append(data);
        				}
        			});
        		}else{
        			$('._'+id).remove();
        		}
        	}
        	function activated(id){
        		if($('.c_'+id).is(':checked')){
        			$('.qd_'+id).prop('disabled',false).addClass('actived').val($('.qd_'+id).attr('val'));
        			$('.foc_'+id).prop('disabled',false).addClass('actived').val('0');
        			$('.car_'+id).prop('disabled',false).addClass('actived').val('1');
        			$('.imp_'+id).prop('disabled',false).addClass('actived').val('0');
        		}else{
        			$('.qd_'+id).val('').prop('disabled',true).removeClass('actived');
        			$('.foc_'+id).val('').prop('disabled',true).removeClass('actived');
        			$('.car_'+id).val('').prop('disabled',true).removeClass('actived');
        			$('.imp_'+id).val('').prop('disabled',true).removeClass('actived');
        		}
        	};
            function download_template(url){
                progress_bar();
                $.fileDownload(url)
                  .done(function () { dieYou_progress_bar(); });
            }
        </script> <?php
    }else{
	    echo "No records found!";
	}
}else{ echo "<div class='alert alert-info'>You can't create new packing list, because there is no active Purchase order</div>"; } ?>
<script type="text/javascript">
  $('#notifications').slideDown('slow').delay(5000).slideUp('slow');
</script>
