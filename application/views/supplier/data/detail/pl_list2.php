<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.16.0/moment.min.js" type="text/javascript"></script>
<link rel="stylesheet" href="<?=base_url('assets/dist/daterangepicker.min.css');?>" />
<script src="<?=base_url('assets/dist/jquery.daterangepicker.js');?>"></script>
<div class="col-md-12"> 
          <div class="box box-success">
            <div class="box-header with-border">
              <h3 class="box-title">Select The Created Packinglist Date</h3>
            </div>
          
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-inline"  method="post" id="two-inputs" >
            <!-- <form class="form-vertical" method="POST" action="<?=base_url('data/Packinglist');?>" id="two-inputs"> -->
              <div class="box-body">
                <div class="col-md-4">
                  <div class="form-group">
                  <label for="from" class="col-sm-5 control-label">Created From</label>

                  <div class="col-sm-7">
                    <input id="daterange1" class="form-control" name="from" placeholder="Start Date">
                  </div>
                </div>
                </div>
                <div class="col-md-6">
                <div class="form-group">
                  <label for="until" class="col-sm-5 control-label">Until</label>

                  <div class="col-sm-7">
                    <input id="daterange2" class="form-control" name="until" placeholder="End Date">
                  </div>
                </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <button type="submit" class="btn btn-success btn-block" id="search">Search</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
</div>
<div class="row">
  <div class="col-md-12">
    
      <div class="box-header with-border">
      <div class="box-body">
        <table class="table table-striped table-bordered" id="table">
          <thead>
            <tr class="bg-green">
            <th class="text-center">#</th>
            <th>PL NUMBER</th>
            <!--th>SJ / DN </th-->
            <th>INVOICE</th>
            <!-- <th>RESI / AWB</th> -->
            <th class="text-center">QC-CHECK REPORT</th>
            <th class="text-center">QC REPORT</th>
            <th class="text-center">DETAIL</th>
            <th class="text-center">ACTION</th>
            </tr>
          </thead>
        
          </table>
      </div>
    </div>
    
  </div>
</div>
<script type="text/javascript">
 $(document).ready(function(){
    $('#two-inputs').dateRangePicker(
      {
        
        getValue: function()
        {
          if ($('#daterange1').val() && $('#daterange2').val() )
            return $('#daterange1').val() + ' to ' + $('#daterange2').val();
          else
            return '';
        },
        setValue: function(s,s1,s2)
        {
          $('#daterange1').val(s1);
          $('#daterange2').val(s2);
        }
      });
      var table;
       table = $('#table').DataTable({ 

            "processing": true, 
            "serverSide": true, 
            "order": [],
            
            "ajax": {
                "url": "<?php echo site_url('data/packinglist_date_range')?>",
                "type": "POST",
                "datatype":"json",
                 "data":function(data) {
                    data.from = $('#daterange1').val();
                    data.to = $('#daterange2').val();
                      data.<?php echo $this->security->get_csrf_token_name(); ?> = "<?php echo $this->security->get_csrf_hash(); ?>";
                  },
            },

            
            "columns": [
              { "data": "no" },
              {
            render: function(data, type, full, meta)
            {
              var url = full.print_packing_list;
              var packing_list = full.no_packinglist;
              return '<a a target="_blank" href="'+url+'">'+packing_list+'</a>';
            }
           } ,
              { "data": "kst_invoicevendor" },
              {
            render: function(data, type, full, meta)
            {
              
             var flag =full.qc_check_report;
             var no_packinglist = full.no_packinglist;
             var kst_suratjalanvendor = full.kst_suratjalanvendor;
             var kst_invoicevendor = full.kst_invoicevendor;
             var url_upload = full.url_upload;
             var kst_resi = full.kst_resi;
             

             if(flag>0){
              return '<center>Already Uploaded</center>';
             }else{
              return '<center><form id="form_'+no_packinglist+'"><input type="file" class="files" name="file" onChange="upload_file(\''+no_packinglist+'\',\''+url_upload+'\')" id="'+no_packinglist+'"><input type="hidden" name="no_packinglist" value="'+no_packinglist+'"><input type="hidden" name="kst_suratjalanvendor" value="'+kst_suratjalanvendor+'"><input type="hidden" name="kst_invoicevendor" value="'+kst_invoicevendor+'"><input type="hidden" name="kst_resi" value="'+kst_resi+'"></form></center>' ;
             }
            }
           },
           {
            render: function(data, type, full, meta)
            {
              
             var flag =full.qc_check_report;
             var no_packinglist = full.no_packinglist;
             var kst_suratjalanvendor = full.kst_suratjalanvendor;
             var kst_invoicevendor = full.kst_invoicevendor;
             var url_download = full.url_download;
             var kst_resi = full.kst_resi;
             

             if(flag>0){
              return '<center><a href="'+url_download+'"><i class="fa fa-download"></i></a><center>';
             }else{
              return "<center>-</center>";
             }
            }
           },
           {
            render: function(data, type, full, meta)
            {
              
             var no_packinglist = full.no_packinglist;
             var kst_suratjalanvendor = full.kst_suratjalanvendor;
             var kst_invoicevendor = full.kst_invoicevendor;
             var kst_resi = full.kst_resi;
             var url_detail = full.url_detail;
             var data   = full.data;
             
             return '<center><a class="link" id="link" onClick=detail(\''+url_detail+'\',\''+data+'\') data="'+no_packinglist+'&sj='+kst_suratjalanvendor+'&inv='+kst_invoicevendor+'&awb='+kst_resi+'">Detail</a></center>';
            }
           },
           {
            render: function(data, type, full, meta)
            {
              
             var no_packinglist = full.no_packinglist;
             var kst_suratjalanvendor = full.kst_suratjalanvendor;
             var kst_invoicevendor = full.kst_invoicevendor;
             var kst_resi = full.kst_resi;
             var is_locked = full.is_locked;
             var type_po = full.type_po;
             var url_print_fb = full.url_print_fb;
             var url_print_acc = full.url_print_acc;
             var url_edit_header =full.url_edit_header;
             var url_isactive =full.url_isactive;
             var url_lock =full.url_lock;
             var data   = full.data;
             
             if(is_locked=='f'){
              return '<a><i class="fa fa-qrcode" style="margin-right:10px" onclick="return confirm(\'Please confirm packing list first\')" ></i></a><a title="Edit Header" style="margin-right:10px" onclick="edit_header(\''+url_edit_header+'\')"><i class="fa fa-edit"></i></a><a title="Delete Header" style="margin-right:10px" onclick="isactive(\''+url_isactive+'\',\''+data+'\')"><i class="fa fa-trash"></i></a></center><a title="Lock Packinglist" style="margin-right:10px" onclick="lock(\''+url_lock+'\',\''+data+'\')"><i class="fa fa-unlock"></i></a>';
             }else{
              if (type_po == 1) {
                return '<center><a title="Print Barcode" target="_Blank" style="margin-right:10px" href="'+url_print_fb+'"><i class="fa fa-qrcode"></i><a onclick="return alert(\'Packing List has been locked.\')"><i class="fa fa-lock"></i></a></center>';
              }else{
                return '<center><a title="Print Barcode" target="_Blank" style="margin-right:10px" href="'+url_print_acc+'"><i class="fa fa-qrcode"></i></a><a onclick="return alert(\'Packing List has been locked.\')"><i class="fa fa-lock"></i></a></center>';
              }

             }             
             
            }
           }  
           ]

        });
  
    $('#search').on( 'click change', function (event) {
      event.preventDefault();

      if($('#daterange1').val()=="")
      {
        $('#daterange1').focus();
      }
      else if($('#daterange2').val()=="")
      {
        $('#daterange2').focus();
      }
      else
      {
        table.draw();
      }

    } );
 });
function upload_file(id,url){
    var form_data = new FormData($('#form_'+id)[0]);
    var x = confirm('Do you really want to upload a file?');
    if(x==true){
      $.ajax({
          url: url,
          type: "POST",
          data: form_data,
          enctype: 'multipart/form-data',
          processData: false,
          contentType: false,
          success: function( values )
          {
            if (values == 1) {
          $('#table').DataTable().ajax.reload();
              } 
          }
        })
      .fail(function(jqXHR, textStatus) {
            
            $.unblockUI();
            alert('File upload failed ...');
        });
    }else{
      $('#table').DataTable().ajax.reload();
    }
  } 
  
     function detail(url,data) {
      $.ajax({
        url:url,
        data:data,
        type:"POST",
        success:function(data){
          var modal = $('#myModal > div > div');
          $('#myModal').modal('show');
          modal.children('.modal-header').children('.modal-title').html('EDIT DATA');
          modal.parent('.modal-dialog').addClass('modal-lg');
          modal.children('.modal-body').html(data);

        }
      })
      return false;
     }
    function edit_header(url){      
      $.ajax({
        url:url,
        success:function(data){
          $('#myModal').modal('show');
          $('.modal-dialog').removeClass('modal-lg');
          $('.modal-title').html('EDIT DATA');
          $('.modal-body').html(data);
          $('#header').submit(function(){
            $.ajax({
            url:$('#header').attr('action'),
            type: "POST",
            data: $('#header').serialize(),
            success:function(values){
              if(values==1){
                $('#myModal').modal('hide');
                $('#table').DataTable().ajax.reload();
              }else{
                alert("Packinglist Already locked, Refresh Your Page");
                $('#table').DataTable().ajax.reload(); 
              }
            }
          });
            return false;
          })
        }
      })
      return false;
    }
    function isactive(url,data){
      event.preventDefault();
      var x = confirm('Do you really want to Delete a Packinglist?');     
      if(x==true)
        $.ajax({
        url:url,
        type:"POST",
        data:data,
        success:function(value){
          if (value==1) {
            $('#table').DataTable().ajax.reload();
          }else{
            alert('Packinglist Already locked, Refresh Your Page');
            $('#table').DataTable().ajax.reload();
          }
        }
      })
    }
    function lock(url,data){
      //event.preventDefault();
      var x = confirm('Do you really want to Lock a Packinglist?');     
      if(x==true){
        $.ajax({
          url:url,
          type:"POST",
          data:data,
          success:function(value){
            $('#table').DataTable().ajax.reload();
          }
        });
      }
      
    }
</script>