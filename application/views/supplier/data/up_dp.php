<div class="row">
	<div class="col-md-4">
          <!-- Application buttons -->
          <div class="box box-primary">
            <div class="box-header">
              <h3 class="box-title">Date Promised</h3>
            </div>
            <div class="box-body">
              <!-- <p>Add the classes <code>.xls</code> to an <code>&lt;a></code> tag to achieve the following:</p> -->
              <a onclick="location.href = '<?=base_url('data/download_excel_po?c_bpartner_id='.$this->session->userdata('user_id'));?>';" href='javascript:void(0)' class="btn btn-app">
              	<span class="badge bg-green">
              		<?=$this->db->distinct()->select('c_order_id')->where('c_bpartner_id',$this->session->userdata('user_id'))->where('status','t')->get('show_po_status')->num_rows();?>
              	</span>
                <i class="fa fa-download"></i> Download Date Promised
              </a>
              <a href="#" class="btn btn-app upload_dp">
                <i class="fa fa-upload"></i> Upload
              </a>
            </div>
            <!-- /.box-body -->
          </div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box bg-blue">
            <span class="info-box-icon"><i class="fa fa-calendar"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">TOTALS</span>
              <span class="info-box-number">
              	<?=
              		$this->db2->distinct()->select('c_order_id')->where('c_bpartner_id',$this->session->userdata('user_id'))->get('adt_unconfirmed_date_promised')->num_rows();
              	?>
              </span>

                  <span class="progress-description">
                    Unconfirmed Date Promised
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
    </div>
</div>
<div class="row">
	<div class="col-md-4">
          <!-- Application buttons -->
          <div class="box box-warning">
            <div class="box-header">
              <h3 class="box-title">Packing List</h3>
            </div>
            <div class="box-body">
              <!-- <p>Add the classes <code>.xls</code> to an <code>&lt;a></code> tag to achieve the following:</p> -->
             
              <?php 
              $user = $this->session->userdata('user_id');
              $this->db2->where('c_bpartner_id',$user);
              $users = $this->db2->get('f_web_po_header');
              if($users->result()[0]->type_po==2){ ?>
                <a onclick="location.href = '<?=base_url('data/download_pl_acc?c_bpartner_id='.$this->session->userdata('user_id'));?>';" href='javascript:void(0)' class="btn btn-app"><i class="fa fa-download"></i>Download Packing List</a>
              <?php }else{ ?>
                  <a onclick="location.href = '<?=base_url('data/download_excel_pl_fbr?c_bpartner_id='.$this->session->userdata('user_id'));?>';" href='javascript:void(0)' class="btn btn-app"><i class="fa fa-download"></i>Download Packing List</a>
              <?php } 
              ?>
              <?php 
              $user = $this->session->userdata('user_id');
              $this->db2->where('c_bpartner_id',$user);
              $users = $this->db2->get('f_web_po_header');
              if($users->result()[0]->type_po==2 && $this->session->userdata('user_id') != 1001120){ ?>     
                <a href="#" class="btn btn-app upload_placc"><i class="fa fa-upload"></i> Upload</a>
              <?php }elseif ($this->session->userdata('user_id') == 1001120) { ?>
                <a href="#" class="btn btn-app upload_pl"><i class="fa fa-upload"></i> Upload</a>
              <?php }else{ ?>
                <a href="#" class="btn btn-app upload_pl_fbr"><i class="fa fa-upload"></i> Upload</a>
              <?php } ?>

            </div>
            <!-- /.box-body -->
          </div>
	</div>
	<div class="col-md-4 col-sm-6 col-xs-12">
          <div class="info-box bg-yellow">
            <span class="info-box-icon"><i class="fa fa-ship"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">TOTALS</span>
              <span class="info-box-number"><?=$this->db->distinct()->select('no_packinglist')->where('c_bpartner_id',$this->session->userdata('user_id'))->where('isactive','t')->get('po_detail')->num_rows();?></span>

                  <span class="progress-description">
                    Packing List Created
                  </span>
            </div>
            <!-- /.info-box-content -->
          </div>
          <!-- /.info-box -->
    </div>
</div>
<!-- ----------------------------------------------------------------------------------------------------------------------- -->
<div class="hidden upload_dp_pop">
	<form enctype="multipart/form-data" method="POST" class="upload_dp_" action='<?=base_url('data/upload_dp_new');?>' >
		<label>File</label>
		<input type="file" name="file" class="form-control">
		<label style="font-size: 12px; font-style: italic;">Please upload <strong>.xls(excel 97-2003 workbook)</strong> format only!</label><br>
		<label style="font-size: 12px; font-style: italic;">Make sure to <strong>delete the blank records</strong> before uploading a file!</label>
		<button class="btn btn-success btn-flat btn-block">Verify File</button>
	</form>
	<div class="view_">

	</div>
</div>
<div class="hidden upload_pl_pop">
  <form enctype="multipart/form-data" method="POST" class="upload_pl_" action='<?=base_url('data/upload_pl_new');?>' >
    <label>File</label>
    <input type="file" name="file" class="form-control">
    <label></label>
    <button class="btn btn-success btn-flat btn-block">Verify File</button>
  </form>
  <div class="view_">

  </div>
</div>
<!-- modal upload Packinglist -->
<div class="hidden upload_placc_pop">
  <form enctype="multipart/form-data" method="POST" class="upload_pl_acc" action='<?=base_url('data/upload_pl_acc');?>' >
    <label>File</label>
    <input type="file" name="file" class="form-control">
    <label></label>
    <button class="btn btn-success btn-flat btn-block">Verify File</button>
  </form>
  <div class="view_">

  </div>
</div>
<div class="hidden upload_pl_fbr_pop">
  <form enctype="multipart/form-data" method="POST" class="upload_pl_fbr_" action='<?=base_url('data/upload_pl_fr');?>' >
    <label>File</label>
    <input type="file" name="file_pl_fbr" class="form-control">
    <label></label>
    <button class="btn btn-success btn-flat btn-block">Verify File</button>
  </form>
  <div class="view_">

  </div>
</div>

<script type="text/javascript">
	$(function(){
		$('.upload_dp').click(function(){
			$('.view_').html('');
			var val = $('.upload_dp_pop').html();
			$('#myModal').modal('show');
			$('.modal-dialog').addClass('modal-lg');
			$('.modal-title').text('Upload Date Promised');
			$('.modal-body').html(val);
			$('.upload_dp_').submit(function(){
				var data = new FormData(this);
				$.ajax({
					url:$(this).attr('action'),
					type: "POST",
					data: data,
					contentType: false,       
					cache: false,          
					processData:false, 
					success: function(response){
						$('.modal-dialog').removeClass('modal-lg');
						$('.view_').html(response);
					}
				})
				return false;
			})
		})
	})
  $(function(){
    $('.upload_pl').click(function(){
      $('.view_').html('');
      var val = $('.upload_pl_pop').html();
      $('#myModal').modal('show');
      $('.modal-dialog').addClass('modal-md');
      $('.modal-title').text('Upload Packing List YKK');
      $('.modal-body').html(val);
      $('.upload_pl_').submit(function(){
        var data = new FormData(this);
        $.ajax({
          url:$(this).attr('action'),
          type: "POST",
          data: data,
          contentType: false,       
          cache: false,          
          processData:false, 
          success: function(response){
            $('.modal-dialog').removeClass('modal-md');
            $('.view_').html(response);
          }
        })
        return false;
      })
    })
  })
  $(function(){
    $('.upload_placc').click(function(){
      $('.view_').html('');
      var val = $('.upload_placc_pop').html();
      $('#myModal').modal('show');
      $('.modal-dialog').addClass('modal-md');
      $('.modal-title').text('Upload Packing List Acc');
      $('.modal-body').html(val);
      $('.upload_pl_acc').submit(function(){
        var data = new FormData(this);
        $.ajax({
          url:$(this).attr('action'),
          type: "POST",
          data: data,
          contentType: false,       
          cache: false,          
          processData:false, 
          success: function(response){
            $('.modal-dialog').removeClass('modal-md');
            $('.view_').html(response);
          }
        })
        return false;
      })
    })
  })
  $(function(){
    $('.upload_pl_fbr').click(function(){
      $('.view_').html('');
      var val = $('.upload_pl_fbr_pop').html();
      $('#myModal').modal('show');
      $('.modal-dialog').addClass('modal-md');
      $('.modal-title').text('Upload Packing List Fbr');
      $('.modal-body').html(val);
      $('.upload_pl_fbr_').submit(function(){
        var data = new FormData(this);
        $.ajax({
          url:$(this).attr('action'),
          type: "POST",
          data: data,
          contentType: false,       
          cache: false,          
          processData:false, 
          success: function(response){
            $('.modal-dialog').removeClass('modal-md');
            $('.view_').html(response);
          }
        })
        return false;
      })
    })
  })
</script>