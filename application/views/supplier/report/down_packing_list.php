<?php 
		$no_packinglist = $_GET['no_packinglist'];
		$kst_invoicevendor = $_GET['invoice'];
		$this->db->where('kst_invoicevendor',$kst_invoicevendor);
		$this->db->where('no_packinglist',$no_packinglist);
		$data = $this->db->get('packinglist_header');
		 header("Content-Type:application/vnd.ms-excel");
  header('Content-Disposition:attachment; filename="download packinglist.xls"');
		foreach($data->result() as $pols){
	?>
	<div class="outer">
		<div class="create">
			<span style="font-size: 12px; padding-right: 10px;" >Created On: <?=$pols->date;?></span>
		</div>
		<div class="header">
			<span style="font-size: 32px; margin-top: 30px;">PACKING LIST</span><br><br>
			<span style="font-size: 18px"><?=$pols->supplier;?></span>
		</div>
		<div class="create">
			<div class="header-kiri">
				<span style="padding-left: 10px">Packing List no : <?=$pols->no_packinglist;?></span> 
			</div>
			<div class="header-kanan">
				<span style="padding-right: 10px">Invoice no : <?=$pols->kst_invoicevendor;?></span> 
			</div>
		</div>
		<div class="isi">
			<table border="1px" class="tables">
				<thead>
					<tr>
						<th>PURCHASE ORDER</th>
						<th>ITEM CODE</th>
						<th>PRODUCT</th>
						<th>QUANTITY</th>
						<th>FOC</th>
						<th>UOM</th>
						<?php if ($pols->type_po =='1') {
								echo "<th>ROLL</th>";
							}
							else{
								echo "<th>CARTON</th>";
							}
							;?>
					</tr>
				</thead>
				<tbody>
					<?php
						$this->db->order_by('documentno'); 
						$this->db->where('isactive','TRUE');
						$detail = $this->db->where('no_packinglist',$no_packinglist)->get('po_detail');
						foreach($detail->result() as $pol){
					?>
					<tr>
						<td><?=$pol->documentno?></td>
						<td><?=$pol->item?></td>
						<td><?=$pol->desc_product;?></td>
						<td><?=$pol->qty_upload;?></td>
						<td><?=$pol->foc;?></td>
						<td><?=$pol->uomsymbol;?></td>
						<td><?=$pol->qty_carton;?></td>
					</tr>
					<?php
						}
					?>
				</tbody>
			</table>

		</div>
	</div>

	<?php
		}
	?>