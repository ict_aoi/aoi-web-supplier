<?php
   if(isset($_GET['m_warehouse_id']) && $_GET['m_warehouse_id'] != NULL){
		$this->db2->where('m_warehouse_id',$_GET['m_warehouse_id']);
	}
	$this->db2->where('c_bpartner_id',$_GET['c_bpartner_id']);
	$data_all=$this->db2->get('adt_format_packinglist_v1')->result();
	$tgl = date("Y-m-d");

	header("cache-control:no-cache.must-revalidate");
    header("pragma:no-cache");
    header("Content-type=appalication/x-ms-excel");
    header('Content-Disposition:attachment; filename="Packing List Fabric '.date('d-m-Y').'.xls"');
    header('Set-Cookie: fileDownload=true; path=/');
?>

<table border="1px">
	<thead>
		<th bgcolor="GreenYellow">ORDERLINE ID</th>
		<th bgcolor="Firebrick">NO PACKING LIST</th>
		<th bgcolor="Firebrick">INVOICE</th>
		<th bgcolor="Firebrick">EX FACTORY DATE</th>
		<th bgcolor="Firebrick">ESTIMATED TIME ARRIVAL</th>
		<th bgcolor="GreenYellow">DOCUMENT PO</th>
		<th bgcolor="GreenYellow">ITEM</th>
		<th bgcolor="GreenYellow">QTY ORDER</th>
		<th bgcolor="GreenYellow">UOM</th>
		<th bgcolor="Firebrick">QTY FOC</th>
		<th bgcolor="Firebrick">ROLL NUMBER</th>
		<th bgcolor="Firebrick">QTY DELIVER</th>
		<th bgcolor="Firebrick">BATCH / DYE LOT</th>
		<th bgcolor="Gold">REMARK</th>
		<th bgcolor="Firebrick">AIR WAYBILL / RESI</th>
		<th bgcolor="Gold">BALANCE</th>
		<th>PRICE</th>
		<th>DOCSTATUS PO</th>
		<th>BP TYPE</th>
	</thead>
	<tbody>
		<?php foreach ($data_all as $data) {?>
		<tr>
			<td bgcolor="GreenYellow"><?php echo $data->c_orderline_id; ?></td>
			<td bgcolor="Firebrick"></td>
			<td bgcolor="Firebrick"></td>
			<td bgcolor="Firebrick"></td>
			<td bgcolor="Firebrick"></td>
			<td bgcolor="GreenYellow"><?php echo $data->documentno; ?></td>
			<td bgcolor="GreenYellow"><?php echo $data->item; ?></td>
			<td style="background-color: GreenYellow"><?php echo sprintf('%0.2f',$data->qtyentered);?></td>
			<td bgcolor="GreenYellow"><?php echo $data->uomsymbol; ?></td>
			<td bgcolor="Firebrick">0</td>
			<td bgcolor="Firebrick"></td>
			<td bgcolor="Firebrick"></td>
			<td bgcolor="Firebrick"></td>
			<td bgcolor="Gold"></td>
			<td bgcolor="Firebrick">-</td>
			<td bgcolor="Gold"><?php echo $data->balance;?></td>
			<td><?=$data->priceentered;?></td>
			<td><?=$data->docstatus;?></td>
			<td><?=$data->c_bp_group_id;?></td>
		</tr>
		<?php } ?>
	</tbody>
</table>