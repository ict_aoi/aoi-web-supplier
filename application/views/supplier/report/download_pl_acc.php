<?php
	if(isset($_GET['m_warehouse_id']) && $_GET['m_warehouse_id'] != NULL){
		$this->db2->where('m_warehouse_id',$_GET['m_warehouse_id']);
	}
	$this->db2->where('c_bpartner_id',$_GET['c_bpartner_id']);
	$data_all=$this->db2->get('adt_format_packinglist_per_crd')->result();
	$tgl = date("Y-m-d");

	header("cache-control:no-cache.must-revalidate");
    header("pragma:no-cache");
    header("Content-type=appalication/x-ms-excel");
    header('Content-Disposition:attachment; filename="Template Upload Packinglist '.date('d-m-Y').'.xls"');
	header('Set-Cookie: fileDownload=true; path=/');	
?>
<table border="1px">
<thead>
	<tr>
		<th style="background-color: YellowGreen">C ORDERLINE ID</th>
		<th style="background-color: Crimson">INVOICE NO</th>
		<th style="background-color: YellowGreen">PO NUMBER</th>
		<th style="background-color: YellowGreen">ORDER QTY</th>
		<th style="background-color: Crimson">INVOICE QTY</th>
		<th style="background-color: Crimson">PACKING LIST NO</th>
		<th style="background-color: Crimson">DELIVERY DATE</th>
		<th style="background-color: Crimson">ETA</th>
		<th style="background-color: Gold">PSNO</th>
		<th style="background-color: Crimson">SUM QTY CARTON</th>
		<th style="background-color: Gold">ORDER NO</th>
		<th style="background-color: Gold">REMARK</th>
		<th style="background-color: Crimson">AWB/RESI</th>
		<th style="background-color: YellowGreen">ITEM</th>
		<th style="background-color: YellowGreen">UOM</th>
		<th style="background-color: Crimson">FOC</th>
		<th style="background-color: Gold">ALREADY IN PACKINGLIST</th>
		<th>Price</th>
		<th>DETAIL PRODUCT</th>
		<th style="background-color: Gold">POBUYER</th>
		<th style="background-color: Gold">FIRST CRD</th>
		<th>DOCSTATUS PO</th>

	</tr>
</thead>
<tbody>
	<?php foreach($data_all as $data){ ?>
		<tr>
			<td style="background-color: YellowGreen"><?=$data->c_orderline_id;?></td>
			<td style="background-color: Crimson"></td>
			<td style="background-color: YellowGreen"><?=$data->documentno;?></td>
			<!-- <td style="background-color: YellowGreen"><?=$data->qtyentered;?></td> -->
			<td style="background-color: YellowGreen"><?php echo sprintf('%0.2f',$data->qtyentered);?></td>
			<td style="background-color: Crimson"></td>
			<td style="background-color: Crimson"></td>
			<td style="mso-number-format:'Short Date';background-color: Crimson;"></td>
			<td style="mso-number-format:'Short Date';background-color: Crimson;"></td>
			<td style="background-color: Gold"></td>
			<td style="background-color: Crimson"></td>
			<td style="background-color: Gold"></td>
			<td style="background-color: Gold"></td>
			<td style="background-color: Crimson; text-align: center;">-</td>
			<td style="background-color: YellowGreen"><?=$data->item;?></td>
			<td style="background-color: YellowGreen"><?=$data->uomsymbol;?></td>
			<td style="background-color: Crimson">0</td>
			<td style="background-color: Gold"><?=$data->qty_upload;?></td>
			<td><?=$data->priceentered;?></td>
			<td><?=$data->prod_detail;?></td>
			<td style="background-color: Gold"><?=$data->pobuyer;?></td>
			<td style="background-color: Gold"><?=$data->crd;?></td>
			<td><?=$data->docstatus;?></td>
		</tr>
	<?php } ?>
</tbody>	
</table>