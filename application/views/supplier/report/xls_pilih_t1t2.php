<?php
  if(isset($_GET['from']) && $_GET['from'] != NULL){
    $dari   = date("Y-m-d 00:00:00",strtotime($_GET['from']));
    $this->db2->where('issue_date >=',$dari);
    }
    if(isset($_GET['until']) && $_GET['until'] != NULL){
      $sampai = date("Y-m-d 23:59:59",strtotime($_GET['until']));
      $this->db2->where('issue_date <=',$sampai);
    }
    if (isset($_GET['c_bpartner_id']) && $_GET['c_bpartner_id'] != NULL){
      $this->db2->where('c_bpartner_id',$_GET['c_bpartner_id']);      
    }

    $data_all= $this->db2->get('adt_report_t1t2_new_v1');
        
  header("Content-Type:application/vnd.ms-excel");
  header('Content-Disposition:attachment; filename="Report T1 & T2.xls"');
?>

<table border="1px">
  <thead>
    <th>ISSUE_DATE</th>
<th>ORDER_NUMBER</th>
<th>LATEST_UPDATE</th>
<th>VERSION</th>
<th>PURPOSE</th>
<th>BUYER</th>
<th>T1_ADDRESS1BUY</th>
<th>T1_ADDRESS2BUY</th>
<th>T1_ADDRESS3BUY</th>
<th>T1_ADDRESS4BUY</th>
<th>T1_ADDRESSCITYBUY</th>
<th>T1_ADDRESSSTATEBUY</th>
<th>T1_ADDRESSPOSTALCODEBUY</th>
<th>T1_ADDRESSCOUNTRYBUY</th>
<th>CONTACT_PERSONBUYER</th>
<th>CONTACT_NOBUYER</th>
<th>EMAILBUYER</th>
<th>SHIP_TO</th>
<th>T1_ADDRESS1</th>
<th>T1_ADDRESSCITY</th>
<th>T1_ADDRESSPROVINCE</th>
<th>T1_ADDRESSPOSTAL</th>
<th>T1_ADDRESSCOUNTRY</th>
<th>T1_6DIGIT_FACTORYCODE</th>
<th>SELLER</th>
<th>ACTUAL_MANUFACTURER</th>
<th>T2_6DIGIT_FACTORYCODE</th>
<th>PO_DOWNLOAD_DATE</th>
<th>CURRENCY</th>
<th>ADIDAS_ARTICLENUMBER</th>
<th>PAYMENT_TERMS</th>
<th>INCOTERM</th>
<th>SHIP_MODE</th>
<th>COUNTRY_ORIGIN</th>
<th>FORWARDER</th>
<th>LINE_STATUS</th>
<th>LINE NO</th>
<th>REF</th>
<th>MATERIAL_NAME</th>
<th>WIDTH</th>
<th>WEIGHT</th>
<th>SIZE</th>
<th>MATERIAL_COLOUR</th>
<th>UNIT_PRICE</th>
<th>ORDER_QUANTITY</th>
<th>UOM</th>
<th>BUYER_REQUESTDATE</th>
<th>DELIVERY_DATE</th>
<th>DELIVERY_QUANTITY</th>
<th>SEASON</th>
<th>PRIORITY & ORDER TYPE </th>
<th>ADIDAS_ORDERNUMBER</th>

  </thead>
  <tbody>
    <?php foreach($data_all->result() as $data){ ?>
      <tr>
        <td style="mso-number-format:'Short Date'"><?=$data->issue_date;?></td>
        <td><?=$data->order_number;?></td>
        <td style="mso-number-format:'Short Date'"><?=$data->latest_update;?></td>
        <td><?=$data->version;?></td>
        <td><?=$data->purpose;?></td>
        <td><?=$data->buyer;?></td>
        <td><?=$data->t1_address1buy;?></td>
        <td><?=$data->t1_address2buy;?></td>
        <td><?=$data->t1_address3buy;?></td>
        <td><?=$data->t1_address4buy;?></td>
        <td><?=$data->t1_addresscitybuy;?></td>
        <td><?=$data->t1_addressstatebuy;?></td>
        <td><?=$data->t1_addresspostalcodebuy;?></td>
        <td><?=$data->t1_addresscountrybuy;?></td>
        <td><?=$data->contact_personbuyer;?></td>
        <td><?=$data->contact_nobuyer;?></td>
        <td><?=$data->emailbuyer;?></td>
        <td><?=$data->ship_to;?></td>
        <td><?=$data->t1_address1;?></td>
        <td><?=$data->t1_addresscity;?></td>
        <td><?=$data->t1_addressprovince;?></td>
        <td><?=$data->t1_addresspostal;?></td>
        <td><?=$data->t1_addresscountry;?></td>
        <td><?=$data->t1_6digit_factorycode;?></td>
        <td><?=$data->seller;?></td>
        <td><?=$data->actual_manufacturer;?></td>
        <td><?=$data->t2_6digit_factorycode;?></td>
        <td style="mso-number-format:'Short Date'"><?=$data->po_download_date;?></td>
        <td><?=$data->currency;?></td>
        <td><?=$data->adidas_articlenumber;?></td>
        <td><?=$data->payment_terms;?></td>
        <td><?=$data->incoterm;?></td>
        <td><?=$data->ship_mode;?></td>
        <td><?=$data->country_origin;?></td>
        <td><?=$data->forwarder;?></td>
        <td><?=$data->line_status;?></td>
        <td><?=$data->line;?></td>
        <td><?=$data->ref;?></td>
        <td><?=$data->material_name;?></td>
        <td><?=$data->width;?></td>
        <td><?=$data->weight;?></td>
        <td><?=$data->size;?></td>
        <td><?=$data->material_colour;?></td>
        <td><?=$data->unit_price;?></td>
        <td><?=$data->order_quantity;?></td>
        <td><?=$data->uom;?></td>
        <td style="mso-number-format:'Short Date'"><?=$data->buyer_requestdate;?></td>
        <td style="mso-number-format:'Short Date'"><?=$data->delivery_date;?></td>
        <td><?=$data->delivery_quantity;?></td>
        <td><?=$data->season;?></td>
        <td></td>
        <td><?=$data->adidas_ordernumber;?></td>

      </tr>
    <?php }?>
  </tbody>
</table> 