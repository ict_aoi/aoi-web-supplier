<?php
  if(isset($_GET['from']) && $_GET['from'] != NULL){
    // $dari   = date("Y-m-d 00:00:00",strtotime($_GET['from']));
    $dari = $_GET['from'];
    $this->db->where('create_date >=',$dari);
    }
    if(isset($_GET['until']) && $_GET['until'] != NULL){
      // $sampai = date("Y-m-d 23:59:59",strtotime($_GET['until']));
      $sampai = $_GET['until'];
      $this->db->where('create_date <=',$sampai);
    }
    if (isset($_GET['c_bpartner_id']) && $_GET['c_bpartner_id'] != NULL){
      $this->db->where('c_bpartner_id',$_GET['c_bpartner_id']);      
    }

    
    if (isset($_GET['no_invoice']) && $_GET['no_invoice'] != NULL){
      $this->db->where('kst_invoicevendor',$_GET['no_invoice']);      
    }
    
    if (isset($_GET['warehouse']) && $_GET['warehouse'] != NULL){
      $this->db->where('m_warehouse_id',$_GET['warehouse']);      
    }

    $data_all= $this->db->get('tpb_xls_update_awb');
        
  header("Content-Type:application/vnd.ms-excel");
  header('Content-Disposition:attachment; filename="Update AWB.xls"');
?>

<table border="1px">
  <thead>
    <th style="background-color: GreenYellow">Packing List Number</th>
    <th style="background-color: GreenYellow">Invoice Number</th>
    <th>AWB/Resi Number</th>
    <th style="background-color: GreenYellow">C_BPartner_ID</th>
    <th style="background-color: GreenYellow">Supplier Name</th>
    <th style="background-color: GreenYellow">PL Created at</th>
    <th>Nomor Pend.</th>
    <th>Nomor AJU</th>
    <th>Kurs</th>
    <th>BC Date</th>
    <th>No KPBC</th>
    <th>HS Code</th>
    <th>ETA Port</th>
    <th>Jenis BC</th>
    <th>Gross Weight</th>
    <th>CBM</th>
    <th>FCL</th>
    <th>Port of Discharge</th>
    <th>Remarks</th>
    <th>Kode Valuta</th>
    <th>BM</th>
    <th>PPN</th>
    <th>PPH</th>
  </thead>
  <tbody>
    <?php foreach($data_all->result() as $data){ ?>
      <tr>
        <td style="mso-number-format:'\@'; background-color: GreenYellow"><?=$data->no_packinglist;?></td>
        <td style="mso-number-format:'\@'; background-color: GreenYellow"><?=$data->kst_invoicevendor;?></td>
        <td style="mso-number-format:'\@'"><?=$data->kst_resi;?></td>
        <td style="background-color: GreenYellow"><?=$data->c_bpartner_id;?></td>
        <td style="background-color: GreenYellow"><?=$data->supplier;?></td>
        <td style="mso-number-format:'Short Date'; background-color: GreenYellow"><?=$data->create_date;?></td>
        <td></td>
        <td></td>
        <td></td>
        <td style="mso-number-format:'Short Date'"></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
      </tr>
    <?php }?>
  </tbody>
</table> 
